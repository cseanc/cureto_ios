//
//  SettingsController.swift
//  Cureto
//
//  Created by Sean Choo on 3/5/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import Haneke
import SafariServices

class SettingsController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerNib(UINib(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsCell")
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBarHidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        navigationController?.navigationBarHidden = true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let publishHeader = NSBundle.mainBundle().loadNibNamed("PublishHeader", owner: self, options: nil)[0] as! UIView
        return publishHeader
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        } else if section == 1 {
            return 1
        } else {
            return 2
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 55
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SettingsCell", forIndexPath: indexPath) as! SettingsTableViewCell
        cell.separatorHeight.constant = 0.5
        cell.titleLabel.textColor = UIColor.blackColor()
        
        if indexPath.section == 2 {
            cell.disclosureIndicator.hidden = false
            
            if indexPath.row == 0 {
                cell.titleLabel.text = "Special Thanks"
                cell.separator.hidden = false
            } else {
                cell.titleLabel.text = "Policies and Terms"
                cell.separator.hidden = true
            }
            
        } else {
            cell.disclosureIndicator.hidden = true
            
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    cell.titleLabel.text = "Change Password"
                    cell.separator.hidden = false
                    
                } else if indexPath.row == 1 {
                    cell.titleLabel.text = "Clear Cache"
                    cell.separator.hidden = false
                    
                } else {
                    cell.titleLabel.text = "Log Out"
                    cell.separator.hidden = true
                }
                
            } else if indexPath.section == 1 {
                if indexPath.row == 0 {
                    cell.titleLabel.text = "Log Out from All Devices"
                    cell.separator.hidden = true
                }
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                performSegueWithIdentifier("settings_to_password_segue", sender: self)
            } else if indexPath.row == 1 {
                clearCacheTapped()
            } else {
                logoutTapped()
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                logoutAllTapped()
            }
        } else {
            if indexPath.row == 0 {
                performSegueWithIdentifier("settings_to_thanks_segue", sender: self)
            } else {
                if let url = NSURL(string: CuretoUrls.policiesAndTermsUrl) {
                    if #available(iOS 9, *) {
                        let svc = SFSafariViewController(URL: url)
                        presentViewController(svc, animated: true, completion: nil)
                    } else {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }
            }
        }
    }
    
    func clearCacheTapped() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        
        let clearCacheAction = UIAlertAction(title: "Clear Cache", style: .Default, handler: { action in
            self.clearCache()
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(clearCacheAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func clearCache() {
        Shared.imageCache.removeAll()
        CuretoAlerts.notifyUserWith(CuretoAlerts.MessageType.CacheCleared, viewController: self)
    }
    
    func logoutTapped() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)

        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)

        let logoutAction = UIAlertAction(title: "Log Out", style: .Destructive, handler: { action in
            self.logout()
        })

        alertController.addAction(cancelAction)
        alertController.addAction(logoutAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func logout(all: Bool = false) {
        guard let token = CredentialStore.sharedInstance.accessToken else {
            return
        }
        
        CredentialStore.sharedInstance.clearEverything()
        
        CuretoProgressHUD.sharedInstance.startSpinning(view)
        
        if all {
            CuretoRequests.logout(token, all: true, onCompletion: { succeeded, message in
                CuretoProgressHUD.sharedInstance.stopSpinning()
                if succeeded {
                    NSNotificationCenter.defaultCenter().postNotificationName("LoginStatusChanged", object: nil)
                } else {
                    CuretoBannerAlert.showMessage(message)
                }
            })
            
        } else {
            CuretoRequests.logout(token, onCompletion: { succeeded, message in
                CuretoProgressHUD.sharedInstance.stopSpinning()
                if succeeded {
                    CredentialStore.sharedInstance.clearEverything()
                    NSNotificationCenter.defaultCenter().postNotificationName("LoginStatusChanged", object: nil)
                } else {
                    CuretoBannerAlert.showMessage(message)
                }
            })
        }

    }
    
    func logoutAllTapped() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        
        let logoutAction = UIAlertAction(title: "Log Out from All Devices", style: .Destructive, handler: { action in
            self.logout(true)
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(logoutAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
}
