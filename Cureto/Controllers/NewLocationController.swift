//
//  NewLocationController.swift
//  Cureto
//
//  Created by Sean Choo on 4/28/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class NewLocationController: UIViewController, UITextFieldDelegate {
    
    var addLocationController: AddLocationController?
    
    var textTimer: NSTimer?
    
    var currentPin: MKAnnotation?
    var validName: Bool = false
    var validArea: Bool = false

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var areaTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        separatorHeight.constant = 0.5
        disableAddButton()
        
    }
    
//    override func viewWillAppear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: false)
//    }
//    
//    override func viewWillDisappear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: true)
//    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        textTimer?.invalidate()
        textTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(NewLocationController.typingPaused), userInfo: textField, repeats: false)
        disableAddButton()
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            areaTextField.becomeFirstResponder()
        default:
            view.endEditing(true)
        }
        return true
    }
    
    func typingPaused(timer: NSTimer) {
        if let textField = timer.userInfo as? UITextField {
            switch textField {
            case nameTextField:
                if let name = nameTextField.text {
                    if name == "" {
                        validName = false
                    } else {
                        validName = true
                    }
                }
            case areaTextField:
                if let area = areaTextField.text {
                    if area == "" {
                        validArea = false
                    } else {
                        validArea = true
                    }
                }
            default:
                break
            }
        }
        updateFormStatus()
    }
    
    func disableAddButton() {
        addButton.enabled = false
        addButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
    }
    
    func enableAddButton() {
        addButton.enabled = true
        addButton.setTitleColor(MainColor.globalTint, forState: .Normal)
    }
    
    func updateFormStatus() {
        if currentPin != nil && validName && validArea {
            enableAddButton()
        } else {
            disableAddButton()
        }
    }
    
    @IBAction func longPressedOnMap(sender: AnyObject) {
        if sender.state != UIGestureRecognizerState.Began {
            return
        }
        let pinLocation = sender.locationInView(mapView)
        let pinCoordinate = mapView.convertPoint(pinLocation, toCoordinateFromView: mapView)
        dropPinOnCoordinate(pinCoordinate)
    }
    
    func dropPinOnCoordinate(coordinate: CLLocationCoordinate2D) {
        if CuretoLocationHelper.withinBoundary(CuretoArea.HK.boundary(), latitude: coordinate.latitude, longitude: coordinate.longitude) {
            if let pin = currentPin {
                mapView.removeAnnotation(pin)
            }
            let newPin = MKPointAnnotation()
            newPin.coordinate = coordinate
            newPin.title = "\(coordinate.latitude), \(coordinate.longitude)"
            mapView.addAnnotation(newPin)
            currentPin = newPin
            updateFormStatus()
        }
    }
    
    @IBAction func addButtonTapped(sender: AnyObject) {
        guard let controller = addLocationController, pin = currentPin, name = nameTextField.text, area = areaTextField.text else {
            return
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd-HH-mm-ss"
        let dateString = dateFormatter.stringFromDate(NSDate())
        
        let identifier = CuretoGlobal.toBeDetermined + dateString
        
        let restaurant = Restaurant(identifier: identifier, name: name, latitude: pin.coordinate.latitude, longitude: pin.coordinate.longitude)
        restaurant.city = area
        restaurant.country = "Hong Kong"
        
        navigationController?.popViewControllerAnimated(true)
        
        controller.addNewLocation(restaurant)
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
}
