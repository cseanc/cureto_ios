//
//  ArticleController.swift
//  Cureto
//
//  Created by Sean on 11/20/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import MapKit
import Haneke
import SafariServices

class ArticleController: UITableViewController, UIGestureRecognizerDelegate {
    
    // For navigation transition
    var coverImage: UIImage?
    var presenting: Bool = false
    var coverCell: CoverPhotoDisplay?
    
    // For article edit
    var articleIdForEdit: Int?
    var coverPhoto: UIImage?
    var titleText = ""
    var contentText = ""
    var originalSubcontent = [PhotoWithSubtitle]()
    var subcontent = [PhotoWithSubtitle]()
    var restaurant: Restaurant?
    var mealType: CuretoMealType?
    var mealPrice = ""
    var foodType = 0
    
    // For menu action
    var starred = false
    var pinned = false
    var starImage: UIImageView?
    var pinImage: UIImageView?
    var starCountLabel: UILabel?
    
    var selectedArticle: Article?
    
    var articleMenu: ArticleMenu?
    var articleMenuHidden: Bool = false
    
    var lastContentOffset: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ArticleController.putBackCoverImage(_:)), name: "PushAnimatorComplete", object: nil)
                
        navigationItem.title = "Article"
        if let draft = selectedArticle?.draft {
            if draft {
                navigationItem.title = "Draft"
            }
        }
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500.0
        
        let tabBarHeight = tabBarController?.tabBar.bounds.height
        
        if let height = tabBarHeight {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70 - height, right: 0)
        }
        
        tableView.registerNib(UINib(nibName: "CoverPhoto-Display", bundle: nil), forCellReuseIdentifier: "CoverCell")
        tableView.registerNib(UINib(nibName: "TitleAndContent-Display", bundle: nil), forCellReuseIdentifier: "TitleContentCell")
        tableView.registerNib(UINib(nibName: "SubPhotoAndSubtitle-Display", bundle: nil), forCellReuseIdentifier: "SubPhotoTitleCell")
        tableView.registerNib(UINib(nibName: "MapTableViewCell", bundle: nil), forCellReuseIdentifier: "MapCell")
        
        addFooterView()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(articleTableViewReloadRequired(_:)), name: "ArticleTableViewReloadRequired", object: nil)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBarHidden = false
        navigationController?.hidesBarsOnSwipe = true
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.addStatusBar()
        
        NSNotificationCenter.defaultCenter().postNotificationName("MiddleButtonNotNeeded", object: nil)
        tabBarController?.tabBar.hidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        navigationController?.navigationBarHidden = false
        navigationController?.hidesBarsOnSwipe = false
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.removeStatusBar()
        
        NSNotificationCenter.defaultCenter().postNotificationName("MiddleButtonNeeded", object: nil)
        tabBarController?.tabBar.hidden = false
        removeArticleMenu()
    }
    
    override func viewDidAppear(animated: Bool) {
        addArticleMenu()
        articleMenuHidden = false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "article_to_restaurant_info_segue" || segue.identifier == "article_shortcut_to_restaurant_info_segue" {
            let destination = segue.destinationViewController as! RestaurantController
            destination.currentRestaurant = selectedArticle?.restaurant
            destination.restaurantId = selectedArticle?.restaurant?.id
            
        } else if segue.identifier == "article_to_template_segue" {
            let navController = segue.destinationViewController as! UINavigationController
            if let destination = navController.viewControllers[0] as? TemplateController {
                destination.lastViewController = self
                destination.articleId = articleIdForEdit
                destination.coverPhoto = coverPhoto
                destination.titleText = titleText
                destination.contentText = contentText
                destination.originalSubContent = originalSubcontent
                destination.subContent = subcontent
                destination.selectedRestaurant = restaurant
                destination.selectedMealType = mealType
                destination.selectedMealPrice = mealPrice
                destination.selectedFoodType = foodType
                
                // Booleans
                destination.editMode = true
                if let draft = selectedArticle?.draft {
                    if draft {
                        destination.published = false
                    } else {
                        destination.published = true
                    }
                }
            }
        } else if segue.identifier == "article_to_curator_segue" {
            let destination = segue.destinationViewController as! CuratorShowController
            destination.selectedCurator = selectedArticle?.author
        }
    }
    
}

// MARK: - Handling table view

extension ArticleController {
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let article = selectedArticle {
            return 2 + article.subcontent.count
        } else {
            return 1
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            if let article = selectedArticle {
                if let width = article.coverPhotoWidth, height = article.coverPhotoHeight {
                    let heightRatio = height / width
                    return MainScreen.width * heightRatio
                }
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func putBackCoverImage(notification: NSNotification) {
        coverCell?.coverImageView.image = coverImage
        
        if let fromCollectionView = notification.object as? Bool {
            if fromCollectionView {
                if let photoUrl = selectedArticle?.coverPhotoUrl {
                    if let url = NSURL(string: photoUrl) {
                        coverCell?.coverImageView.hnk_setImageFromURL(url)
                    }
                }
            }
        }
        
        presenting = false
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var returnCell: UITableViewCell!
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("CoverCell", forIndexPath: indexPath) as! CoverPhotoDisplay
            
            cell.coverImageView.image = UIImage(named: "Test_Picture")
            
            if presenting {
                coverCell = cell
                
            } else {
                if let photoUrl = selectedArticle?.coverPhotoUrl {
                    if let url = NSURL(string: photoUrl) {
                        cell.coverImageView.hnk_setImageFromURL(url)
                    }
                }
 
            }
            
            returnCell = cell
            
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("TitleContentCell", forIndexPath: indexPath) as! TitleAndContentDisplay
            
            if let article = selectedArticle {
                if let title = article.title {
                    cell.titleLabel.text = title
                }
                if let content = article.content {
                    cell.contentLabel.textAlignment = NSTextAlignment.Left
                    cell.contentLabel.textColor = MainColor.contentTint
                    let paraStyle = NSMutableParagraphStyle()
                    paraStyle.lineSpacing = CuretoGlobal.lineSpacing
                    let attrs = [NSFontAttributeName: CuretoGlobal.contentFont,
                        NSParagraphStyleAttributeName: paraStyle]
                    let attrContent = NSMutableAttributedString(string: content, attributes: attrs)
                    cell.contentLabel.attributedText = attrContent
                    
                    cell.contentLabel.urlLinkTapHandler = { label, url, range in
                        if url.hasPrefix("http") {
                            if let link = NSURL(string: url) {
                                if #available(iOS 9, *) {
                                    let svc = SFSafariViewController(URL: link)
                                    self.presentViewController(svc, animated: true, completion: nil)
                                } else {
                                    UIApplication.sharedApplication().openURL(link)
                                }
                            }
                        }
                    }
                }
            }
            
            returnCell = cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("SubPhotoTitleCell", forIndexPath: indexPath) as! SubPhotoAndSubtitleDisplay
            
            cell.subPhotoView.image = UIImage(named: "Test_Picture")
            
            if let article = selectedArticle {
                let subcontent = article.subcontent[indexPath.row - 2]
                
                if let width = subcontent.subphotoWidth, height = subcontent.subphotoHeight {
                    let heightRatio = height / width
                    cell.subPhotoViewHeight.constant = MainScreen.width * heightRatio
                }
                
                if let photoUrl = subcontent.subphotoUrl {
                    if let url = NSURL(string: photoUrl) {
                        cell.subPhotoView.hnk_setImageFromURL(url, placeholder: nil, format: nil, failure: { error in
                            
                            }, success: { photo in
                                cell.subPhotoView.image = photo
                        })
                    }
                }
                
                if let subtitle = subcontent.subtitle {
                    cell.subtitleLabel.textAlignment = .Left
                    cell.subtitleLabel.textColor = MainColor.contentTint
                    let paraStyle = NSMutableParagraphStyle()
                    paraStyle.lineSpacing = CuretoGlobal.lineSpacing
                    let attrs = [NSFontAttributeName: CuretoGlobal.contentFont,
                        NSParagraphStyleAttributeName: paraStyle]
                    let attrSubtitle = NSMutableAttributedString(string: subtitle, attributes: attrs)
                    cell.subtitleLabel.attributedText = attrSubtitle

                    cell.subtitleLabel.urlLinkTapHandler = { label, url, range in
                        if url.hasPrefix("http") {
                            if let link = NSURL(string: url) {
                                if #available(iOS 9, *) {
                                    let svc = SFSafariViewController(URL: link)
                                    self.presentViewController(svc, animated: true, completion: nil)
                                } else {
                                    UIApplication.sharedApplication().openURL(link)
                                }
                            }
                        }
                    }

                }
                
            }
            
            returnCell = cell
        }
        
        return returnCell
    }
    
}

// MARK: - Handling scroll view

extension ArticleController {
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if let menu = articleMenu {
            if lastContentOffset < 0.0 {
                // do nothing
            } else if lastContentOffset > scrollView.contentOffset.y {
                unhideArticleMenu(menu)
            } else if lastContentOffset < scrollView.contentOffset.y {
                hideArticleMenu(menu)
            }
            lastContentOffset = scrollView.contentOffset.y
        }

    }
    
}

// MARK: - Custom methods

extension ArticleController {
    
    func addFooterView() {
        let footerView = NSBundle.mainBundle().loadNibNamed("MapTableViewCell", owner: self, options: nil)[0] as! MapTableViewCell
        
        var footerHeight: CGFloat = 486
        
        if let height = tabBarController?.tabBar.frame.height {
            footerHeight -= height
        }
        
        footerView.frame = CGRectMake(0, 0, MainScreen.width, footerHeight)
        
        footerView.separatorHeight.constant = 0.6
        
        footerView.mapButton.addTarget(self, action: #selector(mapTapped(_:)), forControlEvents: .TouchUpInside)
        footerView.userButton.addTarget(self, action: #selector(userTapped(_:)), forControlEvents: .TouchUpInside)
        
        if let article = selectedArticle {
            
            if article.foodType == 0 {
                footerView.mealTypeLabel.text = article.mealType
            } else {
                footerView.mealTypeLabel.text = CuretoMealType.stringForFoodType(article.foodType)
            }
            
            if let mealPrice = article.mealPrice?.roundedFullFigureString() {
                footerView.mealPriceLabel.text = "HK$ \(mealPrice) / person"
            }
            
            if let restaurant = article.restaurant {
                
                if let lat = restaurant.latitude, lng = restaurant.longitude, name = restaurant.name {
                    
                    footerView.restaurantNameLabel.text = name
                    
                    if var addressString = restaurant.address {
                        if let city = restaurant.city {
                            addressString += ", \(city)"
                        }
                        footerView.restaurantAddressLabel.text = addressString
                    }

                    let location = CLLocation(latitude: lat, longitude: lng)
                    let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 1000.0, 1000.0)
                    footerView.mapView.setRegion(coordinateRegion, animated: false)

                    let pin = MKPointAnnotation()
                    pin.coordinate = location.coordinate
                    footerView.mapView.addAnnotation(pin)
                }
            }
            
            if let author = article.author {
                if let displayName = author.displayName {
                    footerView.displayNameLabel.text = displayName
                }
                if let username = author.username {
                    footerView.usernameLabel.text = "@\(username)"
                }
            }
        }
        
        tableView.tableFooterView = footerView
    }
    
    func addArticleMenu() {
        guard let article = selectedArticle else {
            return
        }
        
        if articleMenu == nil {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let menuView = NSBundle.mainBundle().loadNibNamed("ArticleMenu", owner: self, options: nil)[0] as! ArticleMenu
            menuView.frame = CGRectMake(0, MainScreen.height - 70, MainScreen.width, 70)
            menuView.blurView.layer.cornerRadius = 8
            menuView.blurView.layer.masksToBounds = true
            
            appDelegate.window?.addSubview(menuView)
//            menuView.slideInFromBottom()
            menuView.slideInFromBottom(0.35, completionDelegate: nil)
            
            if article.draft {
                menuView.starImageView.image = UIImage(named: "Star_Faded")
                menuView.starButton.enabled = false
                
                menuView.pinImageView.image = UIImage(named: "Pin_Faded")
                menuView.pinButton.enabled = false
                
            } else {
                menuView.starImageView.image = UIImage(named: "Star_Light")
                menuView.starButton.enabled = true
                menuView.starButton.addTarget(self, action: #selector(starTapped(_:)), forControlEvents: .TouchUpInside)
                starImage = menuView.starImageView
                
                menuView.pinImageView.image = UIImage(named: "Pin_Light")
                menuView.pinButton.enabled = true
                menuView.pinButton.addTarget(self, action: #selector(pinTapped(_:)), forControlEvents: .TouchUpInside)
                pinImage = menuView.pinImageView
            }
            
            if article.starsCount > 0 {
                menuView.starCountLabel.text = article.starsCount.humanizedString()
            } else {
                menuView.starCountLabel.text = ""
            }
            starCountLabel = menuView.starCountLabel
            
            getRelationship()
            
            menuView.moreButton.addTarget(self, action: #selector(moreTapped(_:)), forControlEvents: .TouchUpInside)
            articleMenu = menuView
        }
        
    }
    
    func removeArticleMenu() {
        if let menu = articleMenu {
            menu.removeFromSuperview()
            self.articleMenu = nil
        }
    }
    
    func hideArticleMenu(menu: UIView) {
        if !articleMenuHidden {
            UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                menu.frame = CGRectMake(0, MainScreen.height, MainScreen.width, 70)
                }, completion: { finished in
                    self.articleMenuHidden = true
            })
        }
    }
    
    func unhideArticleMenu(menu: UIView) {
        if articleMenuHidden {
            UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                menu.frame = CGRectMake(0, MainScreen.height - 70, MainScreen.width, 70)
                }, completion: { finished in
                    self.articleMenuHidden = false
            })
        }
    }
    
    func getStarCount(label: UILabel) {
        if let id = selectedArticle?.id {
            CuretoRequests.getStarCountForArticle(id, onCompletion: { count, message in
                if count > 0 {
                    label.text = count.humanizedString()
                } else {
                    label.text = ""
                }
            })
        }
    }
    
    func getRelationship() {
        guard let token = CredentialStore.sharedInstance.accessToken, id = selectedArticle?.id else {
            return
        }
        
        CuretoRequests.getArticleRelationshipWithUser(token, articleID: id, onCompletion: {
            starred, pinned, message in
            if starred {
                self.starred = true
                self.starImage?.image = UIImage(named: "Star_Filled")
            }
            if pinned {
                self.pinned = true
                self.pinImage?.image = UIImage(named: "Pin_Filled")
            }
        })
    }
    
    func mapTapped(sender: UIButton) {
        performSegueWithIdentifier("article_to_restaurant_info_segue", sender: self)
    }
    
    func userTapped(sender: UIButton) {
        performSegueWithIdentifier("article_to_curator_segue", sender: self)
    }
    
    func animateMenuObjectForType(type: Int, state: Int, imageView: UIImageView) {
        
        let scale: CGFloat = 0.1
        
        if state == 0 {
            UIView.animateWithDuration(0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
                imageView.transform = CGAffineTransformMakeScale(scale, scale)
                
                }, completion: { finished in
                    
                    if type == 0 {
                        imageView.image = UIImage(named: "Star_Filled")
                    } else {
                        imageView.image = UIImage(named: "Pin_Filled")
                    }
                    
                    UIView.animateWithDuration(0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
                        imageView.transform = CGAffineTransformIdentity
                        
                        }, completion: { finished2 in
                            self.wobbleImageView(imageView)
                    })
            })
            
        } else {
            if type == 0 {
                imageView.image = UIImage(named: "Star_Light")
            } else {
                imageView.image = UIImage(named: "Pin_Light")
            }
            
        }
    }
    
    func wobbleImageView(imageView: UIImageView) {
        let expandAnimation = CABasicAnimation(keyPath: "transform.scale")
        expandAnimation.fromValue = 1
        expandAnimation.toValue = 1.25
        expandAnimation.beginTime = 0
        expandAnimation.duration = 0.1
        expandAnimation.fillMode = kCAFillModeForwards
        
        let shrinkAnimation = CABasicAnimation(keyPath: "transform.scale")
        shrinkAnimation.fromValue = 1.25
        shrinkAnimation.toValue = 1
        shrinkAnimation.beginTime = expandAnimation.beginTime + expandAnimation.duration
        shrinkAnimation.duration = 0.1
        expandAnimation.fillMode = kCAFillModeBackwards
        
        let wobbleAnimationGroup = CAAnimationGroup()
        wobbleAnimationGroup.animations = [expandAnimation, shrinkAnimation]
        wobbleAnimationGroup.duration = shrinkAnimation.beginTime + shrinkAnimation.duration
        wobbleAnimationGroup.repeatCount = 2
        imageView.layer.addAnimation(wobbleAnimationGroup, forKey: nil)
    }
    
    func starTapped(sender: UIButton) {
        guard let token = CredentialStore.sharedInstance.accessToken, username = CredentialStore.sharedInstance.username, id = selectedArticle?.id, countLabel = starCountLabel else {
            CuretoBannerAlert.showMessage("Log in to upvote articles")
            return
        }
        
        if starred {
            CuretoRequests.destroyStar(token, articleID: id, onCompletion: { succeeded, message in
                self.getStarCount(countLabel)
            })
        } else {
            CuretoRequests.createStar(token, articleID: id, onCompletion: { succeeded, message in
                self.getStarCount(countLabel)
                if succeeded {
                    if let recipient = self.selectedArticle?.author?.username, title = self.selectedArticle?.title {
                        if recipient != username {
                            let content = "@\(username) upvoted your article titled \(title)"
                            CuretoRequests.sendMessage(token, recipientUsername: recipient, content: content, onCompletion: { succeeded, message in
                                
                            })
                        }
                    }
                }
            })
        }
        
        // Animate image view
        
        guard let star = starImage else {
            return
        }
        
        if starred {
            starred = false
            animateMenuObjectForType(0, state: 1, imageView: star)
        } else {
            starred = true
            animateMenuObjectForType(0, state: 0, imageView: star)
        }
        
    }
    
    func pinTapped(sender: UIButton) {
        guard let token = CredentialStore.sharedInstance.accessToken, id = selectedArticle?.id else {
            CuretoBannerAlert.showMessage("Log in to pin articles")
            return
        }
        
        if pinned {
            CuretoRequests.destroyPin(token, articleID: id, onCompletion: { succeeded, message in
                if succeeded {
                    NSNotificationCenter.defaultCenter().postNotificationName("PinsReloadNeeded", object: nil)
                }
            })

        } else {
            CuretoRequests.createPin(token, articleID: id, onCompletion: {
                succeeded, message in
                if succeeded {
                    NSNotificationCenter.defaultCenter().postNotificationName("PinsReloadNeeded", object: nil)
//                    if let recipient = self.selectedArticle?.author?.username, title = self.selectedArticle?.title, username = CredentialStore.sharedInstance.username {
//                        if recipient != username {
//                            let content = "@\(username) pinned your article titled \(title)"
//                            CuretoRequests.sendMessage(token, recipientUsername: recipient, content: content, onCompletion: { succeeded, message in
//                                
//                            })
//                        }
//                    }
                }
            })
            
        }
        
        // Animate image view
        
        guard let pin = pinImage else {
            return
        }
        
        if pinned {
            pinned = false
            animateMenuObjectForType(1, state: 1, imageView: pin)
        } else {
            pinned = true
            animateMenuObjectForType(1, state: 0, imageView: pin)
        }
        
    }
    
    func moreTapped(sender: UIButton) {
        guard let username = CredentialStore.sharedInstance.username,
            authorUsername = selectedArticle?.author?.username else {
                CuretoBannerAlert.showMessage("Log in to use this action")
                return
        }
        
        if authorUsername == username {
            presentOwnerAlertController()
        } else {
            presentNonOwnerAlertController()
        }
    }
    
    func backTapped(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func articleTableViewReloadRequired(sender: NSNotification) {
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func presentNonOwnerAlertController() {
        guard let menu = articleMenu else {
            return
        }
        
        hideArticleMenu(menu)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
            self.unhideArticleMenu(menu)
        })
        
        let reportAction = UIAlertAction(title: "Report", style: .Destructive, handler: { action in
            self.presentReportAlert()
        })
        
        let askAction = UIAlertAction(title: "Ask to Revise", style: .Default, handler: { action in
            self.presentAskToReviseAlert()
        })
        
        alertController.addAction(reportAction)
        alertController.addAction(askAction)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func presentOwnerAlertController() {
        guard let menu = articleMenu else {
            return
        }
        
        hideArticleMenu(menu)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
            self.unhideArticleMenu(menu)
        })
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: { action in
            self.presentOwnerDeleteAlert()
        })
        
        let editAction = UIAlertAction(title: "Edit", style: .Default, handler: { action in
            self.prepareForOwnerEdit()
        })
        
        alertController.addAction(deleteAction)
        alertController.addAction(editAction)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func prepareForOwnerEdit() {
        guard let token = CredentialStore.sharedInstance.accessToken, id = self.selectedArticle?.id else {
            return
        }
        
        CuretoProgressHUD.sharedInstance.startMessageSpinning(view, message: "Retrieving article")
        CuretoRequests.retrieveArticle(token, id: id, onCompletion: { result, message in
            if let article = result {
                CuretoArticleHelper.parseArticleForEdit(article, controller: self, onCompletion: { succeeded, message in
                    CuretoProgressHUD.sharedInstance.stopMessageSpinning()
                    if succeeded {
                        self.performSegueWithIdentifier("article_to_template_segue", sender: self)
                    }
                })
                
            } else {
                CuretoProgressHUD.sharedInstance.stopMessageSpinning()
            }
        })
    }
    
    func presentOwnerDeleteAlert() {
        let alertController = UIAlertController(title: "Delete Article", message: "Your article will be deleted", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
        })
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: { action in
            guard let token = CredentialStore.sharedInstance.accessToken, id = self.selectedArticle?.id else {
                return
            }
            CuretoProgressHUD.sharedInstance.startMessageSpinning(self.view, message: "Deleting article")
            CuretoRequests.destroyArticle(token, articleId: id, onCompletion: { succeeded, message in
                CuretoProgressHUD.sharedInstance.stopMessageSpinning()
                self.navigationController?.popToRootViewControllerAnimated(true)
                NSNotificationCenter.defaultCenter().postNotificationName("ArticleTableViewReloadRequired", object: nil)
            })
        })
        
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func presentReportAlert() {
        let alertController = UIAlertController(title: "Report Article", message: "Are you sure to report this article for inappropriate content?", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
        })
        
        let reportAction = UIAlertAction(title: "Report", style: .Destructive, handler: { action in
            guard let token = CredentialStore.sharedInstance.accessToken, username = CredentialStore.sharedInstance.username, id = self.selectedArticle?.id else {
                return
            }
            
            let content = "@\(username) reported article with ID \(id)"
            
            CuretoRequests.sendMessage(token, recipientUsername: CuretoGlobal.adminUsername, content: content, onCompletion: { succeeded, message in
                if succeeded {
                    CuretoAlerts.notifyUserWith(.ArticleReported, viewController: self)
                }
            })
        })
        
        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func presentAskToReviseAlert() {
        let alertController = UIAlertController(title: "Ask To Revise", message: "Ask the author to revise this article for", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
        })
        
        let closedAction = UIAlertAction(title: "Restaurant Closed", style: .Default, handler: { action in
            self.sendAskToReviseMessageForType(0)
        })
        
        let priceAction = UIAlertAction(title: "Incorrect Price", style: .Default, handler: { action in
            self.sendAskToReviseMessageForType(1)
        })
        
        alertController.addAction(closedAction)
        alertController.addAction(priceAction)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func sendAskToReviseMessageForType(type: Int) {
        guard let token = CredentialStore.sharedInstance.accessToken, username = CredentialStore.sharedInstance.username, title = selectedArticle?.title, authorUsername = selectedArticle?.author?.username else {
            return
        }
        
        var content = "@\(username) asked you to revise your article titled \(title) because "
        
        if type == 0 {
            content += "the restaurant has closed"
            CuretoRequests.sendMessage(token, recipientUsername: CuretoGlobal.adminUsername, content: content, onCompletion: { succeeded, message in
                if succeeded {
                    CuretoAlerts.notifyUserWith(.ArticleAskedToRevise, viewController: self)
                }
            })
            
        } else {
            content += "the price seems incorrect"
            CuretoRequests.sendMessage(token, recipientUsername: authorUsername, content: content, onCompletion: { succeeded, message in
                if succeeded {
                    CuretoAlerts.notifyUserWith(.ArticleAskedToRevise, viewController: self)
                }
            })
        }
        
//        CuretoRequests.sendMessage(token, recipientUsername: authorUsername, content: content, onCompletion: { succeeded, message in
//            if succeeded {
//                CuretoAlerts.notifyUserWith(.ArticleAskedToRevise, viewController: self)
//            }
//        })
        
    }
}