//
//  LoginController.swift
//  Cureto
//
//  Created by Sean Choo on 4/15/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import Validator

class LoginController: UIViewController, UITextFieldDelegate {
    
    var textTimer: NSTimer?
    
    var validUsername: Bool = false
    var validPassword: Bool = false
    
    // For password reset
    var emailTextField: UITextField?

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var forgotPasswordLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        formView.layer.cornerRadius = 3
        innerView.layer.cornerRadius = 3
        
        separatorHeight.constant = 0.5
        
        disableLoginButton()

        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginController.forgotPasswordTapped))
        forgotPasswordLabel.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(animated: Bool) {
        usernameTextField.becomeFirstResponder()
    }
    
    func enableLoginButton() {
        loginButton.enabled = true
        loginButton.setTitleColor(MainColor.globalTint, forState: .Normal)
    }
    
    func disableLoginButton() {
        loginButton.enabled = false
        loginButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
    }
    
    func updateFormStatus() {
        if validUsername && validPassword {
            enableLoginButton()
        } else {
            disableLoginButton()
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        textTimer?.invalidate()
        textTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(LoginController.typingPaused), userInfo: textField, repeats: false)
        
        disableLoginButton()
        
        switch textField {
        case usernameTextField:
            let inverseSet = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyz0123456789._").invertedSet
            let components = string.componentsSeparatedByCharactersInSet(inverseSet)
            let filtered = components.joinWithSeparator("")
            return string == filtered
        case passwordTextField:
            let inverseSet = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._+-!@").invertedSet
            let components = string.componentsSeparatedByCharactersInSet(inverseSet)
            let filtered = components.joinWithSeparator("")
            return string == filtered
        default:
            return true
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case usernameTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            view.endEditing(true)
        default:
            return false
        }
        
        return true
    }
    
    func validateUsername(usernameText: String?) {
        if let username = usernameText {
            if username != "" {
                validUsername = true
            } else {
                validUsername = false
            }
            
            updateFormStatus()
        }
    }
    
    func validatePassword(passwordText: String?) {
        if let password = passwordText {
            if password != "" {
                validPassword = true
            } else {
                validPassword = false
            }
            
            updateFormStatus()
        }
    }
    
    func validateEmail(emailText: String?) -> Bool {
        let rule = ValidationRulePattern(pattern: .EmailAddress, failureError: ValidationError(message: "Invalid Email"))
        
        if let email = emailText {
            let result = email.validate(rule: rule)
            switch result {
            case .Valid:
                return true
            case .Invalid(_):
                return false
            }
        }
        
        return false
    }
    
    func typingPaused(timer: NSTimer) {
        if let textField = timer.userInfo as? UITextField {
            switch textField {
            case usernameTextField:
                validateUsername(usernameTextField.text)
            case passwordTextField:
                validatePassword(passwordTextField.text)
            default:
                break
            }
        }
    }
    
    func forgotPasswordTapped(recognizer: UITapGestureRecognizer) {
        let alertController = UIAlertController(title: "Enter your email", message: nil, preferredStyle: .Alert)
        
        let submitAction = UIAlertAction(title: "Reset Password", style: .Destructive, handler: { action in
            self.resetPassword()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        
        alertController.addTextFieldWithConfigurationHandler({ textField in
            textField.placeholder = "Email"
            textField.keyboardType = .EmailAddress
            textField.delegate = self
            self.emailTextField = textField
        })
        alertController.addAction(submitAction)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func resetPassword() {
        guard let email = emailTextField?.text else {
            return
        }
        
        let validEmail = validateEmail(email)
        
        if validEmail {
            CuretoProgressHUD.sharedInstance.startMessageSpinning(view, message: "Requesting password reset...")
            CuretoRequests.resetPassword(email, onCompletion: { succeeded, message in
                CuretoProgressHUD.sharedInstance.stopMessageSpinning()
                if succeeded {
                    CuretoAlerts.notifyUserWith(.PasswordReset, viewController: self)
                } else {
                    CuretoAlerts.notifyUserWith(.EmailNoUser, viewController: self)
                }
            })
            
        } else {
            CuretoAlerts.notifyUserWith(.InvalidEmail, viewController: self)
        }
    }
    
    @IBAction func login(sender: AnyObject) {
        guard let username = usernameTextField.text, password = passwordTextField.text else {
            return
        }
        
        if validUsername && validPassword {
            CuretoProgressHUD.sharedInstance.startSpinning()
            CuretoRequests.login(username, password: password, onCompletion: { succeeded, message in
                CuretoProgressHUD.sharedInstance.stopSpinning()
                if succeeded {
                    self.dismissViewControllerAnimated(true, completion: nil)
                    NSNotificationCenter.defaultCenter().postNotificationName("LoginStatusChanged", object: nil)
                } else {
                    CuretoBannerAlert.showMessage(message)
                }
            })
        }
    }
    
    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}
