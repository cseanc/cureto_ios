//
//  CuratorEditProfileController.swift
//  Cureto
//
//  Created by Sean Choo on 1/20/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuratorEditProfileController: UIViewController {
    
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var bioView: UIView!
    @IBOutlet weak var displayNameTextField: UITextField!
    @IBOutlet weak var bioTextView: UITextView!
    
    var curatorController: CuratorController?
    var curator: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        separatorHeight.constant = 0.5
        bioView.layer.masksToBounds = true
        bioView.layer.cornerRadius = 3
        
        setupUserInfo()        
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBarHidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        navigationController?.navigationBarHidden = true
    }
    
    func setupUserInfo() {
        guard let user = curator else {
            return
        }
        
        if let name = user.displayName {
            displayNameTextField.text = name
        }
        
        if let bio = user.bio {
            bioTextView.text = bio
        }
    }
    
    @IBAction func confirm(sender: AnyObject) {
        if let token = CredentialStore.sharedInstance.accessToken {
            CuretoRequests.editProfile(token, displayName: displayNameTextField.text!, bio: bioTextView.text, onCompletion: {
                result, message in
                self.curatorController?.tableView.reloadSections(CuretoGlobal.tableViewFirstSectionIndex, withRowAnimation: .Automatic)
                self.navigationController?.popViewControllerAnimated(true)
            })
        }
    }
    
}
