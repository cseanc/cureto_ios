//
//  BeACuratorController.swift
//  Cureto
//
//  Created by Sean on 11/11/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import SafariServices

class BeACuratorController: UIViewController {
        
    @IBOutlet weak var onboardFlyer: UIImageView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        separatorHeight.constant = 0.6
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(BeACuratorController.flyerTapped))
        onboardFlyer.addGestureRecognizer(tap)
    }
    
//    override func viewWillAppear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: false)
//    }
//    
//    override func viewWillDisappear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: true)
//    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return false
    }
    
    func flyerTapped(recognizer: UITapGestureRecognizer) {
        if let url = NSURL(string: CuretoUrls.designPhilosophyUrl) {
            if #available(iOS 9, *) {
                let svc = SFSafariViewController(URL: url)
                presentViewController(svc, animated: true, completion: nil)
            } else {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    
}
