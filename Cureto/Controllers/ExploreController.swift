//
//  ExploreController.swift
//  Cureto
//
//  Created by Sean Choo on 7/9/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import Haneke

class ExploreController: UITableViewController {
    
    var viewOption: Int = 1
    var seed = Int(arc4random())
    
    // For areas
    var groupedAreas = [GroupedAreaWithPicture]()
    var selectedArea: AreaWithPicture?
    
    // For curators
    var curators = [User]()
    var nextCurators = [User]()
    var page = 1
    var nextPage = 2
    var safeToLoadNextPage = true
    
    var selectedCurator: User?
    
    var rightBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var searchButton: UIBarButtonItem!
    
    // For segmented title view
    var firstViewOptionButton: UIButton?
    var secondViewOptionButton: UIButton?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setSegmentedTitleView()
        rightBarButtonItem = searchButton
        navigationItem.rightBarButtonItem = nil
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 250
        
        tableView.sectionIndexBackgroundColor = MainColor.lighterGray
        
        tableView.registerNib(UINib(nibName: "Cover-F2", bundle: nil), forCellReuseIdentifier: "AreaCell")
        tableView.registerNib(UINib(nibName: "CuratorTableViewCell", bundle: nil), forCellReuseIdentifier: "CuratorCell")
        
        refreshControl?.addTarget(self, action: #selector(refreshTable(_:)), forControlEvents: .ValueChanged)
        
        reloadTableView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "explore_to_area_segue" {
            let destination = segue.destinationViewController as! ExploreAreaController
            destination.selectedArea = selectedArea
            destination.pinMode = false
        } else if segue.identifier == "explore_to_curator_show_segue" {
            let destination = segue.destinationViewController as! CuratorShowController
            destination.selectedCurator = selectedCurator
        }
    }
    
    func refreshTable(refreshControl: UIRefreshControl) {
        reloadTableView()
    }
    
    func reloadTableView() {
        if viewOption == 1 {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            CuretoRequests.getRestaurantAreasWithPicture({ areasWithPicture, message in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.refreshControl?.endRefreshing()
                self.groupedAreas = areasWithPicture
                self.tableView.reloadData()
            })
        } else {
            page = 1
            nextPage = 2
            safeToLoadNextPage = true
            seed = Int(arc4random())
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            CuretoRequests.discoverCurators(seed, page: page, onCompletion: { result, message in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.refreshControl?.endRefreshing()
                self.curators = result
                self.tableView.reloadData()
                if result.count > 0 {
                    CuretoTableViewHelper.setupLoadMoreForTableView(self.tableView)
                }
                
                CuretoRequests.discoverCurators(self.seed, page: self.nextPage, onCompletion: { result2, message2 in
                    self.nextCurators = result2
                    if result2.count == 0 {
                        self.tableView.tableFooterView = nil
                    }
                })
                
            })
        }
    }
    
    func loadNextPage() {
        if nextCurators.count > 0 {
            let firstRowOfNewPage = curators.count
            curators += nextCurators
            let lastRowOfNewPage = curators.count - 1
            
            var rowsToAdd = [NSIndexPath]()
            for i in firstRowOfNewPage...lastRowOfNewPage {
                let indexPath = NSIndexPath(forRow: i, inSection: 0)
                rowsToAdd.append(indexPath)
            }
            
            tableView.insertRowsAtIndexPaths(rowsToAdd, withRowAnimation: .Left)
            
            page += 1
            nextPage += 1
            nextCurators = []
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            CuretoRequests.discoverCurators(seed, page: nextPage, onCompletion: { result, message in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.nextCurators = result
                self.safeToLoadNextPage = true
            })
            
        } else {
            tableView.tableFooterView = nil
        }
    }
    
    func setSegmentedTitleView() {
        let titleView = NSBundle.mainBundle().loadNibNamed("SegmentedTitleView", owner: self, options: nil)[0] as! SegmentedTitleView
        titleView.frame = CGRect(x: 0, y: 0, width: 174, height: 27)
        titleView.titles("Areas", secondTitle: "Curators")
        
        navigationItem.titleView = titleView
        
        titleView.firstButton.addTarget(self, action: #selector(firstViewOptionTapped(_:)), forControlEvents: .TouchUpInside)
        titleView.secondButton.addTarget(self, action: #selector(secondViewOptionTapped(_:)), forControlEvents: .TouchUpInside)
        
        firstViewOptionButton = titleView.firstButton
        secondViewOptionButton = titleView.secondButton
    }
    
    func firstViewOptionTapped(sender: UIButton) {
        navigationItem.rightBarButtonItem = nil
        
        viewOption = 1
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        reloadTableView()
        if let titleView = navigationItem.titleView as? SegmentedTitleView {
            titleView.viewOptionsChanged(1)
        }
    }
    
    func secondViewOptionTapped(sender: UIButton) {
        navigationItem.rightBarButtonItem = rightBarButtonItem
        
        viewOption = 2
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0)
        reloadTableView()
        if let titleView = navigationItem.titleView as? SegmentedTitleView {
            titleView.viewOptionsChanged(2)
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if viewOption == 1 {
            return groupedAreas.count
        } else {
            return 1
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewOption == 1 {
            let group = groupedAreas[section]
            return group.areas.count
        } else {
            return curators.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cellToDisplay: UITableViewCell!
        
        if viewOption == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("AreaCell", forIndexPath: indexPath) as! CoverF2
            
            let group = groupedAreas[indexPath.section]
            let area = group.areas[indexPath.row]
            cell.nameLabel.text = area.name
            
            cell.coverImageView.image = UIImage(named: CuretoGlobal.placeholderImageName)
            if let urlString = area.pictureUrl {
                if let url = NSURL(string: urlString) {
                    cell.coverImageView.hnk_setImageFromURL(url)
                }
            }
            
            cellToDisplay = cell
            
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("CuratorCell", forIndexPath: indexPath) as! CuratorTableViewCell
            cell.separatorHeight.constant = 0.5
            
            let curator = curators[indexPath.row]
            
            cell.nameLabel.text = curator.displayName
            if let count = curator.articlesCount {
                if count > 1 {
                    cell.countLabel.text = "\(count) articles"
                } else if count == 1 {
                    cell.countLabel.text = "1 article"
                } else {
                    cell.countLabel.text = "No article yet"
                }
            }
            
            cell.coverImageView.image = UIImage(named: CuretoGlobal.placeholderImageName)
            if let urlString = curator.profileImageUrl {
                if let url = NSURL(string: urlString) {
                    cell.placeholderLabel.hidden = true
                    cell.coverImageView.hnk_setImageFromURL(url)
                }
            } else {
                cell.placeholderLabel.hidden = false
                cell.placeholderLabel.text = curator.displayName
            }
            
            cellToDisplay = cell
        }
        
        return cellToDisplay
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if viewOption == 1 {
            let group = groupedAreas[indexPath.section]
            selectedArea = group.areas[indexPath.row]
            performSegueWithIdentifier("explore_to_area_segue", sender: self)
        } else {
            selectedCurator = curators[indexPath.row]
            performSegueWithIdentifier("explore_to_curator_show_segue", sender: self)
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if viewOption == 1 {
            return 35
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if viewOption == 1 {
            let headerView = NSBundle.mainBundle().loadNibNamed("FilterSectionHeaderView", owner: self, options: nil)[0] as! FilterSectionHeaderView
            headerView.frame = CGRectMake(0, 0, MainScreen.width, 35)
            headerView.backgroundColor = MainColor.lighterGray
            
            headerView.bottomSpace.constant = 5
            
            headerView.headerLabel.text = groupedAreas[section].title
            
            return headerView
            
        } else {
            return nil
        }
    }
    
    override func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        if viewOption == 1 {
            var titles = [String]()
            for group in groupedAreas {
                titles.append(group.title)
            }
            return titles
            
        } else {
            return nil
        }
    }
    
}

// MARK: - Handling Scroll View

extension ExploreController {
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        let maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        //        print("offset: \(offset) maxOffset: \(maxOffset)")
        //        print(safeToLoadNextPage)
        //
        //        print(round(offset))
        //        print(round(maxOffset))
        
        let rOffset = round(offset)
        let rMaxOffset = round(maxOffset)
        
        if rOffset == rMaxOffset && safeToLoadNextPage {
            safeToLoadNextPage = false
            NSTimer.scheduledTimerWithTimeInterval(CuretoGlobal.paginationDelay, target: self, selector: #selector(ExploreController.loadNextPage), userInfo: nil, repeats: false)
        }
    }
}
