//
//  CuratorGridController.swift
//  Cureto
//
//  Created by Sean Choo on 7/10/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuratorGridController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var refreshControl: UIRefreshControl?
    
    var articles = [Article]()
    var nextArticles = [Article]()
    
    var nextPageAvailable = true
    
    var selectedArticle: Article?
    
    
    // For pagination
    var page: Int = 1
    var nextPage: Int = 2
    var safeToLoadNextPage: Bool = true
    
    
    // For navigation transition
    var navigationBarHeight: CGFloat = 64
    var initialFrame = CGRectZero
    var toImageView: UIImageView?

    var showMode: Bool = false
    
    // From curator controller
    var curatorController: CuratorController?
    var previousViewIndex = 0
    
    // From curator show controller
    var curatorShowController: CuratorShowController?
    
    var selectedUsername: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.alwaysBounceVertical = true
        collectionView?.registerNib(UINib(nibName: "Cover-G1", bundle: nil), forCellWithReuseIdentifier: "ArticleCell")
        
        if showMode {
            selectedUsername = curatorShowController?.selectedCurator?.username
        } else {
            selectedUsername = CredentialStore.sharedInstance.username
        }
        
        reloadArticles()
    }
    
    override func viewWillAppear(animated: Bool) {
        // For navigation transition
        navigationController?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        if refreshControl == nil {
            refreshControl = UIRefreshControl()
            refreshControl?.addTarget(self, action: #selector(refreshCollectionView(_:)), forControlEvents: .ValueChanged)
            collectionView?.addSubview(refreshControl!)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "curator_grid_to_article_segue" {
            let destination = segue.destinationViewController as! ArticleController
            destination.selectedArticle = selectedArticle
            destination.coverImage = toImageView?.image
            destination.presenting = true
        }
    }
    
    
    func refreshCollectionView(refreshControl: UIRefreshControl) {
        reloadArticles()
    }
    
    func reloadArticles() {
        guard let username = selectedUsername else {
            return
        }
        
        page = 1
        nextPage = 2
        safeToLoadNextPage = true
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        CuretoRequests.getUserArticles(username, page: page, onCompletion: { result, message in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            self.refreshControl?.endRefreshing()
            self.articles = result
            self.collectionView?.reloadData()
            
            CuretoRequests.getUserArticles(username, page: self.nextPage, onCompletion: { result2, message2 in
                self.nextArticles = result2
                if result2.count == 0 {
                    self.nextPageAvailable = false
                    self.collectionView?.reloadData()
                }
            })
        })
    }
    
    func loadNextPage() {
        guard let username = selectedUsername else {
            return
        }
        
        if nextArticles.count > 0 {
            let firstItemOfNewPage = articles.count
            articles += nextArticles
            let lastItemOfNewPage = articles.count - 1
            
            var itemsToAdd = [NSIndexPath]()
            for i in firstItemOfNewPage...lastItemOfNewPage {
                let indexPath = NSIndexPath(forItem: i, inSection: 0)
                itemsToAdd.append(indexPath)
            }
            
            collectionView?.insertItemsAtIndexPaths(itemsToAdd)
            
            page += 1
            nextPage += 1
            nextArticles = []
            
            CuretoRequests.getUserArticles(username, page: nextPage, onCompletion: { result, message in
                self.nextArticles = result
                self.safeToLoadNextPage = true
            })
            
        } else {
            // Remove collection view footer
            nextPageAvailable = false
            collectionView?.reloadData()
        }

    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return articles.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var width: CGFloat = 154
        if MainScreen.width % 2 == 0 {
            width = (MainScreen.width - 12) / 2
        } else {
            width = (MainScreen.width - 11) / 2
        }
        let height = width / 182 * 260
        return CGSizeMake(width, height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        if MainScreen.width % 2 == 0 {
            return 4
        } else {
            return 3
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 6, left: 4, bottom: 12, right: 4)
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ArticleCell", forIndexPath: indexPath) as! CoverG1
        cell.contentView.layer.borderWidth = 0.5
        cell.contentView.layer.borderColor = MainColor.separatorTint.CGColor
        cell.separatorHeight.constant = 0.5
        
        if MainScreen.width == 320 {
            cell.authorLabelTopSpace.constant = 2
            cell.titleLabelTopSpace.constant = 0
            cell.titleLabelBottomSpace.constant = 0
        }
        
        let article = articles[indexPath.item]
        cell.imageView.image = UIImage(named: CuretoGlobal.placeholderImageName)
        if let imageUrl = article.coverPhotoThumbUrl {
            if let url = NSURL(string: imageUrl) {
                cell.imageView.hnk_setImageFromURL(url)
            }
        }
        cell.authorLabel.text = article.restaurant?.city
        cell.titleLabel.text = article.title
        cell.locationLabel.text = article.restaurant?.name
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if nextPageAvailable {
            return CGSize(width: MainScreen.width, height: 50)
        } else {
            return CGSize(width: MainScreen.width, height: 0)
        }
    }
    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let footerView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterView", forIndexPath: indexPath)
        return footerView
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        selectedArticle = articles[indexPath.item]
        
        // For navigation transitioning
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CoverG1
        initialFrame = cell.convertRect(cell.imageView.frame, toView: nil)
        
        if let width = selectedArticle?.coverPhotoWidth, height = selectedArticle?.coverPhotoHeight {
            let heightRatio = height / width
            toImageView = UIImageView(frame: CGRectMake(0, navigationBarHeight, MainScreen.width, MainScreen.width * heightRatio))
            toImageView?.image = cell.imageView.image
        }
        
        performSegueWithIdentifier("curator_grid_to_article_segue", sender: self)
    }
    

    @IBAction func back(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: {
            if self.showMode {
                if let controller = self.curatorShowController {
                    controller.viewOptions?.selectedSegmentIndex = 0
                }
            } else {
                if let controller = self.curatorController {
                    controller.viewOptions?.selectedSegmentIndex = self.previousViewIndex
                }
            }
        })
    }
    
}

extension CuratorGridController: UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    // For navigation transition
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        guard let _ = toVC as? ArticleController else {
            return nil
        }
        
        if operation == UINavigationControllerOperation.Push {
            let pushAnimator = CuretoPushAnimator()
            pushAnimator.initialFrame = initialFrame
            pushAnimator.toImageView = toImageView
            pushAnimator.fromCollectionView = true
            pushAnimator.tabBarPresent = false
            return pushAnimator
        }
        
        return nil
    }
    
}

// MARK: - Handling Scroll View

extension CuratorGridController {
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        let maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
//        print("offset: \(offset) maxOffset: \(maxOffset)")
//        print(safeToLoadNextPage)
//
//        print(round(offset))
//        print(round(maxOffset))
        
        let rOffset = round(offset)
        let rMaxOffset = round(maxOffset)
        
        if rOffset == rMaxOffset && safeToLoadNextPage {
            safeToLoadNextPage = false
            NSTimer.scheduledTimerWithTimeInterval(CuretoGlobal.paginationDelay, target: self, selector: #selector(CuratorGridController.loadNextPage), userInfo: nil, repeats: false)
        }
    }
}
