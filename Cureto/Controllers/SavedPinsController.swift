//
//  SavedPinsController.swift
//  Cureto
//
//  Created by Sean Choo on 7/9/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import Haneke

class SavedPinsController: UITableViewController {
    
    var presentedPinTutorial: UIView?

    var groupedAreas = [GroupedAreaWithPicture]()
    var selectedArea: AreaWithPicture?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(pinsReloadNeeded(_:)), name: "PinsReloadNeeded", object: nil)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 250
        
        tableView.sectionIndexBackgroundColor = MainColor.lighterGray
        
        tableView.registerNib(UINib(nibName: "Cover-F2", bundle: nil), forCellReuseIdentifier: "AreaCell")
        
        refreshControl?.addTarget(self, action: #selector(refreshTable(_:)), forControlEvents: .ValueChanged)
        
        reloadPinAreas()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "saved_pins_to_explore_area_segue" {
            let destination = segue.destinationViewController as! ExploreAreaController
            destination.selectedArea = selectedArea
            destination.pinMode = true
        }
    }
    
    func pinsReloadNeeded(notification: NSNotification) {
        reloadPinAreas()
    }
    
    func refreshTable(refreshControl: UIRefreshControl) {
        reloadPinAreas()
    }
    
    func reloadPinAreas() {
        guard let token = CredentialStore.sharedInstance.accessToken else {
            return
        }
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        CuretoRequests.getUserPinsAreasWithPicture(token, onCompletion: { groupedAreas, message in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            self.refreshControl?.endRefreshing()
            self.groupedAreas = groupedAreas
            self.tableView.reloadData()
        })
    }
    
    func presentPinTutorial() {
        guard let navController = navigationController else {
            return
        }
        
        if presentedPinTutorial == nil {
            let width: CGFloat = 145
            let height: CGFloat = width
            let tutorialView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
            tutorialView.center = navController.view.center
            let tutorialImageView = UIImageView(frame: CGRectMake(0, 0, width, height))
            tutorialImageView.contentMode = .ScaleAspectFit
            tutorialImageView.clipsToBounds = true
            tutorialImageView.image = UIImage(named: "Pin_Tutorial")
            tutorialImageView.layer.masksToBounds = true
            tutorialImageView.layer.cornerRadius = 7
            tutorialView.addSubview(tutorialImageView)
            navController.view.addSubview(tutorialView)
            presentedPinTutorial = tutorialView
        }
    }
    
    func removePinTutorial() {
        presentedPinTutorial?.removeFromSuperview()
        presentedPinTutorial = nil
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return groupedAreas.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let group = groupedAreas[section]
        return group.areas.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AreaCell", forIndexPath: indexPath) as! CoverF2
        
        let group = groupedAreas[indexPath.section]
        let area = group.areas[indexPath.row]
        
        cell.nameLabel.text = area.name
        
        cell.coverImageView.image = UIImage(named: CuretoGlobal.placeholderImageName)
        if let urlString = area.pictureUrl {
            if let url = NSURL(string: urlString) {
                cell.coverImageView.hnk_setImageFromURL(url)
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let group = groupedAreas[indexPath.section]
        selectedArea = group.areas[indexPath.row]
        
        performSegueWithIdentifier("saved_pins_to_explore_area_segue", sender: self)
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = NSBundle.mainBundle().loadNibNamed("FilterSectionHeaderView", owner: self, options: nil)[0] as! FilterSectionHeaderView
        headerView.frame = CGRectMake(0, 0, MainScreen.width, 35)
        headerView.backgroundColor = MainColor.lighterGray
        
        headerView.bottomSpace.constant = 5
        
        headerView.headerLabel.text = groupedAreas[section].title
        
        return headerView
    }
    
    override func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        if groupedAreas.count == 0 {
            navigationController?.popToRootViewControllerAnimated(true)
            tabBarController?.tabBar.hidden = false
            presentPinTutorial()
            return nil
        }
        
        removePinTutorial()
        
        var titles = [String]()
        
        for group in groupedAreas {
            titles.append(group.title)
        }
        
        return titles
    }
    
}
