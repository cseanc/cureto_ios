//
//  TemplateController.swift
//  Cureto
//
//  Created by Sean on 11/17/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import CoreLocation

class TemplateController: UIViewController {
    
    // For raising keyboard at initial load
//    var initialLoad = true
    var titleTextField: UITextField?
    
    // Code for edit requirements
    var coverDidChange = false
    
    // Booleans for article edit
    var editMode = false
    var published = false
    
    // For article edit
    var lastViewController: UIViewController?
    var articleId: Int?
    var originalSubContent = [PhotoWithSubtitle]()
    
    // For publish controller
    var coverPhotoLocation: CLLocation?
    var selectedRestaurant: Restaurant?
    var selectedMealType: CuretoMealType?
    var selectedMealPrice = ""
    var selectedFoodType = 0
    
    // For subcontent controller
    var selectedSubcontent: PhotoWithSubtitle?
    var selectedSubcontentTag: Int = 0
    
    // For template controller
    var coverPhoto: UIImage?
    var titleText: String = ""
    var contentText: String = ""
    
    var imagePickerController = UIImagePickerController()
    var subImagePickerController = UIImagePickerController()
    var textTimer: NSTimer?
    
    var articleHead: ArticleHead?
    var subContent: [PhotoWithSubtitle] = []
    
    @IBOutlet weak var designTableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var topSeparatorHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topSeparatorHeight.constant = 0.5
        
        imagePickerController.delegate = self
        subImagePickerController.delegate = self
        
        designTableView.rowHeight = UITableViewAutomaticDimension
        designTableView.estimatedRowHeight = 500.0
        
        designTableView.registerNib(UINib(nibName: "CoverPhoto", bundle: nil), forCellReuseIdentifier: "CoverCell")
        designTableView.registerNib(UINib(nibName: "TitleAndContent", bundle: nil), forCellReuseIdentifier: "TitleContentCell")
        designTableView.registerNib(UINib(nibName: "SubPhotoAndSubtitle", bundle: nil), forCellReuseIdentifier: "SubPhotoTitleCell")
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(templateContentChanged(_:)), name: "TemplateContentChanged", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(textFieldDidChanged(_:)), name: "UITextFieldTextDidChangeNotification", object: nil)
        
        nextButton.enabled = false
        nextButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
        
    }
    
    override func viewWillAppear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: false)
        NSNotificationCenter.defaultCenter().postNotificationName("TemplateContentChanged", object: nil)
    }
    
//    override func viewWillDisappear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: true)
//    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "template_to_content_segue" {
            let destination = segue.destinationViewController as! ContentController
            destination.templateController = self
            
        } else if segue.identifier == "template_to_subcontent_edit_segue" {
            let destination = segue.destinationViewController as! SubcontentEditController
            destination.templateController = self
            destination.subcontentRow = selectedSubcontentTag
            destination.selectedSubcontent = selectedSubcontent
            
        } else if segue.identifier == "template_to_publish_segue" {
            let destination = segue.destinationViewController as! PublishController
            
            // Must
            destination.articleController = lastViewController
            destination.templateController = self
            destination.coverPhotoLocation = coverPhotoLocation
            destination.articleHead = articleHead
            destination.originalSubContent = originalSubContent
            destination.subContent = subContent
            destination.foodType = selectedFoodType
            
            // Booleans
            destination.editMode = editMode
            destination.published = published
            destination.coverDidChange = coverDidChange
            
            // Optional
            destination.location = selectedRestaurant
            destination.mealType = selectedMealType
            destination.mealPriceString = selectedMealPrice
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func templateContentChanged(notification: NSNotification) {
        if let _ = coverPhoto {
            if titleText != "" && contentText != "" {
                nextButton.enabled = true
                nextButton.setTitleColor(MainColor.globalTint, forState: .Normal)
            } else {
                nextButton.enabled = false
                nextButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
            }
        } else {
            nextButton.enabled = false
            nextButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
        }
    }
    
    func reloadTitleAndContent() {
        let indexPath = NSIndexPath(forRow: 1, inSection: 0)
        designTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }
    
    func insertSubcontentAtIndexPath(indexPath: NSIndexPath) {
        designTableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        scrollTableViewToLastCell()
    }
    
    func reloadSubcontentAtIndexPath(indexPath: NSIndexPath) {
        designTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }
    
    func deleteSubcontentAtIndexPath(indexPath: NSIndexPath) {
        designTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        scrollTableViewToLastCell()
    }

    @IBAction func next(sender: AnyObject) {
        if let photo = coverPhoto {
            if titleText != "" && contentText != "" {
                articleHead = ArticleHead(title: titleText, content: contentText, coverPhoto: photo)
                articleHead?.id = articleId
                performSegueWithIdentifier("template_to_publish_segue", sender: self)
            }
        }
    }
    
    @IBAction func cancel(sender: AnyObject) {
        presentCancelAlert()
    }
    
}

// MARK: - Handling table view

extension TemplateController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + subContent.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            if let photo = coverPhoto {
                let heightRatio = photo.size.height / photo.size.width
                return MainScreen.width * heightRatio
            } else {
                return MainScreen.width / 3 * 2
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cellToShow: UITableViewCell!
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("CoverCell", forIndexPath: indexPath) as! CoverPhoto
            cell.addCoverPhotoButton.addTarget(self, action: #selector(addCoverPhotoButtonTapped(_:)), forControlEvents: .TouchUpInside)
            cell.changeCoverPhotoButton.addTarget(self, action: #selector(changeCoverPhotoButtonTapped(_:)), forControlEvents: .TouchUpInside)
            
            if let photo = coverPhoto {
                cell.coverImageView.image = photo
                cell.addCoverPhotoButton.hidden = true
                cell.addCoverPhotoLabel.hidden = true
                cell.changeCoverPhotoButton.hidden = false
            } else {
                if let assetImage = UIImage(named: "Test_Picture") {
                    cell.coverImageView.image = assetImage
                }
                cell.addCoverPhotoButton.hidden = false
                cell.addCoverPhotoLabel.hidden = false
                cell.changeCoverPhotoButton.hidden = true
            }
            
            cellToShow = cell
            
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("TitleContentCell", forIndexPath: indexPath) as! TitleAndContent
            
            cell.titleTextField.delegate = self
            cell.titleTextField.text = titleText
            titleTextField = cell.titleTextField
                        
            if contentText == "" {
                cell.contentLabel.textAlignment = NSTextAlignment.Center
                cell.contentLabel.textColor = MainColor.placeholderTint
                let attrs = [NSFontAttributeName: CuretoGlobal.contentFont]
                let attrContent = NSMutableAttributedString(string: "Enter your content here", attributes: attrs)
                cell.contentLabel.attributedText = attrContent
            } else {
                cell.contentLabel.textAlignment = NSTextAlignment.Left
                cell.contentLabel.textColor = MainColor.contentTint
                let paraStyle = NSMutableParagraphStyle()
                paraStyle.lineSpacing = CuretoGlobal.lineSpacing
                let attrs = [NSFontAttributeName: CuretoGlobal.contentFont,
                    NSParagraphStyleAttributeName: paraStyle]
                let attrContent = NSMutableAttributedString(string: contentText, attributes: attrs)
                cell.contentLabel.attributedText = attrContent
            }
            
            cell.contentButton.addTarget(self, action: #selector(contentTapped(_:)), forControlEvents: .TouchUpInside)
            
            cellToShow = cell
            
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("SubPhotoTitleCell", forIndexPath: indexPath) as! SubPhotoAndSubtitle
            
            cell.subcontentEditButton.tag = indexPath.row
            cell.subcontentEditButton.addTarget(self, action: #selector(subcontentEditButtonTapped(_:)), forControlEvents: .TouchUpInside)
            
            let photoWithSubtitle = subContent[indexPath.row - 2]
            
//            print("Subphoto did change: \(photoWithSubtitle.subphotoDidChange) Row: \(indexPath.row)")
            
            if let photo = photoWithSubtitle.photo {
                let heightRatio = photo.size.height / photo.size.width
                cell.subPhotoViewHeight.constant = MainScreen.width * heightRatio
                cell.subPhotoView.image = photo
            }
            
            if let subtitle = photoWithSubtitle.subtitle {
                cell.subtitleLabel.textAlignment = .Left
                cell.subtitleLabel.textColor = MainColor.contentTint
                let paraStyle = NSMutableParagraphStyle()
                paraStyle.lineSpacing = CuretoGlobal.lineSpacing
                let attrs = [NSFontAttributeName: CuretoGlobal.contentFont,
                    NSParagraphStyleAttributeName: paraStyle]
                let attrSubtitle = NSMutableAttributedString(string: subtitle, attributes: attrs)
                cell.subtitleLabel.attributedText = attrSubtitle
            }
            
            cellToShow = cell
            
        }
        
        return cellToShow
    }
        
}

// MARK: - Handling image picker

extension TemplateController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func pickImageFromLibraryForType(type: Int) {
        var imagePickerToPresent: UIImagePickerController!
        
        if type == 0 {
            imagePickerToPresent = imagePickerController
        } else if type == 1 {
            imagePickerToPresent = subImagePickerController
        }
        
        imagePickerToPresent.sourceType = .PhotoLibrary
        presentViewController(imagePickerToPresent, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if picker == imagePickerController {
            coverDidChange = true
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                if let url = info[UIImagePickerControllerReferenceURL] as? NSURL {
                    CuretoImageHelper.coordinateForImageWithUrl(url, onCompletion: { location in
                        self.coverPhotoLocation = location
                    })
                }
                
                if let photo = CuretoImageHelper.resizeImageWithMaxWidth(pickedImage, maxWidth: 1080.0, quality: 0.95) {
                    coverPhoto = photo
                    let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                    self.designTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                    dismissViewControllerAnimated(true, completion: nil)
                }
            }
        } else if picker == subImagePickerController {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                if let photo = CuretoImageHelper.resizeImageWithMaxWidth(pickedImage, maxWidth: 1080.0, quality: 0.95) {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let subPhotoTitleController = storyboard.instantiateViewControllerWithIdentifier("SubPhotoTitleController") as! SubPhotoTitleController
                    subPhotoTitleController.templateController = self
                    subPhotoTitleController.subPhoto = photo
                    
                    picker.pushViewController(subPhotoTitleController, animated: true)
                }
            }
        }
    }
    
}

// MARK: - Handling textfield/textview

extension TemplateController: UITextFieldDelegate {
    
//    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        textTimer?.invalidate()
//        textTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(typingPaused(_:)), userInfo: textField, repeats: false)
//        return true
//    }
    
    func textFieldDidChanged(notification: NSNotification) {
        if let textField = notification.object as? UITextField, titleField = titleTextField {
            if textField == titleField {
                textTimer?.invalidate()
                textTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(typingPaused(_:)), userInfo: textField, repeats: false)
            }
        }
    }
    
    func typingPaused(timer: NSTimer) {
        if let textField = timer.userInfo as? UITextField {
            if let text = textField.text {
                if text != "" {
                    titleText = text
                } else {
                    titleText = ""
                }
            }
            NSNotificationCenter.defaultCenter().postNotificationName("TemplateContentChanged", object: nil)
        }
    }
    
}

// MARK: - Custom methods

extension TemplateController {
    
    func addCoverPhotoButtonTapped(sender: UIButton) {
        pickImageFromLibraryForType(0)
    }
    
    func changeCoverPhotoButtonTapped(sender: UIButton) {
        pickImageFromLibraryForType(0)
    }
    
    @IBAction func addSubPhotoButtonTapped(sender: UIButton) {
        pickImageFromLibraryForType(1)
    }
    
    func contentTapped(sender: UIButton) {
        performSegueWithIdentifier("template_to_content_segue", sender: self)
    }
    
    func subcontentEditButtonTapped(sender: UIButton) {
        let index = sender.tag - 2
        selectedSubcontent = subContent[index]
        selectedSubcontentTag = sender.tag
        performSegueWithIdentifier("template_to_subcontent_edit_segue", sender: sender)
    }
    
    func presentCancelAlert() {
        let alertController = UIAlertController(title: "Discard Changes", message: "Your changes will be discarded", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let discardAction = UIAlertAction(title: "Discard", style: .Destructive, handler: { action in
            self.dismissViewControllerAnimated(true, completion: nil)
        })

        alertController.addAction(discardAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }

    func scrollTableViewToLastCell() {
        let lastRowNumber = designTableView.numberOfRowsInSection(0) - 1
        let lastRowIndexPath = NSIndexPath(forRow: lastRowNumber, inSection: 0)
        designTableView.scrollToRowAtIndexPath(lastRowIndexPath, atScrollPosition: .Top, animated: true)
    }
}