//
//  SubPhotoTitleController.swift
//  Cureto
//
//  Created by Sean on 11/26/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class SubPhotoTitleController: UIViewController, UIGestureRecognizerDelegate {
    
    // For raising keyboard at initial load
    var initialLoad = true
    
    var textViewTimer: NSTimer?
    var templateController: TemplateController?
    
    var subPhoto: UIImage?
    var subtitle: String = ""
    
    @IBOutlet weak var subTableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        separatorHeight.constant = 0.5
        
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        subTableView.rowHeight = UITableViewAutomaticDimension
        subTableView.estimatedRowHeight = 500.0
        
        subTableView.registerNib(UINib(nibName: "SubPhoto", bundle: nil), forCellReuseIdentifier: "SubPhotoCell")
        subTableView.registerNib(UINib(nibName: "Subtitle", bundle: nil), forCellReuseIdentifier: "SubtitleCell")
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(subContentChanged(_:)), name: "SubContentChanged", object: nil)

        addButton.enabled = false
        addButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
    }
    
    override func viewWillAppear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: false)
        navigationController?.navigationBarHidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        if initialLoad {
            initialLoad = false
            scrollTableViewToLastCell()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: true)
        navigationController?.navigationBarHidden = false
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func subContentChanged(notification: NSNotification) {
        if let _  = subPhoto {
            if subtitle == "" {
                addButton.enabled = false
                addButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
            } else {
                addButton.enabled = true
                addButton.setTitleColor(MainColor.globalTint, forState: .Normal)
            }
        }
    }

    @IBAction func add(sender: AnyObject) {
        guard let photo = subPhoto, template = templateController else {
            return
        }
        
        if subtitle != "" {
            let photoWithSubtitle = PhotoWithSubtitle(photo: photo, subtitle: subtitle)
            let row = template.subContent.count + 2
            template.subContent.append(photoWithSubtitle)
            let indexPath = NSIndexPath(forRow: row, inSection: 0)
            dismissViewControllerAnimated(true, completion: {
                template.insertSubcontentAtIndexPath(indexPath)
            })
        }
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
}

// MARK: - Handling table view

extension SubPhotoTitleController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            if let photo = subPhoto {
                let heightRatio = photo.size.height / photo.size.width
                return MainScreen.width * heightRatio
            } else {
                return MainScreen.width / 4 * 3
            }
        }
        return UITableViewAutomaticDimension
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cellToShow: UITableViewCell!
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("SubPhotoCell", forIndexPath: indexPath) as! SubPhoto
            if let photo = subPhoto {
                cell.subPhotoView.image = photo
            }
            cellToShow = cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("SubtitleCell", forIndexPath: indexPath) as! Subtitle
            cell.subtitleTextView.delegate = self
            
            let paraStyle = NSMutableParagraphStyle()
            paraStyle.lineSpacing = CuretoGlobal.lineSpacing
            let attrs = [NSFontAttributeName: CuretoGlobal.contentFont,
                NSParagraphStyleAttributeName: paraStyle]
            
            cell.subtitleTextView.typingAttributes = attrs
            cell.subtitleTextView.textAlignment = .Left
                        
            if subtitle == "" {
                cell.subtitleTextView.attributedText = NSMutableAttributedString(string: "Enter description here", attributes: attrs)
                cell.subtitleTextView.textColor = MainColor.placeholderTint
            } else {
                cell.subtitleTextView.attributedText = NSMutableAttributedString(string: subtitle, attributes: attrs)
                cell.subtitleTextView.textColor = MainColor.contentTint
            }
            
            cellToShow = cell
        }
        return cellToShow
    }
    
    func scrollTableViewToLastCell() {
        let lastRowNumber = subTableView.numberOfRowsInSection(0) - 1
        let lastRowIndexPath = NSIndexPath(forRow: lastRowNumber, inSection: 0)
        subTableView.scrollToRowAtIndexPath(lastRowIndexPath, atScrollPosition: .Top, animated: true)
    }
    
}

// MARK: - Handling textview

extension SubPhotoTitleController: UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if subtitle == "" {
            textView.text = ""
        } else {
            textView.text = subtitle
        }
        textView.textColor = UIColor.blackColor()
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        textViewTimer?.invalidate()
        textViewTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(typingPaused(_:)), userInfo: textView, repeats: false)
        return true
    }
    
    func typingPaused(timer: NSTimer) {
        if let textView = timer.userInfo as? UITextView {
            if textView.text != "" {
                subtitle = textView.text
            } else {
                subtitle = ""
            }
            NSNotificationCenter.defaultCenter().postNotificationName("SubContentChanged", object: nil)
        }
    }
    
}