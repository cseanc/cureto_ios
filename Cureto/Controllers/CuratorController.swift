//
//  CuratorController.swift
//  Cureto
//
//  Created by Sean on 11/17/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import Haneke
import RSKImageCropper

class CuratorController: CuretoTableViewController {
    
    var curator: User?
    
    var selectedViewIndex: Int = 0
    
    var imagePickerController: UIImagePickerController!
    
    var headerBlurView: UIVisualEffectView?
    var profileImageView: UIImageView?
    var usernameLabel: UILabel?
    var nameLabel: UILabel?
    var bioLabel: UILabel?
    var statsLabel: UILabel?
    var placeholderLabel: UILabel?
    var viewOptions: UISegmentedControl?
    
    // For messages
    var dotView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(articleTableViewReloadRequired(_:)), name: "ArticleTableViewReloadRequired", object: nil)
        
        tableView.registerNib(UINib(nibName: "CuratorTableViewHeader", bundle: nil), forCellReuseIdentifier: "CuratorHeader")
        tableView.registerNib(UINib(nibName: "Cover-A1", bundle: nil), forCellReuseIdentifier: "ArticleCell")
        tableView.registerNib(UINib(nibName: "Cover-A1", bundle: nil), forCellReuseIdentifier: "DraftCell")
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500.0
        
        refreshControl?.addTarget(self, action: #selector(refreshTable(_:)), forControlEvents: .ValueChanged)
        
        reloadArticles()
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBarHidden = true
        tabBarController?.tabBar.hidden = false
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: false)
        
        // For navigation transition
        navigationController?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
    }
    
    override func viewDidAppear(animated: Bool) {
        handleMessagesDot(dotView)
    }
    
//    override func viewWillDisappear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: true)
//    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "curator_to_edit_profile_segue" {
            let destination = segue.destinationViewController as! CuratorEditProfileController
            destination.curatorController = self
            destination.curator = curator
        } else if segue.identifier == "curator_to_article_segue" {
            let destination = segue.destinationViewController as! ArticleController
            destination.selectedArticle = selectedArticle
            destination.coverImage = toImageView?.image
            destination.presenting = true
            
        } else if segue.identifier == "curator_to_grid_segue" {
            let navController = segue.destinationViewController as! UINavigationController
            if let destination = navController.viewControllers[0] as? CuratorGridController {
                destination.curatorController = self
                destination.previousViewIndex = selectedViewIndex
    
                destination.showMode = false
                
                if let displayName = curator?.displayName {
                    destination.navigationItem.title = displayName
                } else if let username = curator?.username {
                    destination.navigationItem.title = username
                }
                
            }
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func viewOptionsChanged(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            performSegueWithIdentifier("curator_to_grid_segue", sender: self)
            return
        }
        
        selectedViewIndex = sender.selectedSegmentIndex
        reloadArticles()
    }
    
    func articleTableViewReloadRequired(sender: NSNotification) {
        tableView.reloadSections(CuretoGlobal.tableViewFirstSectionIndex, withRowAnimation: .Automatic)
        reloadArticles()
    }
    
    func handleMessagesDot(dot: UIView?) {
        if MessagesTracker.sharedInstance.count > 0 {
            dot?.hidden = false
        } else {
            dot?.hidden = true
        }
    }
    
}

// MARK: - Handling Table View

extension CuratorController {
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return articles.count
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var returnCell: UITableViewCell!
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("CuratorHeader", forIndexPath: indexPath) as! CuratorTableViewHeader
            headerBlurView = cell.headerBlurView
            cell.headerBlurView.alpha = 0.0
            cell.separatorHeight.constant = 0.5
            profileImageView = cell.profileImageView
            usernameLabel = cell.usernameLabel
            nameLabel = cell.nameLabel
            bioLabel = cell.bioLabel
            statsLabel = cell.statsLabel
            placeholderLabel = cell.placeholderLabel
            
            setupUserInfo()
            
            cell.viewOptions.selectedSegmentIndex = selectedViewIndex
            cell.viewOptions.addTarget(self, action: #selector(viewOptionsChanged(_:)), forControlEvents: .ValueChanged)
            viewOptions = cell.viewOptions
            
            cell.editProfileImageButton.addTarget(self, action: #selector(editProfileImageTapped(_:)), forControlEvents: .TouchUpInside)
            cell.editProfileButton.addTarget(self, action: #selector(editProfileTapped(_:)), forControlEvents: .TouchUpInside)
            cell.settingsButton.addTarget(self, action: #selector(settingsTapped(_:)), forControlEvents: .TouchUpInside)
            cell.mailButton.addTarget(self, action: #selector(mailTapped(_:)), forControlEvents: .TouchUpInside)
            
            handleMessagesDot(cell.dotView)
            dotView = cell.dotView
            
            returnCell = cell
        } else {
            if selectedViewIndex == 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("ArticleCell", forIndexPath: indexPath) as! CoverA1
                let article = articles[indexPath.row]
                returnCell = CuretoTableViewHelper.setupCoverCellForArticle(article, row: indexPath.row, cell: cell, tableViewController: self)
                
            } else if selectedViewIndex == 2 {
                let cell = tableView.dequeueReusableCellWithIdentifier("DraftCell", forIndexPath: indexPath) as! CoverA1
                let draft = articles[indexPath.row]
                returnCell = CuretoTableViewHelper.setupCoverCellForArticle(draft, row: indexPath.row, cell: cell, tableViewController: self)
                
            } else {
                let cell = tableView.dequeueReusableCellWithIdentifier("ArticleCompactCell", forIndexPath: indexPath)
                returnCell = cell
            }
        }
        
        returnCell.selectionStyle = .None
        
        return returnCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 1 {
            CuretoTableViewHelper.setupTableViewSelectionForPushAnimator(self, tableView: tableView, indexPath: indexPath, segueIdentifier: "curator_to_article_segue")
        }
    }
    
    
    func refreshTable(refreshControl: UIRefreshControl) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            refreshControl.endRefreshing()
            return
        }
        
        tableView.reloadSections(CuretoGlobal.tableViewFirstSectionIndex, withRowAnimation: .Automatic)
        reloadArticles()
    }
}

// MARK: - Custom methods

extension CuratorController {
    
    func reloadArticles() {
        guard let token = CredentialStore.sharedInstance.accessToken, username = CredentialStore.sharedInstance.username else {
            return
        }
        
        page = 1
        nextPage = 2
        safeToLoadNextPage = true
        
        if selectedViewIndex == 0 {
            CuretoRequests.getUserArticles(username, page: page, onCompletion: { result, message in
                self.refreshControl?.endRefreshing()
                self.articles = result
                self.tableView.reloadSections(CuretoGlobal.tableViewSecondSectionIndex, withRowAnimation: .Automatic)
                if result.count > 0 {
                    CuretoTableViewHelper.setupLoadMoreForTableView(self.tableView)
                }
                
                CuretoRequests.getUserArticles(username, page: self.nextPage, onCompletion: { result2, message in
                    self.nextArticles = result2
                    if result2.count == 0 {
                        self.tableView.tableFooterView = nil
                    }
                })
            })

        } else if selectedViewIndex == 2 {
            CuretoRequests.getDrafts(token, page: page, onCompletion: { result, message in
                self.refreshControl?.endRefreshing()
                self.articles = result
                self.tableView.reloadSections(CuretoGlobal.tableViewSecondSectionIndex, withRowAnimation: .Automatic)
                if result.count > 0 {
                    CuretoTableViewHelper.setupLoadMoreForTableView(self.tableView)
                }
                
                CuretoRequests.getDrafts(token, page: self.nextPage, onCompletion: { result, message in
                    self.nextArticles = result
                    if result.count == 0 {
                        self.tableView.tableFooterView = nil
                    }
                })
            })
            
        }
    }
    
    func loadNextPage() {
        guard let token = CredentialStore.sharedInstance.accessToken, username = CredentialStore.sharedInstance.username else {
            return
        }
        
        if nextArticles.count > 0 {
            let firstRowOfNewPage = articles.count
            articles += nextArticles
            let lastRowOfNewPage = articles.count - 1
            
            var rowsToAdd = [NSIndexPath]()
            for i in firstRowOfNewPage...lastRowOfNewPage {
                let indexPath = NSIndexPath(forRow: i, inSection: 1)
                rowsToAdd.append(indexPath)
            }
            
            tableView.insertRowsAtIndexPaths(rowsToAdd, withRowAnimation: .Left)
            
            page += 1
            nextPage += 1
            nextArticles = []
            
            if selectedViewIndex == 0 {
                CuretoRequests.getUserArticles(username, page: nextPage, onCompletion: { result, message in
                    self.nextArticles = result
                    self.safeToLoadNextPage = true
                })
                
            } else if selectedViewIndex == 1 {
                CuretoRequests.getDrafts(token, page: nextPage, onCompletion: { result, message in
                    self.nextArticles = result
                    self.safeToLoadNextPage = true
                })
                
            }
            
        } else {
            tableView.tableFooterView = nil
        }
    }
    
    func setupUserInfo() {
        guard let username = CredentialStore.sharedInstance.username else {
            return
        }
        
        CuretoRequests.getUserInfo(username, onCompletion: { result, message in
            guard let user = result else {
                return
            }
            
            self.curator = user
            
            if let usrnameLabel = self.usernameLabel, username = user.username {
                usrnameLabel.text = "@\(username)"
            }
            
            if let displayNameLabel = self.nameLabel, displayName = user.displayName {
                displayNameLabel.text = displayName
            }
            
            if let imageView = self.profileImageView, profileImageUrl = user.profileImageUrl {
                if let url = NSURL(string: profileImageUrl) {
                    imageView.hnk_setImageFromURL(url)
                }
            }
            
            if let placeholder = self.placeholderLabel, imageView = self.profileImageView {
                if let profileImageUrl = user.profileImageUrl {
                    placeholder.hidden = true
                    if let url = NSURL(string: profileImageUrl) {
                        imageView.hnk_setImageFromURL(url)
                    }
                } else {
                    placeholder.hidden = false
                    placeholder.text = user.displayName
                    if let assetImage = UIImage(named: "Test_Picture") {
                        imageView.image = assetImage
                    }
                }
            }
            
            if let bLabel = self.bioLabel, bio = user.bio {
                if bio == "" {
                    bLabel.text = "--"
                } else {
                    bLabel.text = bio
                }
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
            }
            
            if let sLabel = self.statsLabel, count = user.articlesCount {
                if count == 0 {
                    sLabel.text = "No article yet"
                } else if count == 1 {
                    sLabel.text = "\(count) article"
                } else {
                    sLabel.text = "\(count) articles"
                }
            }
        })
        
    }
    
    func editProfileImageTapped(sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        
        let removeAction = UIAlertAction(title: "Remove Cover Photo", style: .Destructive, handler: { action in
            self.removeProfilePicture()
        })
        
        let changeAction = UIAlertAction(title: "Upload Cover Photo", style: .Default, handler: { action in
            self.changeProfilePicture()
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(removeAction)
        alertController.addAction(changeAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func editProfileTapped(sender: UIButton) {
        performSegueWithIdentifier("curator_to_edit_profile_segue", sender: self)
    }
    
    func settingsTapped(sender: UIButton) {
        performSegueWithIdentifier("curator_to_settings_segue", sender: self)
    }
    
    func mailTapped(sender: UIButton) {
        performSegueWithIdentifier("curator_to_messages_segue", sender: self)
    }
    
}

// MARK: - Handling image picker and cropper

extension CuratorController: UIImagePickerControllerDelegate, RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource {
    
    func changeProfilePicture() {
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        imagePickerController.sourceType = .PhotoLibrary
        
        presentViewController(imagePickerController, animated: true, completion: nil)
    }
    
    func removeProfilePicture() {
        profileImageView?.image = UIImage(named: "Test_Picture")
        placeholderLabel?.text = curator?.displayName
        placeholderLabel?.hidden = false
        
        if let token = CredentialStore.sharedInstance.accessToken {
            CuretoRequests.removeCoverPhoto(token, onCompletion: { succeeded, message in
                
            })
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let cropController: RSKImageCropViewController = RSKImageCropViewController(image: pickedImage)
            cropController.delegate = self
            cropController.dataSource = self
            cropController.cropMode = .Custom
            picker.pushViewController(cropController, animated: true)
        }
    }
    
    func imageCropViewControllerDidCancelCrop(controller: RSKImageCropViewController) {
        imagePickerController.popViewControllerAnimated(true)
    }
    
    func imageCropViewController(controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        if let imageView = profileImageView {
            if let image = resizedImage(croppedImage) {
                imageView.image = image
                placeholderLabel?.text = ""
                placeholderLabel?.hidden = true
            }
            imagePickerController.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func imageCropViewControllerCustomMaskRect(controller: RSKImageCropViewController) -> CGRect {
        let maskSize = CGSizeMake(MainScreen.width, MainScreen.width / 16 * 9)
        let viewWidth = CGRectGetWidth(controller.view.frame)
        let viewHeight = CGRectGetHeight(controller.view.frame)
        let maskRect = CGRectMake((viewWidth - maskSize.width) * 0.5, (viewHeight - maskSize.height) * 0.5, maskSize.width, maskSize.height)
        
        return maskRect
    }
    
    func imageCropViewControllerCustomMaskPath(controller: RSKImageCropViewController) -> UIBezierPath {
        let rect = controller.maskRect
        let point1 = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect))
        let point2 = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect))
        let point3 = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect))
        let point4 = CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect))
        
        let rectangle = UIBezierPath()
        rectangle.moveToPoint(point1)
        rectangle.addLineToPoint(point2)
        rectangle.addLineToPoint(point4)
        rectangle.addLineToPoint(point3)
        rectangle.closePath()
        
        return rectangle
    }
    
    func imageCropViewControllerCustomMovementRect(controller: RSKImageCropViewController) -> CGRect {
        return controller.maskRect
    }
    
    func resizedImage(image: UIImage) -> UIImage? {
        var actualWidth = image.size.width
        var actualHeight = image.size.height
//        print("actual \(actualWidth)x\(actualHeight)")
        
        let maxWidth: CGFloat = 1080.0
        let maxHeight: CGFloat = maxWidth / 16 * 9
        
        let compressionQuality: CGFloat = 0.95
        
        if actualWidth > maxWidth || actualHeight > maxHeight {
            actualWidth = maxWidth
            actualHeight = maxHeight
        }
        
//        print("actual \(actualWidth)x\(actualHeight)")
        
        let rect = CGRectMake(0, 0, actualWidth, actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.drawInRect(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        
        if let imageData = UIImageJPEGRepresentation(img, compressionQuality) {
            if let token = CredentialStore.sharedInstance.accessToken {
                CuretoRequests.changeCoverPhoto(token, image: imageData, onCompletion: { succeeded, message in
                    
                })
            }
            return UIImage(data: imageData)
        }
        return nil
    }
    
}

// MARK: - Handling Scroll View

extension CuratorController {
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        let maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let headerImageHeight = (MainScreen.width / 16 * 9)
        
        let rOffset = round(offset)
        let rMaxOffset = round(maxOffset)
        
        if rOffset == rMaxOffset && safeToLoadNextPage {
            safeToLoadNextPage = false
            NSTimer.scheduledTimerWithTimeInterval(CuretoGlobal.paginationDelay, target: self, selector: #selector(CuratorController.loadNextPage), userInfo: nil, repeats: false)
        }
        
        if let blur = headerBlurView {
                        
            UIView.beginAnimations("", context: nil)
            
            if offset <= headerImageHeight * 0.01 {
                blur.alpha = 0
            } else if offset > headerImageHeight * 0.01 && offset <= headerImageHeight {
                blur.alpha = offset / headerImageHeight
            } else {
                blur.alpha = 1.0
            }
            
            UIView.commitAnimations()
        }
    }
}