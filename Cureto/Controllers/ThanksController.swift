//
//  ThanksController.swift
//  Cureto
//
//  Created by Sean Choo on 4/8/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import SafariServices

class ThanksController: UITableViewController {
    
    var selectedLink: String?
    
    var philosophyThanks = [Thank]()
    var designThanks = [Thank]()
    var programmingThanks = [Thank]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        
        tableView.registerNib(UINib(nibName: "ThankTableViewCell", bundle: nil), forCellReuseIdentifier: "ThankCell")
        
        setupThanks()
    }

    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBarHidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        navigationController?.navigationBarHidden = true
    }
    
    func setupThanks() {
        let basecamp = Thank(people: "Basecamp (formerly 37signals)", reason: "For laying aspirational philosophies and principles that form the core value of this app", link: "https://m.signalvnoise.com")
        philosophyThanks.append(basecamp)
        
        let icons8 = Thank(people: "Icons8", reason: "For creating and sharing most of the beautiful icons used in this app", link: "https://icons8.com")
        designThanks.append(icons8)
        
        let foursquare = Thank(people: "Foursquare", reason: "For making restaurants information available to everyone", link: "https://foursquare.com")
        let alamofire = Thank(people: "Alamofire", reason: "For making network requests in iOS elegant", link: "https://github.com/Alamofire/Alamofire")
        let swiftyjson = Thank(people: "SwiftyJSON", reason: "For making json parsing in iOS a smooth and swifty experience", link: "https://github.com/SwiftyJSON/SwiftyJSON")
        let keychainswift = Thank(people: "KeychainSwift", reason: "For making secure data storage in iOS Keychain super easy", link: "https://github.com/marketplacer/keychain-swift")
        let hanekeswift = Thank(people: "HanekeSwift", reason: "For making image loading and caching in iOS effortless", link: "https://github.com/Haneke/HanekeSwift")
        let rskimagecropper = Thank(people: "RSKImageCropper", reason: "For making image cropping in iOS super convenient", link: "https://github.com/ruslanskorb/RSKImageCropper")
        let validator = Thank(people: "Validator", reason: "For making form fields validation easy and fun", link: "https://github.com/adamwaite/Validator")
        let kilabel = Thank(people: "KILabel", reason: "For making UILabel hyperlink simple and intuitive", link: "https://github.com/Krelborn/KILabel")
        programmingThanks = [foursquare, alamofire, swiftyjson, keychainswift, hanekeswift, rskimagecropper, validator, kilabel]
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return philosophyThanks.count
        } else if section == 1 {
            return designThanks.count
        } else {
            return programmingThanks.count
        }
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = NSBundle.mainBundle().loadNibNamed("FilterSectionHeaderView", owner: self, options: nil)[0] as! FilterSectionHeaderView
        headerView.frame = CGRectMake(0, 0, MainScreen.width, 50)
        
        if section == 0 {
            headerView.headerLabel.text = "Philosophy"
        } else if section == 1 {
            headerView.headerLabel.text = "Design"
        } else {
            headerView.headerLabel.text = "Programming"
        }
        
        return headerView
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ThankCell", forIndexPath: indexPath) as! ThankTableViewCell
        
        var thank: Thank!
        
        if indexPath.section == 0 {
            thank = philosophyThanks[indexPath.row]
        } else if indexPath.section == 1 {
            thank = designThanks[indexPath.row]
        } else {
            thank = programmingThanks[indexPath.row]
        }
        
        cell.peopleLabel.text = thank.people
        cell.reasonLabel.text = thank.reason
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        var thank: Thank!
        
        if indexPath.section == 0 {
            thank = philosophyThanks[indexPath.row]
        } else if indexPath.section == 1 {
            thank = designThanks[indexPath.row]
        } else {
            thank = programmingThanks[indexPath.row]
        }
        
        if let link = thank.link {
            if let url = NSURL(string: link) {
                if #available(iOS 9, *) {
                    let svc = SFSafariViewController(URL: url)
                    presentViewController(svc, animated: true, completion: nil)
                } else {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
        }
    }
    
}
