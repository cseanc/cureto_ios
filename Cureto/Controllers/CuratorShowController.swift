//
//  CuratorShowController.swift
//  Cureto
//
//  Created by Sean Choo on 3/23/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import Haneke

class CuratorShowController: CuretoTableViewController {
    
    // FROM ArticleController OR CuratorSearchController
    var selectedCurator: User?
    
    // FROM ChillController
    var selectedMagazineId: Int?
    
    var viewOptions: UISegmentedControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerNib(UINib(nibName: "CuratorShowTableViewHeader", bundle: nil), forCellReuseIdentifier: "CuratorHeader")
        tableView.registerNib(UINib(nibName: "Cover-A1", bundle: nil), forCellReuseIdentifier: "ArticleCell")
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500.0
        
        reloadArticles()
        
        // FROM ChillController
        if let id = selectedMagazineId {
            CuretoRequests.getFeaturedElements(id, onCompletion: { result in
                if result.count > 0 {
                    if let targetId = result[0].targetId {
                        CuretoRequests.getUserWithId(targetId, onCompletion: { user, message in
                            self.selectedCurator = user
                            self.tableView.reloadSections(CuretoGlobal.tableViewFirstSectionIndex, withRowAnimation: .Automatic)
                            self.reloadArticles()
                        })
                    }
                }
            })
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        // For navigation transition
        navigationController?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "curator_show_to_article_segue" {
            let destination = segue.destinationViewController as! ArticleController
            destination.selectedArticle = selectedArticle
            destination.coverImage = toImageView?.image
            destination.presenting = true
        } else if segue.identifier == "curator_show_to_grid_segue" {
            let navController = segue.destinationViewController as! UINavigationController
            if let destination = navController.viewControllers[0] as? CuratorGridController {
                destination.curatorShowController = self
                destination.previousViewIndex = 0
                
                destination.showMode = true
                
                if let displayName = selectedCurator?.displayName {
                    destination.navigationItem.title = displayName
                } else if let username = selectedCurator?.username {
                    destination.navigationItem.title = username
                }
                
            }
        }
        
    }
    
    func viewOptionsChanged(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            performSegueWithIdentifier("curator_show_to_grid_segue", sender: self)
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return articles.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var returnCell: UITableViewCell!
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("CuratorHeader", forIndexPath: indexPath) as! CuratorShowTableViewHeader
            cell.separatorHeight.constant = 0.5
            
            cell.viewOptions.addTarget(self, action: #selector(viewOptionsChanged(_:)), forControlEvents: .ValueChanged)
            viewOptions = cell.viewOptions
            
            if let username = selectedCurator?.username {
                cell.usernameLabel.text = "@\(username)"
            }
            cell.displayNameLabel.text = selectedCurator?.displayName
            
            if let count = selectedCurator?.articlesCount {
                if count == 0 {
                    cell.statsLabel.text = "No article yet"
                } else if count == 1 {
                    cell.statsLabel.text = "\(count) article"
                } else {
                    cell.statsLabel.text = "\(count) articles"
                }
            }
            
            if let bio = selectedCurator?.bio {
                if bio != "" {
                    cell.bioLabel.text = bio
                } else {
                    cell.bioLabel.text = "--"
                }
            } else {
                cell.bioLabel.text = "--"
            }
            
            if let imageUrl = selectedCurator?.profileImageUrl {
                cell.placeholderLabel.hidden = true
                if let url = NSURL(string: imageUrl) {
                    cell.coverImageView.hnk_setImageFromURL(url)
                }
            } else {
                cell.placeholderLabel.hidden = false
                cell.placeholderLabel.text = selectedCurator?.displayName
                cell.coverImageView.image = UIImage(named: CuretoGlobal.placeholderImageName)
            }
            
            returnCell = cell
            
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("ArticleCell", forIndexPath: indexPath) as! CoverA1
            let article = articles[indexPath.row]
            returnCell = CuretoTableViewHelper.setupCoverCellForArticle(article, row: indexPath.row, cell: cell, tableViewController: self)
        }
        
        returnCell.selectionStyle = .None
        
        return returnCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        CuretoTableViewHelper.setupTableViewSelectionForPushAnimator(self, tableView: tableView, indexPath: indexPath, segueIdentifier: "curator_show_to_article_segue")
    }
    
    func reloadArticles() {
        guard let username = selectedCurator?.username else {
            return
        }
        
        page = 1
        nextPage = 2
        safeToLoadNextPage = true
        
        CuretoRequests.getUserArticles(username, page: page, onCompletion: { result, message in
            self.refreshControl?.endRefreshing()
            self.articles = result
            self.tableView.reloadSections(CuretoGlobal.tableViewSecondSectionIndex, withRowAnimation: .Automatic)
            if result.count > 0 {
                CuretoTableViewHelper.setupLoadMoreForTableView(self.tableView)
            }
            CuretoRequests.getUserArticles(username, page: self.nextPage, onCompletion: {
                result2, message in
                self.nextArticles = result2
                if result2.count == 0 {
                    self.tableView.tableFooterView = nil
                }
            })
        })
        
    }
    
    func loadNextPage() {
        guard let username = selectedCurator?.username else {
            return
        }
        
        if nextArticles.count > 0 {
            let firstRowOfNewPage = articles.count
            articles += nextArticles
            let lastRowOfNewPage = articles.count - 1
            
            var rowsToAdd = [NSIndexPath]()
            for i in firstRowOfNewPage...lastRowOfNewPage {
                let indexPath = NSIndexPath(forRow: i, inSection: 1)
                rowsToAdd.append(indexPath)
            }
            
            tableView.insertRowsAtIndexPaths(rowsToAdd, withRowAnimation: .Left)
            
            page += 1
            nextPage += 1
            nextArticles = []
            
            CuretoRequests.getUserArticles(username, page: nextPage, onCompletion: { result, message in
                self.nextArticles = result
                self.safeToLoadNextPage = true
            })
            
        } else {
            tableView.tableFooterView = nil
        }
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        var maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if let height = tabBarController?.tabBar.frame.height {
            maxOffset += height
        }
        
        let rOffset = round(offset)
        let rMaxOffset = round(maxOffset)
        
//        print("offset: \(rOffset) maxOffset: \(rMaxOffset)")

        
        if rOffset == rMaxOffset && safeToLoadNextPage {
            safeToLoadNextPage = false
            NSTimer.scheduledTimerWithTimeInterval(CuretoGlobal.paginationDelay, target: self, selector: #selector(CuratorShowController.loadNextPage), userInfo: nil, repeats: false)
        }
        
    }
}
