//
//  ChillFilterController.swift
//  Cureto
//
//  Created by Sean Choo on 5/2/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class ChillFilterController: UITableViewController {
    
    var chillController: ChillController?
    
    var selectedArea: String = CuretoGlobal.everywhere
    var areaGroups = [AreaGroup]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.sectionIndexBackgroundColor = UIColor.clearColor()
        
        let everywhere = AreaGroup(title: "")
        everywhere.areas = [CuretoGlobal.everywhere]
        areaGroups.append(everywhere)
        
        CuretoRequests.getRestaurantAreas({ areas in
            self.areaGroups += self.groupsFromAreas(areas)
            self.tableView.reloadData()
        })
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return areaGroups.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let group = areaGroups[section]
        return group.areas.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FilterCell", forIndexPath: indexPath) as! FilterTableViewCell
        cell.separatorHeight.constant = 0.5
        
        let group = areaGroups[indexPath.section]
        
        cell.filterLabel.text = group.areas[indexPath.row]
        
        if indexPath.row == group.areas.count - 1 {
            cell.separator.hidden = true
        } else {
            cell.separator.hidden = false
        }
        
        if group.areas[indexPath.row] == selectedArea {
            cell.dotImageView.hidden = false
        } else {
            cell.dotImageView.hidden = true
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let group = areaGroups[indexPath.section]
        selectedArea = group.areas[indexPath.row]
        tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = NSBundle.mainBundle().loadNibNamed("FilterSectionHeaderView", owner: self, options: nil)[0] as! FilterSectionHeaderView
        headerView.frame = CGRectMake(0, 0, MainScreen.width, 35)
        
        headerView.bottomSpace.constant = 5
        
        headerView.headerLabel.text = areaGroups[section].title
        
        return headerView
    }
    
    override func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        var titles = [String]()
        
        for group in areaGroups {
            titles.append(group.title)
        }
        
        return titles
    }
    
    func groupsFromAreas(areas: [String]) -> [AreaGroup] {
        var groups = [AreaGroup]()
        var firstCharacters = [String]()
        
        for area in areas {
            if let firstCharacter = area.characters.first {
                firstCharacters.append(String(firstCharacter))
            }
        }
        
        if firstCharacters.count != areas.count {
            return groups
            
        } else {
            var i = 0
            while i < firstCharacters.count {
                var j = i + 1
                let group = AreaGroup(title: firstCharacters[i])
                group.areas.append(areas[i])
                while j < firstCharacters.count {
                    if firstCharacters[j] != firstCharacters[i] {
                        break
                    } else {
                        group.areas.append(areas[j])
                    }
                    j += 1
                    i += 1
                }
                groups.append(group)
                i += 1
            }
        }
        
        return groups
    }

    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func done(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: {
            if let chill = self.chillController {
                chill.selectedArea = self.selectedArea
                chill.setTitleView()
                if self.selectedArea == CuretoGlobal.everywhere {
                    chill.loadMagazines()
                } else {
                    chill.loadArticles()
                }
            }
        })
    }
}
