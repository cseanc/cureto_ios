//
//  PinsController.swift
//  Cureto
//
//  Created by Sean Choo on 3/20/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import Haneke

class PinsController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    // For navigation transition
    var navigationBarHeight: CGFloat = 64
    var initialFrame = CGRectZero
    var toImageView: UIImageView?
    
    // For chronological order
    var articles = [Article]()
    var nextArticles = [Article]()
    var page = 1
    var nextPage = 2
    var safeToLoadNextPage = true
    
    var pinGroups = [PinGroup]()
    
    // For article controller
    var selectedArticle: Article?
    
    // From curator controller
    var curatorController: CuratorController?
    var previousViewIndex = 0
    
    var refreshControl: UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // For navigation transition
        var statusBarHeight = UIApplication.sharedApplication().statusBarFrame.size.height
        if statusBarHeight == 0 {
            statusBarHeight = 20
        }
        if let navBarHeight = navigationController?.navigationBar.frame.size.height {
            navigationBarHeight = statusBarHeight + navBarHeight
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(pinsReloadNeeded(_:)), name: "PinsReloadNeeded", object: nil)
        
        collectionView?.alwaysBounceVertical = true
        collectionView?.registerNib(UINib(nibName: "Cover-G1", bundle: nil), forCellWithReuseIdentifier: "PinCell")
        
        loadPins()
    }
    
    override func viewWillAppear(animated: Bool) {
        // For navigation transition
        navigationController?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        if refreshControl == nil {
            refreshControl = UIRefreshControl()
            refreshControl?.addTarget(self, action: #selector(refreshCollectionView(_:)), forControlEvents: .ValueChanged)
            collectionView?.addSubview(refreshControl!)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "pin_to_article_segue" {
            let destination = segue.destinationViewController as! ArticleController
            destination.selectedArticle = selectedArticle
            destination.coverImage = toImageView?.image
            destination.presenting = true
            
        }
    }
    
    @IBAction func back(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: {
            if let controller = self.curatorController {
                controller.viewOptions?.selectedSegmentIndex = self.previousViewIndex
            }
        })
    }
    
    func loadPins() {
        if let token = CredentialStore.sharedInstance.accessToken {
            CuretoRequests.getUserAreaPins(token, onCompletion: { result, message in
                self.pinGroups = result
                self.refreshControl?.endRefreshing()
                self.collectionView?.reloadData()
            })
        }
    }
    
    func refreshCollectionView(refreshControl: UIRefreshControl) {
        loadPins()
//        reloadArticles()
    }
    
    func pinsReloadNeeded(sender: NSNotification) {
        loadPins()
//        reloadArticles()
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return pinGroups.count
//        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pinGroups[section].articles.count
//        return articles.count
    }
    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "PinHeaderView", forIndexPath: indexPath) as! PinCollectionViewHeader
        headerView.titleLabel.text = pinGroups[indexPath.section].area
        return headerView
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PinCell", forIndexPath: indexPath) as! CoverG1
        cell.contentView.layer.borderWidth = 0.5
        cell.contentView.layer.borderColor = MainColor.separatorTint.CGColor
        cell.separatorHeight.constant = 0.5
        
        if MainScreen.width == 320 {
            cell.authorLabelTopSpace.constant = 2
            cell.titleLabelTopSpace.constant = 0
            cell.titleLabelBottomSpace.constant = 0
        }
        
        let article = pinGroups[indexPath.section].articles[indexPath.item]
        
        cell.authorLabel.text = article.author?.displayName
        cell.titleLabel.text = article.title
        cell.locationLabel.text = article.restaurant?.name
        
        cell.imageView.image = UIImage(named: CuretoGlobal.placeholderImageName)
        
        if let thumbUrl = article.coverPhotoThumbUrl {
            if let url = NSURL(string: thumbUrl) {
                cell.imageView.hnk_setImageFromURL(url)
            }
        }
        
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        selectedArticle = pinGroups[indexPath.section].articles[indexPath.item]

        // For navigation transitioning
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CoverG1
        initialFrame = cell.convertRect(cell.imageView.frame, toView: nil)
        
        if let width = selectedArticle?.coverPhotoWidth, height = selectedArticle?.coverPhotoHeight {
            let heightRatio = height / width
            toImageView = UIImageView(frame: CGRectMake(0, navigationBarHeight, MainScreen.width, MainScreen.width * heightRatio))
            toImageView?.image = cell.imageView.image
        }
        
        performSegueWithIdentifier("pin_to_article_segue", sender: self)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var width: CGFloat = 154
        if MainScreen.width % 2 == 0 {
            width = (MainScreen.width - 12) / 2
        } else {
            width = (MainScreen.width - 11) / 2
        }
        let height = width / 182 * 260
        return CGSizeMake(width, height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSizeMake(MainScreen.width, 44)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        if MainScreen.width % 2 == 0 {
            return 4
        } else {
            return 3
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 6, left: 4, bottom: 16, right: 4)
    }
    
    // For chronological order
//    func reloadArticles() {
//        guard let token = CredentialStore.sharedInstance.accessToken else {
//            return
//        }
//        
//        page = 1
//        nextPage = 2
//        safeToLoadNextPage = true
//        
//        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
//        CuretoRequests.getUserChronologicalPins(token, page: page, onCompletion: { result, message in
//            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//            self.refreshControl?.endRefreshing()
//            self.articles = result
//            self.collectionView?.reloadData()
//            
//            CuretoRequests.getUserChronologicalPins(token, page: self.nextPage, onCompletion: { result2, message in
//                self.nextArticles = result2
//            })
//        })
//        
//    }
    
//    func loadNextPage() {
//        if nextArticles.count > 0 {
//            let firstRowOfNewPage = articles.count
//            articles += nextArticles
//            let lastRowOfNewPage = articles.count - 1
//            
//            var itemsToAdd = [NSIndexPath]()
//            for i in firstRowOfNewPage...lastRowOfNewPage {
//                let indexPath = NSIndexPath(forItem: i, inSection: 0)
//                itemsToAdd.append(indexPath)
//            }
//            
//            collectionView?.insertItemsAtIndexPaths(itemsToAdd)
//            
//            page += 1
//            nextPage += 1
//            nextArticles = []
//            
//            guard let token = CredentialStore.sharedInstance.accessToken else {
//                return
//            }
//            
//            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
//            CuretoRequests.getUserChronologicalPins(token, page: nextPage, onCompletion: { result, message in
//                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
//                self.nextArticles = result
//                self.safeToLoadNextPage = true
//            })
//            
//        } else {
//            collectionView?.reloadData()
//        }
//    }

//    override func scrollViewDidScroll(scrollView: UIScrollView) {
//        let offset = scrollView.contentOffset.y
//        var maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
//        
//        if let height = tabBarController?.tabBar.frame.height {
//            maxOffset += height
//        }
//        
//        let rOffset = round(offset)
//        let rMaxOffset = round(maxOffset)
//        
//        if rOffset == rMaxOffset && safeToLoadNextPage {
//            safeToLoadNextPage = false
//            NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(PinsController.loadNextPage), userInfo: nil, repeats: false)
//
//        }
//        
//    }
    
}

extension PinsController: UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    // For navigation transition
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        guard let _ = toVC as? ArticleController else {
            return nil
        }
        
        if operation == UINavigationControllerOperation.Push {
            let pushAnimator = CuretoPushAnimator()
            pushAnimator.initialFrame = initialFrame
            pushAnimator.toImageView = toImageView
            pushAnimator.fromCollectionView = true
            pushAnimator.tabBarPresent = false
            return pushAnimator
        }
        
        return nil
    }
    
}