//
//  SignupController.swift
//  Cureto
//
//  Created by Sean Choo on 4/14/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import SafariServices
import Validator

class SignupController: UIViewController, UITextFieldDelegate {
    
    let copyrightString = "This application uses the Foursquare\u{00AE} application programming interface but is not endorsed or certified by Foursquare Labs, Inc. All of the Foursquare\u{00AE} logos (including all badges) and trademarks displayed on this application are the property of Foursquare Labs, Inc"
    
    var textTimer: NSTimer?
    
    var validUsername: Bool = false
    var validEmail: Bool = false
    var validPassword: Bool = false
    
    @IBOutlet weak var signupButton: UIButton!
    
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var copyrightLabel: UILabel!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var usernameStatusLabel: UILabel!
    @IBOutlet weak var emailStatusLabel: UILabel!
    @IBOutlet weak var passwordStatusLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        formView.layer.cornerRadius = 3
        innerView.layer.cornerRadius = 3
        
        separatorHeight.constant = 0.5
        
        setupInfoLabel()
        
        disableSignupButton()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(SignupController.infoTapped))
        infoLabel.addGestureRecognizer(tap)
        
        copyrightLabel.text = copyrightString
    }
    
    override func viewDidAppear(animated: Bool) {
        usernameTextField.becomeFirstResponder()
    }
    
    func setupInfoLabel() {
        let info: String = "By signing up, you agree to the Privacy Policy and the Terms of Service. Learn more"
        let attrs = [NSFontAttributeName: UIFont.systemFontOfSize(12), NSForegroundColorAttributeName: UIColor.darkGrayColor()]
        let underlineAttrs = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        let attributedInfo = NSMutableAttributedString(string: info, attributes: attrs)
        attributedInfo.addAttributes(underlineAttrs, range: NSRange(location: 73, length: 10))
        infoLabel.attributedText = attributedInfo
    }
    
    func enableSignupButton() {
        signupButton.enabled = true
        signupButton.setTitleColor(MainColor.globalTint, forState: .Normal)
    }
    
    func disableSignupButton() {
        signupButton.enabled = false
        signupButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
    }
    
    func updateFormStatus() {
        if validUsername && validEmail && validPassword {
            enableSignupButton()
        } else {
            disableSignupButton()
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        textTimer?.invalidate()
        textTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(SignupController.typingPaused), userInfo: textField, repeats: false)
        
        disableSignupButton()
        
        switch textField {
        case usernameTextField:
            let inverseSet = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyz0123456789._").invertedSet
            let components = string.componentsSeparatedByCharactersInSet(inverseSet)
            let filtered = components.joinWithSeparator("")
            return string == filtered
        case passwordTextField:
            let inverseSet = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._+-!@").invertedSet
            let components = string.componentsSeparatedByCharactersInSet(inverseSet)
            let filtered = components.joinWithSeparator("")
            return string == filtered
        default:
            return true
        }
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case usernameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        default:
            view.endEditing(true)
        }
        
        return true
    }
    
    func validateUsername(usernameText: String?) {
        if let username = usernameText {
            if username != "" {
                usernameStatusLabel.text = CuretoGlobal.searching
                CuretoRequests.validateUsername(username, onCompletion: { valid, message in
                    self.validUsername = valid
                    
                    if valid {
                        self.usernameStatusLabel.text = CuretoGlobal.smile
                    } else {
                        self.usernameStatusLabel.text = CuretoGlobal.frown
                    }
                    
                    self.updateFormStatus()
                })
            } else {
                usernameStatusLabel.text = CuretoGlobal.frown
            }
        }
    }
    
    func validateEmail(emailText: String?) {
        let rule = ValidationRulePattern(pattern: .EmailAddress, failureError: ValidationError(message: "Invalid Email"))
        
        if let email = emailText {
            let result = email.validate(rule: rule)
            
            switch result {
            case .Valid:
                emailStatusLabel.text = CuretoGlobal.searching
                CuretoRequests.validateEmail(email, onCompletion: { valid, message in
                    self.validEmail = valid
                    
                    if valid {
                        self.emailStatusLabel.text = CuretoGlobal.smile
                    } else {
                        self.emailStatusLabel.text = CuretoGlobal.frown
                    }
                    
                    self.updateFormStatus()
                })
                
            case .Invalid(_):
                validEmail = false
                emailStatusLabel.text = CuretoGlobal.frown
                updateFormStatus()
            }
            
        }
    }
    
    func validatePassword(passwordText: String?) {
        if let password = passwordText {
            if password != "" {
                validPassword = true
                passwordStatusLabel.text = CuretoGlobal.smile
            } else {
                validPassword = false
                passwordStatusLabel.text = CuretoGlobal.frown
            }
            
            updateFormStatus()
        }
    }
    
    func typingPaused(timer: NSTimer) {
        if let textField = timer.userInfo as? UITextField {
            switch textField {
            case usernameTextField:
                validateUsername(usernameTextField.text)
            case emailTextField:
                validateEmail(emailTextField.text)
            default:
                validatePassword(passwordTextField.text)
            }
        }
    }
    
    func infoTapped(recognizer: UITapGestureRecognizer) {
        if let url = NSURL(string: CuretoUrls.policiesAndTermsUrl) {
            if #available(iOS 9, *) {
                let svc = SFSafariViewController(URL: url)
                presentViewController(svc, animated: true, completion: nil)
            } else {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    
    @IBAction func signup(sender: AnyObject) {
        guard let username = usernameTextField.text, email = emailTextField.text, password = passwordTextField.text else {
            return
        }
        
        if validUsername && validEmail && validPassword {
            CuretoProgressHUD.sharedInstance.startMessageSpinning(view, message: "This takes a while...")
            CuretoRequests.signup(username, email: email, password: password, onCompletion: { succeeded, message in
                if succeeded {
                    CuretoRequests.login(username, password: password, onCompletion: { loginSucceeded, loginMessage in
                        CuretoProgressHUD.sharedInstance.stopMessageSpinning()
                        if loginSucceeded {
                            self.dismissViewControllerAnimated(true, completion: nil)
                            NSNotificationCenter.defaultCenter().postNotificationName("LoginStatusChanged", object: nil)
                        } else {
                            CuretoBannerAlert.showMessage("Thank you for signing up for Cureto. You can log in now")
                        }
                    })
                } else {
                    CuretoProgressHUD.sharedInstance.stopMessageSpinning()
                    CuretoBannerAlert.showMessage(message)
                }
            })
        }
    }
    
    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}
