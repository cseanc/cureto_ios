//
//  AddLocationController.swift
//  Cureto
//
//  Created by Sean on 11/28/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import CoreLocation

class AddLocationController: UIViewController {
    
    var validLocation = false
    
    var initialLoaded = false
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    var publishController: PublishController?
    
    var foursquareAuth: FoursquareAuth?
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    var restaurants = [Restaurant]()
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var locationSearchBar: UISearchBar!
    @IBOutlet weak var locationTableView: UITableView!
    
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        separatorHeight.constant = 0.5
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        locationTableView.rowHeight = UITableViewAutomaticDimension
        locationTableView.estimatedRowHeight = 58.0
        
        locationTableView.registerNib(UINib(nibName: "SearchTitleAndSubtitle", bundle: nil), forCellReuseIdentifier: "LocationCell")
        
        addFooterView()
    }
    
//    override func viewWillAppear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: false)
//    }
//    
//    override func viewWillDisappear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: true)
//    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "add_location_to_new_segue" {
            let destination = segue.destinationViewController as! NewLocationController
            destination.addLocationController = self
        }
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func addFooterView() {
        let footerView = NSBundle.mainBundle().loadNibNamed("LocationSearchFooterView", owner: self, options: nil)[0] as! LocationSearchFooterView
        footerView.frame = CGRectMake(0, 0, MainScreen.width, 60)
        footerView.separatorHeight.constant = 0.5
        locationTableView.tableFooterView = footerView
    }
    
}

// MARK: - Handling location

extension AddLocationController: CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if let permissionSet = userDefaults.valueForKey(CuretoGlobal.locationPermissonSet) as? String {
            if permissionSet == CuretoGlobal.trueString {
//                print("Location permission set")
                switch status {
                case .AuthorizedWhenInUse:
                    manager.startUpdatingLocation()
                default:
                    presentLocationPermissionAlert()
                }
            } else {
//                print("Location permission not set")
                userDefaults.setValue(CuretoGlobal.trueString, forKey: CuretoGlobal.locationPermissonSet)
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = publishController?.coverPhotoLocation {
            let lat = location.coordinate.latitude
            let lng = location.coordinate.longitude
            
            if CuretoLocationHelper.withinBoundary(CuretoArea.HK.boundary(), latitude: lat, longitude: lng) {
                latitude = lat
                longitude = lng
            } else {
                useCurrentLocation(locations)
            }

        } else {
            useCurrentLocation(locations)
        }
        
        if !initialLoaded {
            initialLoaded = true
            if let token = CredentialStore.sharedInstance.accessToken {
                CuretoRequests.authWithFoursquare(token, onCompletion: { (result: FoursquareAuth?, message: String) in
                    if let auth = result {
                        self.foursquareAuth = auth
                        self.getPlaces(auth, query: "")
                    }
                })
            }
        }
    }
    
    func useCurrentLocation(locations: [CLLocation]) {
        if let location = locations.last {
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
        }
    }
    
    func presentLocationPermissionAlert() {
        let alertController = UIAlertController(title: "Location Disabled", message: "Please enable location to add restaurant 😉", preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: "Later", style: .Cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Settings", style: .Default, handler: { action in
            if let url = NSURL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.sharedApplication().openURL(url)
            }
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
}


// MARK: - Handling table view

extension AddLocationController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if validLocation {
            return restaurants.count + 1
        } else {
            return restaurants.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if validLocation {
            if indexPath.row < restaurants.count {
                let cell = tableView.dequeueReusableCellWithIdentifier("LocationCell", forIndexPath: indexPath) as! SearchTitleAndSubtitle
                
                let restaurant = restaurants[indexPath.row]
                
                if let name = restaurant.name {
                    cell.nameLabel.text = name
                } else {
                    cell.nameLabel.text = "--"
                }
                
                if var addressString = restaurant.address {
                    if let city = restaurant.city {
                        addressString += ", \(city)"
                    }
                    cell.addressLabel.text = addressString
                } else {
                    cell.addressLabel.text = "--"
                }
                
                cell.touchFace.tag = indexPath.row
                cell.touchFace.addTarget(self, action: #selector(restaurantTapped(_:)), forControlEvents: .TouchUpInside)
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCellWithIdentifier("AddCell", forIndexPath: indexPath) as! NewLocationTableViewCell
                cell.addButton.addTarget(self, action: #selector(AddLocationController.newLocationTapped), forControlEvents: .TouchUpInside)
                return cell
            }
            
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("LocationCell", forIndexPath: indexPath) as! SearchTitleAndSubtitle
            return cell
        }
        
    }
    
    func restaurantTapped(sender: UIButton) {
        let restaurant = restaurants[sender.tag]
        if let publish = publishController {
            publish.location = restaurant
            publish.templateController?.selectedRestaurant = restaurant
            publish.publishTableView.reloadData()
            navigationController?.popViewControllerAnimated(true)
        }
    }
    
    func addNewLocation(restaurant: Restaurant) {
        if let publish = publishController {
            publish.location = restaurant
            publish.templateController?.selectedRestaurant = restaurant
            navigationController?.popViewControllerAnimated(true)
        }
    }
    
    func newLocationTapped(sender: UIButton) {
        performSegueWithIdentifier("add_location_to_new_segue", sender: self)
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        locationSearchBar.resignFirstResponder()
    }
}

// MARK: - Handling search

extension AddLocationController: UISearchBarDelegate {
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if let auth = foursquareAuth {
            let encodedSearchText = searchText.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
            getPlaces(auth, query: encodedSearchText!)
        }
    }
    
}

// MARK: - Custom methods

extension AddLocationController {
    
    func getPlaces(auth: FoursquareAuth, query: String) {
        if CuretoLocationHelper.withinBoundary(CuretoArea.HK.boundary(), latitude: latitude, longitude: longitude) {
            validLocation = true
            if query == "" {
                CuretoProgressHUD.sharedInstance.startSpinning(view)
            }
            CuretoRequests.getNearbyPlaces(auth, latitude: latitude, longitude: longitude, query: query, onCompletion: {
                (places: [Restaurant], message: String) in
                CuretoProgressHUD.sharedInstance.stopSpinning()
                self.restaurants = places
                self.locationTableView.reloadData()
            })
        } else {
            validLocation = false
            CuretoAlerts.notifyUserWith(CuretoAlerts.MessageType.AvailableInHongKongOnly, viewController: self)
        }
    }
}
