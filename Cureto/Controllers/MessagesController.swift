//
//  MessagesController.swift
//  Cureto
//
//  Created by Sean Choo on 3/29/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class MessagesController: UITableViewController {
    
    let dateFormatter = NSDateFormatter()
    
    var messages = [Message]()
    var nextMessages = [Message]()
    var page = 1
    var nextPage = 2
    var safeToLoadNextPage = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        
        refreshControl?.addTarget(self, action: #selector(refreshTable(_:)), forControlEvents: .ValueChanged)
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBarHidden = false
    }
    
    override func viewDidAppear(animated: Bool) {
        if let items = tabBarController?.tabBar.items {
            items[4].badgeValue = nil
        }
        MessagesTracker.sharedInstance.count = 0
        
        reloadMessages()
    }
    
    override func viewWillDisappear(animated: Bool) {
        navigationController?.navigationBarHidden = true
        if let token = CredentialStore.sharedInstance.accessToken {
            CuretoRequests.markAllUserMessagesRead(token, onCompletion: { succeeded, message in
                
            })
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MessageCell", forIndexPath: indexPath) as! MessageTableViewCell
        cell.separatorHeight.constant = 0.5
        
        let message = messages[indexPath.row]
        cell.messageLabel.text = message.content
        
        if let date = message.createdAt {
            dateFormatter.dateFormat = "MMM d, hh:mm aa"
            cell.dateLabel.text = dateFormatter.stringFromDate(date)
        }
        
        if message.read {
            cell.contentView.backgroundColor = UIColor.whiteColor()
        } else {
            cell.contentView.backgroundColor = MainColor.unreadTint
        }
        
        return cell
    }
    
    func refreshTable(refreshControl: UIRefreshControl) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            refreshControl.endRefreshing()
            return
        }
        
        reloadMessages()
    }
    
    func reloadMessages() {
        guard let token = CredentialStore.sharedInstance.accessToken else {
            return
        }
        
        page = 1
        nextPage = 2
        safeToLoadNextPage = true
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        CuretoRequests.getUserMessages(token, page: page, onCompletion: { result, message in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            self.refreshControl?.endRefreshing()
            
            self.messages = result
            self.tableView.reloadData()
            if result.count > 0 {
                CuretoTableViewHelper.setupLoadMoreForTableView(self.tableView)
            }
            
            CuretoRequests.getUserMessages(token, page: self.nextPage, onCompletion: { result2, message in
                self.nextMessages = result2
                if result2.count == 0 {
                    self.tableView.tableFooterView = nil
                }
            })
        })
    }
    
    func loadNextPage() {
        if nextMessages.count > 0 {
            let firstRowOfNewPage = messages.count
            messages += nextMessages
            let lastRowOfNewPage = messages.count - 1
            
            var rowsToAdd = [NSIndexPath]()
            for i in firstRowOfNewPage...lastRowOfNewPage {
                let indexPath = NSIndexPath(forRow: i, inSection: 0)
                rowsToAdd.append(indexPath)
            }
            
            tableView.insertRowsAtIndexPaths(rowsToAdd, withRowAnimation: .Left)
            
            page += 1
            nextPage += 1
            nextMessages = []

            guard let token = CredentialStore.sharedInstance.accessToken else {
                return
            }
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            CuretoRequests.getUserMessages(token, page: nextPage, onCompletion: { result, message in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.nextMessages = result
                self.safeToLoadNextPage = true
            })
            
        } else {
            tableView.tableFooterView = nil
        }
    }
    
}

// MARK: - Handling scroll view

extension MessagesController {
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        var maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if let height = tabBarController?.tabBar.frame.height {
            maxOffset += height
        }
        
        let rOffset = round(offset)
        let rMaxOffset = round(maxOffset)
        
        if rOffset == rMaxOffset && safeToLoadNextPage {
            safeToLoadNextPage = false
            NSTimer.scheduledTimerWithTimeInterval(CuretoGlobal.paginationDelay, target: self, selector: #selector(MessagesController.loadNextPage), userInfo: nil, repeats: false)
        }
        
    }
    
}
