//
//  FeaturedController.swift
//  Cureto
//
//  Created by Sean Choo on 5/12/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class FeaturedController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerNib(UINib(nibName: "Cover-F1", bundle: nil), forCellReuseIdentifier: "FeaturedCell")
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return (MainScreen.width / 375 * 369) + 16
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FeaturedCell", forIndexPath: indexPath) as! CoverF1
        
        cell.separatorHeight.constant = 0.5
        
        cell.titleLabel.textAlignment = NSTextAlignment.Center
        cell.titleLabel.textColor = MainColor.featuredTitleTint
        let attrs = [NSFontAttributeName: CuretoGlobal.featuredTitleFont]
        let attributedTitle = NSMutableAttributedString(string: "MATCHA", attributes: attrs)
        attributedTitle.addAttribute(NSKernAttributeName, value: 4.0, range: NSMakeRange(0, attributedTitle.length))
        cell.titleLabel.attributedText = attributedTitle
        
        cell.descLabel.textAlignment = NSTextAlignment.Center
        cell.descLabel.textColor = UIColor.darkGrayColor()
        let attrs2 = [NSFontAttributeName: CuretoGlobal.featuredDescFont]
        let attributedDesc = NSMutableAttributedString(string: "EVERYTHING ON GREEN TEA", attributes: attrs2)
        attributedDesc.addAttribute(NSKernAttributeName, value: 1.5, range: NSMakeRange(0, attributedDesc.length))
        cell.descLabel.attributedText = attributedDesc
        
        return cell
    }
    
}
