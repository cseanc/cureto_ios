//
//  HomeController.swift
//  Cureto
//
//  Created by Sean on 11/19/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import Haneke

class HomeController: CuretoTableViewController {
    
    var selectedFoodType: Int = 0
    var selectedPriceRange: Int = 4

    var style = 0
    var seed = Int(arc4random())
    
    // For segmented title view
    var firstViewOptionButton: UIButton?
    var secondViewOptionButton: UIButton?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setSegmentedTitleView()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(articleTableViewReloadRequired(_:)), name: "ArticleTableViewReloadRequired", object: nil)
        
        tableView.registerNib(UINib(nibName: "Cover-A1", bundle: nil), forCellReuseIdentifier: "HomeCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500.0
        
        refreshControl?.addTarget(self, action: #selector(refreshTable(_:)), forControlEvents: .ValueChanged)
        
        reloadArticlesForStyle(style)
    }
    
    override func viewWillAppear(animated: Bool) {
        tabBarController?.tabBar.hidden = false
        
        // For navigation transition
        navigationController?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "home_to_article_segue" {
            let destination = segue.destinationViewController as! ArticleController
            destination.selectedArticle = selectedArticle
            destination.coverImage = toImageView?.image
            destination.presenting = true
            
        } else if segue.identifier == "home_to_filter_segue" {
            let navController = segue.destinationViewController as! UINavigationController
            if let destination = navController.viewControllers[0] as? FilterController {
                destination.homeController = self
                destination.selectedFoodType = selectedFoodType
                destination.selectedPriceRange = selectedPriceRange
            }
        }
    }
    
    func setSegmentedTitleView() {
        let titleView = NSBundle.mainBundle().loadNibNamed("SegmentedTitleView", owner: self, options: nil)[0] as! SegmentedTitleView
        titleView.frame = CGRect(x: 0, y: 0, width: 174, height: 27)
        titleView.titles("Latest", secondTitle: "Curated")
        
        navigationItem.titleView = titleView
        
        titleView.firstButton.addTarget(self, action: #selector(firstViewOptionTapped(_:)), forControlEvents: .TouchUpInside)
        titleView.secondButton.addTarget(self, action: #selector(secondViewOptionTapped(_:)), forControlEvents: .TouchUpInside)
        
        firstViewOptionButton = titleView.firstButton
        secondViewOptionButton = titleView.secondButton
    }
    
    func firstViewOptionTapped(sender: UIButton) {
        style = 0
        reloadArticlesForStyle(style)
        if let titleView = navigationItem.titleView as? SegmentedTitleView {
            titleView.viewOptionsChanged(1)
        }
    }
    
    func secondViewOptionTapped(sender: UIButton) {
        style = 1
        reloadArticlesForStyle(1)
        if let titleView = navigationItem.titleView as? SegmentedTitleView {
            titleView.viewOptionsChanged(2)
        }
    }
    
    // No longer in use
//    func setTitleView() {
//        let titleView = NSBundle.mainBundle().loadNibNamed("HomeTitleView", owner: self, options: nil)[0] as! HomeTitleView
//        titleView.frame = CGRect(x: 0, y: 0, width: 125, height: 40)
//        
//        var filterText = "Everything"
//        
//        if selectedFoodType == 1 {
//            filterText = "Meal"
//        } else if selectedFoodType == 2 {
//            filterText = "Dessert"
//        } else if selectedFoodType == 3 {
//            filterText = "Snack"
//        }
//        
//        if selectedPriceRange == 0 {
//            filterText += " < HK$50"
//        } else if selectedPriceRange == 1 {
//            filterText += " < HK$100"
//        } else if selectedPriceRange == 2 {
//            filterText += " < HK$200"
//        } else if selectedPriceRange == 3 {
//            filterText += " > HK$200"
//        }
//        
//        titleView.filterLabel.text = filterText
//        
//        titleView.actionButton.addTarget(self, action: #selector(showFilter(_:)), forControlEvents: .TouchUpInside)
//        navigationItem.titleView = titleView
//    }
    
    // No longer in use
//    func showFilter(sender: UIButton) {
//        performSegueWithIdentifier("home_to_filter_segue", sender: self)
//    }

}

// MARK: - Handling table view

extension HomeController {
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("HomeCell", forIndexPath: indexPath) as! CoverA1
        let article = articles[indexPath.row]
        return CuretoTableViewHelper.setupCoverCellForArticle(article, row: indexPath.row, cell: cell, tableViewController: self)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        CuretoTableViewHelper.setupTableViewSelectionForPushAnimator(self, tableView: tableView, indexPath: indexPath, segueIdentifier: "home_to_article_segue")
    }
    
    func refreshTable(refreshControl: UIRefreshControl) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            refreshControl.endRefreshing()
            return
        }
        
        reloadArticlesForStyle(style)
    }
    
}

// MARK: - Custom methods

extension HomeController {
    
    func reloadArticlesForStyle(style: Int) {
        page = 1
        nextPage = 2
        safeToLoadNextPage = true
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        if style == 0 {
            CuretoRequests.getHomeArticles(page, style: style, foodType: selectedFoodType, priceRange: selectedPriceRange, onCompletion: { result, message in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.refreshControl?.endRefreshing()
                
                self.articles = result
                self.tableView.reloadData()
                if result.count > 0 {
                    CuretoTableViewHelper.setupLoadMoreForTableView(self.tableView)
                }
                
                CuretoRequests.getHomeArticles(self.nextPage, style: style, foodType: self.selectedFoodType, priceRange: self.selectedPriceRange, onCompletion: { result2, message2 in
                    self.nextArticles = result2
                    if result2.count == 0 {
                        self.tableView.tableFooterView = nil
                    }
                })
                
            })
            
        } else {
            seed = Int(arc4random())
            CuretoRequests.getCuratedArticles(seed, page: page, onCompletion: { result, message in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.refreshControl?.endRefreshing()
                
                self.articles = result
                self.tableView.reloadData()
                if result.count > 0 {
                    CuretoTableViewHelper.setupLoadMoreForTableView(self.tableView)
                }
                
                CuretoRequests.getCuratedArticles(self.seed, page: self.nextPage, onCompletion: { result2, message2 in
                    self.nextArticles = result2
                    if result2.count == 0 {
                        self.tableView.tableFooterView = nil
                    }
                })
                
            })
        }
        
    }
    
    func loadNextPageForStyle() {
        if nextArticles.count > 0 {
            let firstRowOfNewPage = articles.count
            articles += nextArticles
            let lastRowOfNewPage = articles.count - 1
            
            var rowsToAdd = [NSIndexPath]()
            for i in firstRowOfNewPage...lastRowOfNewPage {
                let indexPath = NSIndexPath(forRow: i, inSection: 0)
                rowsToAdd.append(indexPath)
            }
            
            tableView.insertRowsAtIndexPaths(rowsToAdd, withRowAnimation: .Left)
            
            page += 1
            nextPage += 1
            nextArticles = []
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            
            if style == 0 {
                CuretoRequests.getHomeArticles(nextPage, style: style, foodType: selectedFoodType, priceRange: selectedPriceRange, onCompletion: { result, message in
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    self.nextArticles = result
                    self.safeToLoadNextPage = true
                })
                
            } else {
                CuretoRequests.getCuratedArticles(seed, page: nextPage, onCompletion: { result, message in
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    self.nextArticles = result
                    self.safeToLoadNextPage = true
                })
                
            }
            
        } else {
            tableView.tableFooterView = nil
        }
    }
    
    func articleTableViewReloadRequired(sender: NSNotification) {
        reloadArticlesForStyle(style)
    }
}

// MARK: - Handling Scroll View

extension HomeController {
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        let maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
//        print("offset: \(offset) maxOffset: \(maxOffset)")
//        print(safeToLoadNextPage)
//        
//        print(round(offset))
//        print(round(maxOffset))
        
        let rOffset = round(offset)
        let rMaxOffset = round(maxOffset)
        
        if rOffset == rMaxOffset && safeToLoadNextPage {
            safeToLoadNextPage = false
            NSTimer.scheduledTimerWithTimeInterval(CuretoGlobal.paginationDelay, target: self, selector: #selector(HomeController.loadNextPageForStyle), userInfo: nil, repeats: false)
        }
    }
}