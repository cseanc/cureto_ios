//
//  SubcontentEditController.swift
//  Cureto
//
//  Created by Sean on 12/28/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class SubcontentEditController: UIViewController, UIGestureRecognizerDelegate {
    
    var subImagePickerController: UIImagePickerController!
    
    var textViewTimer: NSTimer?
    var templateController: TemplateController?
    var subcontentRow: Int?
    var selectedSubcontent: PhotoWithSubtitle?
    
    var subPhoto: UIImage?
    var subtitle: String = ""
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var subTableView: UITableView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        separatorHeight.constant = 0.5
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        if let subcontent = selectedSubcontent {
            if let photo = subcontent.photo, text = subcontent.subtitle {
                subPhoto = photo
                subtitle = text
            }
        } else {
            navigationController?.popViewControllerAnimated(true)
        }
        
        subTableView.rowHeight = UITableViewAutomaticDimension
        subTableView.estimatedRowHeight = 500.0
        
        subTableView.registerNib(UINib(nibName: "SubPhoto-Edit", bundle: nil), forCellReuseIdentifier: "SubPhotoCell")
        subTableView.registerNib(UINib(nibName: "Subtitle", bundle: nil), forCellReuseIdentifier: "SubtitleCell")
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(subContentEditChanged(_:)), name: "SubContentEditChanged", object: nil)
        
        saveButton.enabled = false
        saveButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
    }
    
//    override func viewWillAppear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: false)
//    }
//    
//    override func viewWillDisappear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: true)
//    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func subContentEditChanged(notification: NSNotification) {
        if let _  = subPhoto {
            if subtitle == "" {
                saveButton.enabled = false
                saveButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
            } else {
                saveButton.enabled = true
                saveButton.setTitleColor(MainColor.globalTint, forState: .Normal)
            }
        }
    }

    @IBAction func removeSubcontent(sender: AnyObject) {
        presentDeleteAlert()
    }
    
    func presentDeleteAlert() {
        let alertController = UIAlertController(title: "Delete Content", message: "This content will be deleted", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: { action in
            guard let template = self.templateController, row = self.subcontentRow else {
                return
            }
            
            template.subContent.removeAtIndex(row - 2)
            template.designTableView.reloadData()
            self.navigationController?.popViewControllerAnimated(true)
        })
        
        alertController.addAction(deleteAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func save(sender: AnyObject) {
        guard let template = templateController, row = subcontentRow else {
            return
        }
        
        if let subcontent = template.selectedSubcontent {
            subcontent.photo = subPhoto
            subcontent.subtitle = subtitle
            if let subphotoDidChange = selectedSubcontent?.subphotoDidChange {
                subcontent.subphotoDidChange = subphotoDidChange
            }
            
            let indexPath = NSIndexPath(forRow: row, inSection: 0)
            template.reloadSubcontentAtIndexPath(indexPath)
            
            navigationController?.popViewControllerAnimated(true)
        }

    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
}

// MARK: - Handling table view

extension SubcontentEditController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            if let photo = subPhoto {
                let heightRatio = photo.size.height / photo.size.width
                return MainScreen.width * heightRatio
            } else {
                return MainScreen.width / 4 * 3
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cellToShow: UITableViewCell!
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("SubPhotoCell", forIndexPath: indexPath) as! SubPhotoEdit
            cell.changeButton.addTarget(self, action: #selector(changeButtonTapped(_:)), forControlEvents: .TouchUpInside)
            if let photo = subPhoto {
                cell.subPhotoView.image = photo
            }
            cellToShow = cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("SubtitleCell", forIndexPath: indexPath) as! Subtitle
            cell.subtitleTextView.delegate = self
            
            let paraStyle = NSMutableParagraphStyle()
            paraStyle.lineSpacing = CuretoGlobal.lineSpacing
            let attrs = [NSFontAttributeName: CuretoGlobal.contentFont,
                NSParagraphStyleAttributeName: paraStyle]
            
            cell.subtitleTextView.typingAttributes = attrs
            cell.subtitleTextView.textAlignment = .Left
            
            if subtitle == "" {
                cell.subtitleTextView.attributedText = NSMutableAttributedString(string: "Enter description here", attributes: attrs)
                cell.subtitleTextView.textColor = MainColor.placeholderTint
            } else {
                cell.subtitleTextView.attributedText = NSMutableAttributedString(string: subtitle, attributes: attrs)
                cell.subtitleTextView.textColor = MainColor.contentTint
            }
            cellToShow = cell
        }
        return cellToShow
    }
    
}

// MARK: - Handling textview

extension SubcontentEditController: UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if subtitle == "" {
            textView.text = ""
        } else {
            textView.text = subtitle
        }
        textView.textColor = UIColor.blackColor()
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        textViewTimer?.invalidate()
        textViewTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(typingPaused(_:)), userInfo: textView, repeats: false)
        return true
    }
    
    func typingPaused(timer: NSTimer) {
        if let textView = timer.userInfo as? UITextView {
            if textView.text != "" {
                subtitle = textView.text
            } else {
                subtitle = ""
            }
            NSNotificationCenter.defaultCenter().postNotificationName("SubContentEditChanged", object: nil)
        }
    }
    
}

// MARK: - Handling image picker

extension SubcontentEditController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func changeButtonTapped(sender: UIButton) {
        pickSubImageFromSource(CuretoGlobal.sourcePhotoLibrary)
    }
    
    func pickSubImageFromSource(source: String) {
        subImagePickerController = UIImagePickerController()
        subImagePickerController.delegate = self
        
        if source == CuretoGlobal.sourcePhotoLibrary {
            subImagePickerController.sourceType = .PhotoLibrary
        } else {
            subImagePickerController.sourceType = .Camera
        }
        
        presentViewController(subImagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let _ = subImagePickerController {
            if picker == subImagePickerController {
                if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    if let photo = CuretoImageHelper.resizeImageWithMaxWidth(pickedImage, maxWidth: 1080.0, quality: 0.95) {
                        subPhoto = photo
                        selectedSubcontent?.subphotoDidChange = true
                        subTableView.reloadData()
                        
                        dismissViewControllerAnimated(true, completion: {
                            NSNotificationCenter.defaultCenter().postNotificationName("SubContentEditChanged", object: nil)
                        })
                    }
                }
            }
        }
    }
    
}
