//
//  FeaturedArticlesController.swift
//  Cureto
//
//  Created by Sean Choo on 5/16/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class FeaturedArticlesController: CuretoTableViewController {

    var selectedMagazine: FeaturedGroup?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = selectedMagazine?.title?.uppercaseString
        
        tableView.registerNib(UINib(nibName: "Cover-A1", bundle: nil), forCellReuseIdentifier: "ArticleCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500.0
        
        reloadArticles()
    }
    
    override func viewWillAppear(animated: Bool) {
        // For navigation transition
        navigationController?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "featured_articles_to_article_segue" {
            let destination = segue.destinationViewController as! ArticleController
            destination.selectedArticle = selectedArticle
            destination.coverImage = toImageView?.image
            destination.presenting = true
        }
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ArticleCell", forIndexPath: indexPath) as! CoverA1
        let article = articles[indexPath.row]
        return CuretoTableViewHelper.setupCoverCellForArticle(article, row: indexPath.row, cell: cell, tableViewController: self)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        CuretoTableViewHelper.setupTableViewSelectionForPushAnimator(self, tableView: tableView, indexPath: indexPath, segueIdentifier: "featured_articles_to_article_segue")
    }
    
    func reloadArticles() {
        guard let id = selectedMagazine?.id else {
            return
        }
        
        CuretoRequests.getFeaturedElements(id, onCompletion: { result in
            
            var ids = ""
            for element in result {
                if let targetId = element.targetId {
                    ids += "\(targetId),"
                }
            }
            
            CuretoRequests.getFeaturedGroupArticles(ids, onCompletion: { result2, message in
                self.articles = result2
                self.tableView.reloadData()
            })
        })
    }
}
