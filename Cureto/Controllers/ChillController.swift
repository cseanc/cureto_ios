//
//  ChillController.swift
//  Cureto
//
//  Created by Sean Choo on 4/30/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class ChillController: UITableViewController {
    
    // For navigation transition
    var navigationBarHeight: CGFloat = 64
    var initialFrame = CGRectZero
    var toImageView: UIImageView?
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    // For magazines
    var selectedMagazine: FeaturedGroup?
    var magazines = [FeaturedGroup]()
    
    var selectedArticle: Article?
    
    var foodToPresent = [Int]()
    
    var selectedArea = CuretoGlobal.everywhere
    
    var loadComplete = false
    var loadNeeded = 6
    var loaded = 0
    
    var articlesTier50 = [Article]()
    var articlesTier100 = [Article]()
    var articlesTier200 = [Article]()
    var articlesTier300 = [Article]()
    var articlesDessert = [Article]()
    var articlesSnack = [Article]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // For navigation transition
        var statusBarHeight = UIApplication.sharedApplication().statusBarFrame.size.height
        if statusBarHeight == 0 {
            statusBarHeight = 20
        }
        if let navBarHeight = navigationController?.navigationBar.frame.size.height {
            navigationBarHeight = statusBarHeight + navBarHeight
        }
        
        tableView.registerNib(UINib(nibName: "Cover-F1", bundle: nil), forCellReuseIdentifier: "FeaturedCell")
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        
        setTitleView()
        
        if selectedArea != CuretoGlobal.everywhere {
            loadArticles()
        } else {
            loadMagazines()
        }
        
        refreshControl?.addTarget(self, action: #selector(refreshTable(_:)), forControlEvents: .ValueChanged)
    }
    
    override func viewWillAppear(animated: Bool) {
        // For navigation transition
        navigationController?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        if selectedArea == CuretoGlobal.everywhere {
            if let storedDate = userDefaults.valueForKey(CuretoGlobal.featuredLastUpdated) as? String {
                CuretoRequests.getFeaturedLastUpdated({ result in
                    if let date = result {
                        if storedDate != date {
                            self.userDefaults.setValue(date, forKey: CuretoGlobal.featuredLastUpdated)
                            self.loadMagazines()
                        }
                    }
                })
                
            } else {
                CuretoRequests.getFeaturedLastUpdated({ result in
                    if let date = result {
                        self.userDefaults.setValue(date, forKey: CuretoGlobal.featuredLastUpdated)
                    }
                })
            }
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem

        if segue.identifier == "chill_to_filter_segue" {
            let navController = segue.destinationViewController as! UINavigationController
            if let destination = navController.viewControllers[0] as? ChillFilterController {
                destination.chillController = self
                destination.selectedArea = selectedArea
            }
            
        } else if segue.identifier == "chill_to_article_segue" {
            let destination = segue.destinationViewController as! ArticleController
            destination.selectedArticle = selectedArticle
            destination.coverImage = toImageView?.image
            destination.presenting = true
            
        } else if segue.identifier == "chill_to_curator_show_segue" {
            let destination = segue.destinationViewController as! CuratorShowController
            destination.selectedMagazineId = selectedMagazine?.id
            
        } else if segue.identifier == "chill_to_featured_articles_segue" {
            let destination = segue.destinationViewController as! FeaturedArticlesController
            destination.selectedMagazine = selectedMagazine
        }
    }
    
    func setTitleView() {
        let titleView = NSBundle.mainBundle().loadNibNamed("HomeTitleView", owner: self, options: nil)[0] as! HomeTitleView
        titleView.frame = CGRect(x: 0, y: 0, width: 125, height: 40)
        titleView.titleLabel.text = "Chill"
        titleView.filterLabel.text = selectedArea

        titleView.actionButton.addTarget(self, action: #selector(showFilter(_:)), forControlEvents: .TouchUpInside)
        navigationItem.titleView = titleView
    }
    
    func showFilter(sender: UIButton) {
        performSegueWithIdentifier("chill_to_filter_segue", sender: self)
    }
    
    func refreshTable(refreshControl: UIRefreshControl) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            refreshControl.endRefreshing()
            return
        }
        
        if selectedArea == CuretoGlobal.everywhere {
            loadMagazines()
        } else {
            loadArticles()
        }
    }
    
    func loadMagazines() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        CuretoRequests.getFeaturedGroups({ result in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            self.magazines = result
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        })
    }
    
    func loadArticles() {
        loadComplete = false
        loaded = 0
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        CuretoRequests.getChillArticles(selectedArea, type: 0, lowerPrice: 0, upperPrice: 50, onCompletion: { result in
            self.articlesTier50 = result
            self.loaded += 1
            self.updateLoadStatus()
        })
        
        CuretoRequests.getChillArticles(selectedArea, type: 0, lowerPrice: 50, upperPrice: 100, onCompletion: { result in
            self.articlesTier100 = result
            self.loaded += 1
            self.updateLoadStatus()
        })
        
        CuretoRequests.getChillArticles(selectedArea, type: 0, lowerPrice: 100, upperPrice: 200, onCompletion: { result in
            self.articlesTier200 = result
            self.loaded += 1
            self.updateLoadStatus()
        })
        
        CuretoRequests.getChillArticles(selectedArea, type: 0, lowerPrice: 200, upperPrice: 10000, onCompletion: { result in
            self.articlesTier300 = result
            self.loaded += 1
            self.updateLoadStatus()
        })
        
        CuretoRequests.getChillArticles(selectedArea, type: 1, lowerPrice: 0, upperPrice: 10000, onCompletion: { result in
            self.articlesDessert = result
            self.loaded += 1
            self.updateLoadStatus()
        })
        
        CuretoRequests.getChillArticles(selectedArea, type: 2, lowerPrice: 0, upperPrice: 10000, onCompletion: { result in
            self.articlesSnack = result
            self.loaded += 1
            self.updateLoadStatus()
        })
        
    }
    
    func updateLoadStatus() {
        if loaded == loadNeeded {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            loadComplete = true
            refreshControl?.endRefreshing()
            tableView.reloadData()
        } else {
            loadComplete = false
        }
    }
    
    func rowsForMeal() -> [Int] {
        var rows = [Int]()
        
        if articlesTier50.count > 0 {
            rows.append(50)
        }
        
        if articlesTier100.count > 0 {
            rows.append(100)
        }
        
        if articlesTier200.count > 0 {
            rows.append(200)
        }
        
        if articlesTier300.count > 0 {
            rows.append(300)
        }
        
        return rows
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if selectedArea == CuretoGlobal.everywhere {
            return 1
            
        } else {
            if loadComplete {
                foodToPresent = []
                if articlesTier50.count > 0 || articlesTier100.count > 0 || articlesTier200.count > 0 || articlesTier300.count > 0 {
                    foodToPresent.append(0)
                }
                if articlesDessert.count > 0 {
                    foodToPresent.append(1)
                }
                if articlesSnack.count > 0 {
                    foodToPresent.append(2)
                }
                
                return foodToPresent.count
                
            } else {
                return 0
            }
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if selectedArea == CuretoGlobal.everywhere {
            return (MainScreen.width / 375 * 369) + 16
        } else {
            return 305
        }
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedArea == CuretoGlobal.everywhere {
            return magazines.count
            
        } else {
            let foodType = foodToPresent[section]
            
            switch foodType {
            case 0:
                return rowsForMeal().count
            default:
                return 1
            }
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if selectedArea == CuretoGlobal.everywhere {
            let cell = tableView.dequeueReusableCellWithIdentifier("FeaturedCell", forIndexPath: indexPath) as! CoverF1
            cell.separatorHeight.constant = 0.5
            
            let magazine = magazines[indexPath.row]
            
            if MainScreen.width == 320 {
                cell.titleTopSpace.constant = 16
            }
            
            switch MainScreen.width {
            case 320:
                cell.titleTopSpace.constant = 16
            case 414:
                cell.titleTopSpace.constant = 27
            default:
                cell.titleTopSpace.constant = 21
            }
            
            var titleString = "--"
            if let title = magazine.title {
                titleString = title.uppercaseString
            }
            
            cell.titleLabel.textAlignment = NSTextAlignment.Center
            cell.titleLabel.textColor = MainColor.featuredTitleTint
            let attrs = [NSFontAttributeName: CuretoGlobal.featuredTitleFont]
            let attributedTitle = NSMutableAttributedString(string: titleString, attributes: attrs)
            attributedTitle.addAttribute(NSKernAttributeName, value: 2.5, range: NSMakeRange(0, attributedTitle.length))
            cell.titleLabel.attributedText = attributedTitle
            
            var descString = "--"
            if let desc = magazine.desc {
                descString = desc.uppercaseString
            }
            
            cell.descLabel.textAlignment = NSTextAlignment.Center
            cell.descLabel.textColor = UIColor.darkGrayColor()
            let attrs2 = [NSFontAttributeName: CuretoGlobal.featuredDescFont]
            let attributedDesc = NSMutableAttributedString(string: descString, attributes: attrs2)
            attributedDesc.addAttribute(NSKernAttributeName, value: 1.5, range: NSMakeRange(0, attributedDesc.length))
            cell.descLabel.attributedText = attributedDesc
            
            var shadeTint = UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 0.15)
            if let opacity = magazine.opacity {
                let floatedOpacity = CGFloat(opacity) / 100.0
                if floatedOpacity != 0.15 {
                    shadeTint = UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: floatedOpacity)
                }
            }
            
            cell.shadeView.backgroundColor = shadeTint
            
            cell.coverImageView.image = UIImage(named: CuretoGlobal.placeholderImageName)
            if let coverUrl = magazine.coverPhotoUrl {
                if let url = NSURL(string: coverUrl) {
                    cell.coverImageView.hnk_setImageFromURL(url)
                }
            }
            
            cell.touchFace.tag = indexPath.row
            cell.touchFace.addTarget(self, action: #selector(ChillController.magazineTapped), forControlEvents: .TouchUpInside)
            
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("ChillCell", forIndexPath: indexPath) as! ChillTableViewCell
            
            let foodType = foodToPresent[indexPath.section]
            
            switch foodType {
            case 0:
                let rows = rowsForMeal()
                let row = rows[indexPath.row]
                switch row {
                case 50:
                    cell.chillCollectionView.tag = 0
                    cell.priceLabel.text = "HK$ 0 - 50"
                case 100:
                    cell.chillCollectionView.tag = 1
                    cell.priceLabel.text = "HK$ 50 - 100"
                case 200:
                    cell.chillCollectionView.tag = 2
                    cell.priceLabel.text = "HK$ 100 - 200"
                case 300:
                    cell.chillCollectionView.tag = 3
                    cell.priceLabel.text = "HK$ 200 +"
                default:
                    break
                }
            case 1:
                cell.chillCollectionView.tag = 4
                cell.priceLabel.text = "All"
            case 2:
                cell.chillCollectionView.tag = 5
                cell.priceLabel.text = "All"
            default:
                break
            }
            
            cell.chillCollectionView.delegate = self
            cell.chillCollectionView.dataSource = self
            cell.chillCollectionView.registerNib(UINib(nibName: "Cover-G1", bundle: nil), forCellWithReuseIdentifier: "ChillCell")
            cell.chillCollectionView.reloadData()
            
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if selectedArea == CuretoGlobal.everywhere {
            return 0
        } else {
            return 38
        }
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if selectedArea == CuretoGlobal.everywhere {
            return nil
            
        } else {
            let headerView = NSBundle.mainBundle().loadNibNamed("ChillTableViewSectionHeader", owner: self, options: nil)[0] as! ChillTableViewSectionHeader
            headerView.frame = CGRectMake(0, 0, MainScreen.width, 38)
            
            let foodType = foodToPresent[section]
            switch foodType {
            case 0:
                headerView.headerLabel.text = "Meal"
            case 1:
                headerView.headerLabel.text = "Dessert"
            case 2:
                headerView.headerLabel.text = "Snack"
            default:
                break
            }
            
            return headerView
        }
    }
    
    func magazineTapped(sender: UIButton) {
        let magazine = magazines[sender.tag]
        selectedMagazine = magazine
        
        if let type = magazine.featuredType {
            switch type {
            case 2:
                performSegueWithIdentifier("chill_to_curator_show_segue", sender: self)
            default:
                performSegueWithIdentifier("chill_to_featured_articles_segue", sender: self)
            }
        }
    }
    
}

extension ChillController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0:
            return articlesTier50.count
        case 1:
            return articlesTier100.count
        case 2:
            return articlesTier200.count
        case 3:
            return articlesTier300.count
        case 4:
            return articlesDessert.count
        case 5:
            return articlesSnack.count
        default:
            return 0
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ChillCell", forIndexPath: indexPath) as! CoverG1
        cell.contentView.layer.borderWidth = 0.5
        cell.contentView.layer.borderColor = MainColor.separatorTint.CGColor
        cell.separatorHeight.constant = 0.5
        
        cell.imageView.image = UIImage(named: CuretoGlobal.placeholderImageName)
        
        var article: Article?
        
        switch collectionView.tag {
        case 0:
            article = articlesTier50[indexPath.item]
        case 1:
            article = articlesTier100[indexPath.item]
        case 2:
            article = articlesTier200[indexPath.item]
        case 3:
            article = articlesTier300[indexPath.item]
        case 4:
            article = articlesDessert[indexPath.item]
        case 5:
            article = articlesSnack[indexPath.item]
        default:
            break
        }

        if let a = article {
            cell.titleLabel.text = a.title
            cell.locationLabel.text = a.restaurant?.name
            
            if selectedArea == CuretoGlobal.everywhere {
                if let city = a.restaurant?.city {
                    if city != "" {
                        cell.authorLabel.text = city
                    } else {
                        cell.authorLabel.text = "--"
                    }
                } else {
                    cell.authorLabel.text = "--"
                }
            } else {
                cell.authorLabel.text = a.author?.displayName
            }
            
            if let thumbUrl = a.coverPhotoThumbUrl {
                if let url = NSURL(string: thumbUrl) {
                    cell.imageView.hnk_setImageFromURL(url)
                }
            }
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var article: Article?
        
        switch collectionView.tag {
        case 0:
            article = articlesTier50[indexPath.item]
        case 1:
            article = articlesTier100[indexPath.item]
        case 2:
            article = articlesTier200[indexPath.item]
        case 3:
            article = articlesTier300[indexPath.item]
        case 4:
            article = articlesDessert[indexPath.item]
        case 5:
            article = articlesSnack[indexPath.item]
        default:
            break
        }
        
        // For navigation transitioning
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CoverG1
        initialFrame = cell.convertRect(cell.imageView.frame, toView: nil)
        
        if let width = article?.coverPhotoWidth, height = article?.coverPhotoHeight {
            let heightRatio = height / width
            toImageView = UIImageView(frame: CGRectMake(0, navigationBarHeight, MainScreen.width, MainScreen.width * heightRatio))
            toImageView?.image = cell.imageView.image
        }
        
        selectedArticle = article
        performSegueWithIdentifier("chill_to_article_segue", sender: self)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(182, 260)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
    }
    
}

extension ChillController: UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    // For navigation transition
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        guard let _ = toVC as? ArticleController else {
            return nil
        }
        
        if operation == UINavigationControllerOperation.Push {
            let pushAnimator = CuretoPushAnimator()
            pushAnimator.initialFrame = initialFrame
            pushAnimator.toImageView = toImageView
            pushAnimator.fromCollectionView = true
            return pushAnimator
        }
        
        return nil
    }
    
}