//
//  ChangePasswordController.swift
//  Cureto
//
//  Created by Sean Choo on 4/18/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class ChangePasswordController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var originalTextField: UITextField!
    @IBOutlet weak var newTextField: UITextField!
    
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        separatorHeight.constant = 0.5
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBarHidden = false
    }
    
    override func viewDidAppear(animated: Bool) {
        originalTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        navigationController?.navigationBarHidden = true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let inverseSet = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._+-!@").invertedSet
        let components = string.componentsSeparatedByCharactersInSet(inverseSet)
        let filtered = components.joinWithSeparator("")
        return string == filtered
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case originalTextField:
            newTextField.becomeFirstResponder()
        default:
            view.endEditing(true)
        }
        
        return true
    }
    
    @IBAction func done(sender: AnyObject) {
        guard let token = CredentialStore.sharedInstance.accessToken, original = originalTextField.text, new = newTextField.text else {
            return
        }
        
        if original != "" && new != "" {
            CuretoProgressHUD.sharedInstance.startSpinning(view)
            CuretoRequests.updatePassword(token, original: original, new: new, onCompletion: { succeeded, message in
                CuretoProgressHUD.sharedInstance.stopSpinning()
                
                if succeeded {
                    CuretoBannerAlert.showMessage("Password changed!")
                    self.navigationController?.popViewControllerAnimated(true)
                    
                } else {
                    CuretoBannerAlert.showMessage(message)
                }
            })
            
        } else {
            CuretoBannerAlert.showMessage("Password cannot be blank")
        }
    }
    
}
