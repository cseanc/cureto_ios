//
//  CuratorSearchController.swift
//  Cureto
//
//  Created by Sean Choo on 4/1/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuratorSearchController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var selectedCurator: User?
    
    var curators = [User]()
    
    @IBOutlet weak var curatorSearchBar: UISearchBar!
    @IBOutlet weak var curatorsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        curatorsTableView.contentInset = UIEdgeInsetsMake(0, 0, 49, 0)
        
        curatorSearchBar.autocapitalizationType = UITextAutocapitalizationType.None
    }
    
    override func viewDidAppear(animated: Bool) {
        curatorSearchBar.becomeFirstResponder()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "curator_search_to_show_segue" {
            let destination = segue.destinationViewController as! CuratorShowController
            destination.selectedCurator = selectedCurator
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return curators.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CuratorCell", forIndexPath: indexPath)
        
        let curator = curators[indexPath.row]
        
        cell.textLabel?.text = curator.displayName
        
        if let username = curator.username {
            cell.detailTextLabel?.text = "@\(username)"
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        selectedCurator = curators[indexPath.row]
        performSegueWithIdentifier("curator_search_to_show_segue", sender: self)
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        curatorSearchBar.resignFirstResponder()
    }
    
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        let inverseSet = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyz0123456789._").invertedSet
        let components = text.componentsSeparatedByCharactersInSet(inverseSet)
        let filtered = components.joinWithSeparator("")
        return text == filtered
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != "" {
            CuretoRequests.searchCurators(searchText, onCompletion: { result, message in
                self.curators = result
                self.curatorsTableView.reloadData()
            })
        } else {
            curators = []
            self.curatorsTableView.reloadData()
        }
    }
}
