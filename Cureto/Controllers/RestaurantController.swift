//
//  RestaurantController.swift
//  Cureto
//
//  Created by Sean Choo on 1/22/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SafariServices

class RestaurantController: CuretoTableViewController {
    
//    var selectedArticle: Article?
    
    // For articles
//    var articles = [Article]()
//    var nextArticles = [Article]()
//    var page = 1
//    var nextPage = 2
//    var safeToLoadNextPage = true
    
    var restaurantId: Int?
    var currentRestaurant: Restaurant?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBarHidden = false
        navigationItem.title = "Restaurant"
        
        tableView.registerNib(UINib(nibName: "RestaurantInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoCell")
        tableView.registerNib(UINib(nibName: "Cover-A1", bundle: nil), forCellReuseIdentifier: "ArticleCell")
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500.0
        
//        CuretoRequests.publicAuthWithFoursquare({ (result: FoursquareAuth?, message: String) in
//            if let auth = result, identifier = self.currentRestaurant?.identifier {
//                self.getRestaurantInfo(auth, id: identifier)
//            }
//        })
        
        reloadArticles()
    }
    
    override func viewWillAppear(animated: Bool) {
        // For navigation transition
        navigationController?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "restaurant_to_article_segue" {
            let destination = segue.destinationViewController as! ArticleController
            destination.selectedArticle = selectedArticle
            destination.coverImage = toImageView?.image
            destination.presenting = true
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 36
        }
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 {
            return 1
        } else {
            return articles.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var returnCell: UITableViewCell!
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("InfoCell", forIndexPath: indexPath) as! RestaurantInfoTableViewCell
            cell.separatorHeight.constant = 0.5
            cell.webButton.addTarget(self, action: #selector(webTapped(_:)), forControlEvents: .TouchUpInside)
            cell.foursquareButton.addTarget(self, action: #selector(foursquareTapped(_:)), forControlEvents: .TouchUpInside)
            cell.mapButton.addTarget(self, action: #selector(mapTapped(_:)), forControlEvents: .TouchUpInside)
            if let restaurant = currentRestaurant {
                if let lat = restaurant.latitude, lng = restaurant.longitude {
                    
                    if let name = restaurant.name {
                        cell.nameLabel.text = name
                    } else {
                        cell.nameLabel.text = "--"
                    }
                    
                    if var addressString = restaurant.address {
                        if let city = restaurant.city {
                            addressString += ", \(city)"
                        }
                        cell.addressLabel.text = addressString
                    } else {
                        cell.addressLabel.text = "--"
                    }
                    
                    let location = CLLocation(latitude: lat, longitude: lng)
                    let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 350.0, 350.0)
                    cell.mapView.setRegion(coordinateRegion, animated: false)
                    
                    let pin = MKPointAnnotation()
                    pin.coordinate = location.coordinate
                    cell.mapView.addAnnotation(pin)
                }
            }
            returnCell = cell
            
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("HeaderCell", forIndexPath: indexPath)
            returnCell = cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("ArticleCell", forIndexPath: indexPath) as! CoverA1
            let article = articles[indexPath.row]
            returnCell = CuretoTableViewHelper.setupCoverCellForArticle(article, row: indexPath.row, cell: cell, tableViewController: self)
        }
        
        returnCell.selectionStyle = .None
        
        return returnCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 2 {
            CuretoTableViewHelper.setupTableViewSelectionForPushAnimator(self, tableView: tableView, indexPath: indexPath, segueIdentifier: "restaurant_to_article_segue")
        }
    }
    
//    func cellTapped(sender: UIButton) {
//        selectedArticle = articles[sender.tag]
//        performSegueWithIdentifier("restaurant_to_article_segue", sender: self)
//    }
    
    func webTapped(sender: UIButton) {
        guard let name = currentRestaurant?.name else {
            return
        }
        
        let searchAddress = "https://www.google.com.hk/#q=\(name.searchOptimized().urlSafe())"
        if let url = NSURL(string: searchAddress) {
            if #available(iOS 9, *) {
                let svc = SFSafariViewController(URL: url)
                presentViewController(svc, animated: true, completion: nil)
            } else {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    
    func foursquareTapped(sender: UIButton) {
        guard let id = currentRestaurant?.identifier else {
            return
        }
        
        let searchAddress = "https://foursquare.com/v/\(id)"
        if let url = NSURL(string: searchAddress) {
            if #available(iOS 9, *) {
                let svc = SFSafariViewController(URL: url)
                presentViewController(svc, animated: true, completion: nil)
            } else {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    
    func mapTapped(sender: UIButton) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
        })
        
        let copyAction = UIAlertAction(title: "Copy Address", style: .Default, handler: { action in
            if let address = self.currentRestaurant?.address {
                UIPasteboard.generalPasteboard().string = address
                CuretoAlerts.notifyUserWith(.AddressCopied, viewController: self)
            }
        })
        
        let googleMapAction = UIAlertAction(title: "Open in Google Maps", style: .Default, handler: { action in
            self.openPlaceInGoogleMap()
        })
        
        let mapAction = UIAlertAction(title: "Open in Maps", style: .Default, handler: { action in
            self.openPlaceInMap()
        })
        
        alertController.addAction(copyAction)
        alertController.addAction(googleMapAction)
        alertController.addAction(mapAction)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func openPlaceInGoogleMap() {
        guard let lat = currentRestaurant?.latitude, lng = currentRestaurant?.longitude else {
            return
        }
        
        let addressString = "comgooglemaps://?q=\(lat),\(lng)"
        
        if let url = NSURL(string: addressString) {
            if UIApplication.sharedApplication().canOpenURL(url) {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    
    func openPlaceInMap() {
        guard let lat = currentRestaurant?.latitude, lng = currentRestaurant?.longitude else {
            return
        }
        
        let regionDistance:CLLocationDistance = 900
        let coordinates = CLLocationCoordinate2DMake(lat, lng)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = currentRestaurant?.name
        mapItem.openInMapsWithLaunchOptions(options)
    }
    
    func getRestaurantInfo(auth: FoursquareAuth, id: String) {
        CuretoRequests.getPlaceInfo(auth, id: id, onCompletion: { (place, message) in
            self.currentRestaurant = place
            self.tableView.reloadSections(CuretoGlobal.tableViewFirstSectionIndex, withRowAnimation: .Fade)
        })
    }
    
}

// MARK: - Handling articles loading

extension RestaurantController {
    
    func reloadArticles() {
        guard let id = restaurantId else {
            return
        }
        
        page = 1
        nextPage = 2
        safeToLoadNextPage = true
        
        CuretoRequests.getRestaurantArticles(page, id: id, onCompletion: { result, message in
            self.articles = result
            self.tableView.reloadSections(CuretoGlobal.tableViewThirdSectionIndex, withRowAnimation: .Top)
            if result.count > 0 {
                CuretoTableViewHelper.setupLoadMoreForTableView(self.tableView)
            }
            CuretoRequests.getRestaurantArticles(self.nextPage, id: id, onCompletion: { result2, message in
                self.nextArticles = result2
                if result2.count == 0 {
                    self.tableView.tableFooterView = nil
                }
            })
        })
        
    }
    
    func loadNextPage() {
        if nextArticles.count > 0 {
            let firstRowOfNewPage = articles.count
            articles += nextArticles
            let lastRowOfNewPage = articles.count - 1
            
            var rowsToAdd = [NSIndexPath]()
            for i in firstRowOfNewPage...lastRowOfNewPage {
                let indexPath = NSIndexPath(forRow: i, inSection: 2)
                rowsToAdd.append(indexPath)
            }
            
            tableView.insertRowsAtIndexPaths(rowsToAdd, withRowAnimation: .Left)
            
            page += 1
            nextPage += 1
            nextArticles = []
                        
            guard let id = restaurantId else {
                return
            }
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            CuretoRequests.getRestaurantArticles(nextPage, id: id, onCompletion: { result, message in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.nextArticles = result
                self.safeToLoadNextPage = true
            })
            
        } else {
            tableView.tableFooterView = nil
        }
    }
    
}

// MARK: - Handling Scroll View

extension RestaurantController {
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        var maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
//        print("offset: \(offset) maxOffset: \(maxOffset)")
//        print(safeToLoadNextPage)
        
        if let height = tabBarController?.tabBar.frame.height {
            maxOffset += height
        }
        
        let rOffset = round(offset)
        let rMaxOffset = round(maxOffset)
        
        if rOffset == rMaxOffset && safeToLoadNextPage {
            safeToLoadNextPage = false
            NSTimer.scheduledTimerWithTimeInterval(CuretoGlobal.paginationDelay, target: self, selector: #selector(RestaurantController.loadNextPage), userInfo: nil, repeats: false)
        }
        
    }
}
