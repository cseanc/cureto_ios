//
//  PublishController.swift
//  Cureto
//
//  Created by Sean on 11/17/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class PublishController: UIViewController, UIGestureRecognizerDelegate {
    
    // Booleans from template controller
    var editMode = false
    var published = false
    var coverDidChange = false
    
    // For edit
    var originalSubContent = [PhotoWithSubtitle]()
    
    // From template controller
    var articleController: UIViewController?
    var templateController: TemplateController?
    var coverPhotoLocation: CLLocation?
    var articleHead: ArticleHead?
    var subContent = [PhotoWithSubtitle]()
    
    var location: Restaurant?
    var mealType: CuretoMealType?
    var mealPriceString: String = ""
    var foodType: Int = 0
    
    var prevAnnotation: MKPointAnnotation?
    
    var textTimer: NSTimer?
    var mealTypeTextField: UITextField?
    var mealPriceTextField: UITextField?
    
    var mealPicker = UIPickerView()
    var mealTypes: [CuretoMealType] = [.Breakfast, .Lunch, .Teatime, .Dinner, .Supper]
    
    @IBOutlet weak var publishTableView: UITableView!
    @IBOutlet weak var draftButton: UIButton!
    @IBOutlet weak var publishButton: UIButton!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        separatorHeight.constant = 0.5
        
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        publishTableView.registerNib(UINib(nibName: "Publish-C1", bundle: nil), forCellReuseIdentifier: "PublishCell")
        publishTableView.registerNib(UINib(nibName: "Publish-C2", bundle: nil), forCellReuseIdentifier: "PublishCell2")
        publishTableView.registerNib(UINib(nibName: "Publish-C3", bundle: nil), forCellReuseIdentifier: "PublishCell3")
        publishTableView.registerNib(UINib(nibName: "Publish-C4", bundle: nil), forCellReuseIdentifier: "PublishCell4")
        publishTableView.registerNib(UINib(nibName: "MapViewTableViewCell", bundle: nil), forCellReuseIdentifier: "PublishCellMap")
        publishTableView.registerNib(UINib(nibName: "PublishHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "PublishHeaderCell")
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(publishContentChanged(_:)), name: "PublishContentChanged", object: nil)
        
        if editMode {
            if published {
                draftButton.enabled = false
                draftButton.hidden = true
            }
        }
        
        publishButton.enabled = false
        publishButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
        
        mealPicker.delegate = self
        mealPicker.dataSource = self
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().postNotificationName("PublishContentChanged", object: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "publish_to_add_location_segue" {
            let destination = segue.destinationViewController as! AddLocationController
            destination.publishController = self
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func publishContentChanged(notification: NSNotification) {
        if let _ = location {
            if foodType == 0 {
                if mealType != nil && mealPriceString != "" {
                    publishButton.enabled = true
                    publishButton.setTitleColor(MainColor.globalTint, forState: .Normal)
                } else {
                    publishButton.enabled = false
                    publishButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
                }
            } else {
                if mealPriceString != "" {
                    publishButton.enabled = true
                    publishButton.setTitleColor(MainColor.globalTint, forState: .Normal)
                } else {
                    publishButton.enabled = false
                    publishButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
                }
            }
        } else {
            publishButton.enabled = false
            publishButton.setTitleColor(MainColor.disabledButtonTint, forState: .Normal)
        }
    }
    
    @IBAction func back(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func saveAsDraft(sender: AnyObject) {
        saveArticleAsDraft()
        dismissViewControllerAnimated(true, completion: {
            self.articleController?.navigationController?.popToRootViewControllerAnimated(true)
        })
    }
    
    @IBAction func publish(sender: AnyObject) {
        publishArticle()
        dismissViewControllerAnimated(true, completion: {
            self.articleController?.navigationController?.popToRootViewControllerAnimated(true)
        })
    }
    
    func publishArticle() {
        guard let token = CredentialStore.sharedInstance.accessToken, head = articleHead, restaurant = location, mealPrice = Float(mealPriceString) else {
            return
        }
                
        CuretoRequests.retrieveElseCreateRestaurant(token, restaurant: restaurant, onCompletion: { result, message in
            guard let serverRestaurant = result else {
                return
            }
            
            if self.editMode {
                // Publish draft or edit published article
                CuretoRequests.updateArticle(token, coverDidChange: self.coverDidChange, articleHead: head, originalSubContent: self.originalSubContent, subContent: self.subContent, restaurant: serverRestaurant, draft: false, foodType: self.foodType, mealType: self.mealType, mealPrice: mealPrice, onCompletion: { succeeded, message in
//                    print("Publish status: \(message)")
                    if succeeded {
                        NSNotificationCenter.defaultCenter().postNotificationName("ArticleTableViewReloadRequired", object: nil)
                    }
                })

            } else {
                // Publish as new article
                CuretoRequests.publishArticle(token, articleHead: head, subContent: self.subContent, restaurant: serverRestaurant, foodType: self.foodType, mealType: self.mealType, mealPrice: mealPrice, onCompletion: { succeeded, message in
//                    print("Publish status: \(message)")
                    if succeeded {
                        NSNotificationCenter.defaultCenter().postNotificationName("ArticleTableViewReloadRequired", object: nil)
                    }
                })
            }
        })
    }
    
    func saveArticleAsDraft() {
        guard let token = CredentialStore.sharedInstance.accessToken, head = articleHead else {
            return
        }
        
        var mealPrice: Float?
        if let mealPriceFloat = Float(mealPriceString) {
            mealPrice = mealPriceFloat
        }
        
        if self.editMode {
            // Update draft
            if let restaurant = location {
                
                CuretoRequests.retrieveElseCreateRestaurant(token, restaurant: restaurant, onCompletion: { result, message in
                    guard let serverRestaurant = result else {
                        return
                    }
                    
                    CuretoRequests.updateArticle(token, coverDidChange: self.coverDidChange, articleHead: head, originalSubContent: self.originalSubContent, subContent: self.subContent, restaurant: serverRestaurant, draft: true, foodType: self.foodType, mealType: self.mealType, mealPrice: mealPrice, onCompletion: { succeeded, message in
//                        print("Publish status: \(message)")
                        if succeeded {
                            NSNotificationCenter.defaultCenter().postNotificationName("ArticleTableViewReloadRequired", object: nil)
                        }
                    })
                    
                })
                
            } else {
                CuretoRequests.updateArticle(token, coverDidChange: self.coverDidChange, articleHead: head, originalSubContent: self.originalSubContent, subContent: self.subContent, restaurant: nil, draft: true, foodType: self.foodType, mealType: self.mealType, mealPrice: mealPrice, onCompletion: { succeeded, message in
//                    print("Publish status: \(message)")
                    if succeeded {
                        NSNotificationCenter.defaultCenter().postNotificationName("ArticleTableViewReloadRequired", object: nil)
                    }
                })
            }
            
        } else {
            // Save as new draft
            if let restaurant = location {
                
                CuretoRequests.retrieveElseCreateRestaurant(token, restaurant: restaurant, onCompletion: { result, message in
                    guard let serverRestaurant = result else {
                        return
                    }
                    
                    CuretoRequests.saveAsDraft(token, articleHead: head, subContent: self.subContent, restaurant: serverRestaurant, foodType: self.foodType, mealType: self.mealType, mealPrice: mealPrice, onCompletion: { succeeded, message in
//                        print("Saving as draft status: \(message)")
                        if succeeded {
                            NSNotificationCenter.defaultCenter().postNotificationName("ArticleTableViewReloadRequired", object: nil)
                        }
                    })
                })
                
            } else {
                CuretoRequests.saveAsDraft(token, articleHead: head, subContent: subContent, restaurant: location, foodType: self.foodType, mealType: self.mealType, mealPrice: mealPrice, onCompletion: { succeeded, message in
//                    print("Saving as draft status: \(message)")
                    if succeeded {
                        NSNotificationCenter.defaultCenter().postNotificationName("ArticleTableViewReloadRequired", object: nil)
                    }
                })
            }
        }
    }
    
}

// MARK: - Handling table view

extension PublishController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 40
        }
        
        if indexPath.section == 0 {
            if indexPath.row == 2 {
                return 150
            }
        }
        return 50
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if let _ = location {
                return 3
            } else {
                return 2
            }
        } else {
            if foodType == 0 {
                return 4
            } else {
                return 3
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cellToShow: UITableViewCell!
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("PublishHeaderCell", forIndexPath: indexPath)
            cellToShow = cell
        }
        
        if indexPath.section == 0 {
            if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCellWithIdentifier("PublishCell", forIndexPath: indexPath) as! PublishC1
                
                if let restaurant = location {
                    if let name = restaurant.name {
                        cell.locationLabel.textColor = MainColor.publishTint
                        cell.locationLabel.text = name
                    }
                } else {
                    cell.locationLabel.textColor = MainColor.placeholderTint
                    cell.locationLabel.text = "Add Location"
                }
                cell.addLocationButton.addTarget(self, action: #selector(addLocation(_:)), forControlEvents: .TouchUpInside)
                cellToShow = cell
            } else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCellWithIdentifier("PublishCellMap", forIndexPath: indexPath) as! MapViewTableViewCell
                
                if let restaurant = location {
                    if let lat = restaurant.latitude, lng = restaurant.longitude {
                        let pinLocation = CLLocation(latitude: lat, longitude: lng)
                        let coordinateRegion = MKCoordinateRegionMakeWithDistance(pinLocation.coordinate, 250.0, 250.0)
                        cell.mapView.setRegion(coordinateRegion, animated: true)
                        
                        let pin = MKPointAnnotation()
                        pin.coordinate = pinLocation.coordinate
                        if let prevAnno = prevAnnotation {
                            cell.mapView.removeAnnotation(prevAnno)
                        }
                        cell.mapView.addAnnotation(pin)
                        prevAnnotation = pin
                    }
                }
                cellToShow = cell
            }
        } else {
            if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCellWithIdentifier("PublishCell4", forIndexPath: indexPath) as! PublishC4
                cell.foodTypeOptions.selectedSegmentIndex = foodType
                cell.foodTypeOptions.addTarget(self, action: #selector(foodTypeChanged(_:)), forControlEvents: .ValueChanged)
                cellToShow = cell
                
            } else if indexPath.row == 2 {
                if foodType == 0 {
                    let cell = tableView.dequeueReusableCellWithIdentifier("PublishCell2", forIndexPath: indexPath) as! PublishC2
                    cell.separatorHeight.constant = 0.5
                    cell.mealTypeTextField.delegate = self
                    cell.mealTypeTextField.inputView = mealPicker
                    
                    if let type = mealType {
                        cell.mealTypeTextField.text = type.string()
                        mealPicker.selectRow(type.row(), inComponent: 0, animated: false)
                    }
                    
                    mealTypeTextField = cell.mealTypeTextField
                    cellToShow = cell
                } else {
                    let cell = tableView.dequeueReusableCellWithIdentifier("PublishCell3", forIndexPath: indexPath) as! PublishC3
                    cell.priceTextField.delegate = self
                    cell.priceTextField.text = mealPriceString
                    mealPriceTextField = cell.priceTextField
                    cellToShow = cell
                }
            } else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCellWithIdentifier("PublishCell3", forIndexPath: indexPath) as! PublishC3
                cell.priceTextField.delegate = self
                cell.priceTextField.text = mealPriceString
                mealPriceTextField = cell.priceTextField
                cellToShow = cell
            }
            
        }
        
        return cellToShow
    }
    
}

// MARK: - Handling picker view

extension PublishController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return mealTypes.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return mealTypes[row].string()
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        mealType = mealTypes[row]
        templateController?.selectedMealType = mealTypes[row]
        if let textField = mealTypeTextField {
            textField.text = mealTypes[row].string()
            NSNotificationCenter.defaultCenter().postNotificationName("PublishContentChanged", object: nil)
        }
    }
}

// MARK: - Handling text field

extension PublishController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if let typeTextField = mealTypeTextField {
            if textField == typeTextField {
                if mealType == nil {
                    mealType = mealTypes[0]
                    textField.text = mealTypes[0].string()
                    mealPicker.selectRow(0, inComponent: 0, animated: true)
                }
            }
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if let priceTextField = mealPriceTextField {
            if textField == priceTextField {
                textTimer?.invalidate()
                textTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(typingPaused(_:)), userInfo: textField, repeats: false)
            }
        }
        return true
    }
    
    func typingPaused(timer: NSTimer) {
        if let textField = timer.userInfo as? UITextField {
            if let text = textField.text {
                if text != "" {
                    mealPriceString = text
                    templateController?.selectedMealPrice = text
                } else {
                    mealPriceString = ""
                    templateController?.selectedMealPrice = ""
                }
            }
            NSNotificationCenter.defaultCenter().postNotificationName("PublishContentChanged", object: nil)
        }
    }
    
}

// MARK: - Custom methods

extension PublishController {
    
    func foodTypeChanged(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            mealType = nil
            templateController?.selectedMealType = nil
            mealTypeTextField?.text = ""
        }
        foodType = sender.selectedSegmentIndex
        templateController?.selectedFoodType = sender.selectedSegmentIndex
        publishTableView.reloadSections(CuretoGlobal.tableViewSecondSectionIndex, withRowAnimation: .Automatic)
        NSNotificationCenter.defaultCenter().postNotificationName("PublishContentChanged", object: nil)
    }
    
    func addLocation(sender: UIButton) {
        performSegueWithIdentifier("publish_to_add_location_segue", sender: self)
    }
    
}
