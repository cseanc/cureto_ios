//
//  ContentController.swift
//  Cureto
//
//  Created by Sean on 11/25/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class ContentController: UIViewController, UITextViewDelegate {
    
    var contentText = ""
    let keyboardHeight: CGFloat = 270
    
    var templateController: TemplateController?
    
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var bottomSpace: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        separatorHeight.constant = 0.5
    }
    
    override func viewWillAppear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: false)
        
        contentTextView.textAlignment = .Left
        contentTextView.textColor = MainColor.contentTint
        let paraStyle = NSMutableParagraphStyle()
        paraStyle.lineSpacing = CuretoGlobal.lineSpacing
        let attrs = [NSFontAttributeName: CuretoGlobal.contentFont,
            NSParagraphStyleAttributeName: paraStyle]
        
        if let template = templateController {
            contentText = template.contentText
        }
        
        contentTextView.typingAttributes = attrs
        contentTextView.attributedText = NSMutableAttributedString(string: contentText, attributes: attrs)
        contentTextView.becomeFirstResponder()
    }
    
//    override func viewWillDisappear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().postNotificationName("StatusBarRequirementChanged", object: true)
//    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        bottomSpace.constant = keyboardHeight
    }
    
    @IBAction func done(sender: AnyObject) {
        if let template = templateController {
            template.contentText = contentTextView.text
            template.reloadTitleAndContent()
            dismissViewControllerAnimated(true, completion: nil)
        }
    }

    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}
