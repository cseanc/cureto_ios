//
//  ExploreAreaController.swift
//  Cureto
//
//  Created by Sean Choo on 7/24/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import Haneke

class ExploreAreaController: UITableViewController {
    
    // For navigation transition
    var navigationBarHeight: CGFloat = 64
    var initialFrame = CGRectZero
    var toImageView: UIImageView?
    
    var selectedArticle: Article?

    var selectedArea: AreaWithPicture?
    
    var articleGroups = [ArticleGroupWithHeader]()
    
    var pinMode: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // For navigation transition
        var statusBarHeight = UIApplication.sharedApplication().statusBarFrame.size.height
        if statusBarHeight == 0 {
            statusBarHeight = 20
        }
        if let navBarHeight = navigationController?.navigationBar.frame.size.height {
            navigationBarHeight = statusBarHeight + navBarHeight
        }
        
        navigationItem.title = selectedArea?.name
        
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 20, 0)
        
        reloadArticles()
    }
    
    override func viewWillAppear(animated: Bool) {
        // For navigation transition
        navigationController?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = ""
        navigationItem.backBarButtonItem = backBarButtonItem
        
        if segue.identifier == "explore_area_to_article_segue" {
            let destination = segue.destinationViewController as! ArticleController
            destination.selectedArticle = selectedArticle
            destination.coverImage = toImageView?.image
            destination.presenting = true
        }
    }
    
    func reloadArticles() {
        if pinMode {
            guard let token = CredentialStore.sharedInstance.accessToken, area = selectedArea?.name else {
                return
            }
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            CuretoRequests.getUserPinnedArticles(token, area: area, onCompletion: { result, message in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.articleGroups = result
                self.tableView.reloadData()
            })
            
        } else {
            guard let area = selectedArea?.name else {
                return
            }
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            CuretoRequests.getExploreArticles(area, onCompletion: { result, message in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.articleGroups = result
                self.tableView.reloadData()
            })
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 315
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleGroups.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ArticleGroupCell", forIndexPath: indexPath) as! ExploreAreaTableViewCell
        
        let articleGroup = articleGroups[indexPath.row]
        cell.headerLabel.text = articleGroup.header
        
        cell.articlesCollectionView.delegate = self
        cell.articlesCollectionView.dataSource = self
        cell.articlesCollectionView.tag = indexPath.row
        cell.articlesCollectionView.registerNib(UINib(nibName: "Cover-G1", bundle: nil), forCellWithReuseIdentifier: "ArticleCell")
        cell.articlesCollectionView.reloadData()
        
        return cell
    }
}


// MARK: - Handling UICollectionView

extension ExploreAreaController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let articleGroup = articleGroups[collectionView.tag]
        return articleGroup.articles.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ArticleCell", forIndexPath: indexPath) as! CoverG1
        cell.contentView.layer.borderWidth = 0.5
        cell.contentView.layer.borderColor = MainColor.separatorTint.CGColor
        cell.separatorHeight.constant = 0.5
        
        let article = articleGroups[collectionView.tag].articles[indexPath.item]
        cell.authorLabel.text = article.author?.displayName
        cell.titleLabel.text = article.title
        cell.locationLabel.text = article.restaurant?.name
        
        cell.imageView.image = UIImage(named: CuretoGlobal.placeholderImageName)
        if let urlString = article.coverPhotoThumbUrl {
            if let url = NSURL(string: urlString) {
                cell.imageView.hnk_setImageFromURL(url)
            }
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let article = articleGroups[collectionView.tag].articles[indexPath.item]
        
        // For navigation transitioning
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CoverG1
        initialFrame = cell.convertRect(cell.imageView.frame, toView: nil)
        
        if let width = article.coverPhotoWidth, height = article.coverPhotoHeight {
            let heightRatio = height / width
            toImageView = UIImageView(frame: CGRectMake(0, navigationBarHeight, MainScreen.width, MainScreen.width * heightRatio))
            toImageView?.image = cell.imageView.image
        }
        
        selectedArticle = article
        performSegueWithIdentifier("explore_area_to_article_segue", sender: self)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 182, height: 260)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
    }
}

extension ExploreAreaController: UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    // For navigation transition
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        guard let _ = toVC as? ArticleController else {
            return nil
        }
        
        if operation == UINavigationControllerOperation.Push {
            let pushAnimator = CuretoPushAnimator()
            pushAnimator.initialFrame = initialFrame
            pushAnimator.toImageView = toImageView
            pushAnimator.fromCollectionView = true
            return pushAnimator
        }
        
        return nil
    }
    
}