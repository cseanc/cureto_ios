//
//  FilterController.swift
//  Cureto
//
//  Created by Sean Choo on 3/14/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class FilterController: UITableViewController {
    
    var homeController: HomeController?
    
    var selectedFoodType: Int = 0
    var selectedPriceRange: Int = 4
    
    let foodTypes = ["Meal, Dessert, and Snack", "Meal Only", "Dessert Only", "Snack Only"]
    let priceRanges = ["Below HK$50", "Below HK$100", "Below HK$200", "HK$200 and Above", "Nevermind"]
    
    @IBAction func cancelTapped(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func doneTapped(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: {
            if let home = self.homeController {
                home.selectedFoodType = self.selectedFoodType
                home.selectedPriceRange = self.selectedPriceRange
//                home.setTitleView()
                home.reloadArticlesForStyle(0)
            }
        })
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return foodTypes.count
        } else {
            return priceRanges.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FilterCell", forIndexPath: indexPath) as! FilterTableViewCell
        cell.separatorHeight.constant = 0.5
        
        if indexPath.section == 0 {
            cell.filterLabel.text = foodTypes[indexPath.row]
            
            if indexPath.row == foodTypes.count - 1 {
                cell.separator.hidden = true
            } else {
                cell.separator.hidden = false
            }
            
            if indexPath.row == selectedFoodType {
                cell.dotImageView.hidden = false
            } else {
                cell.dotImageView.hidden = true
            }
            
        } else {
            cell.filterLabel.text = priceRanges[indexPath.row]
            
            if indexPath.row == priceRanges.count - 1 {
                cell.separator.hidden = true
            } else {
                cell.separator.hidden = false
            }
            
            if indexPath.row == selectedPriceRange {
                cell.dotImageView.hidden = false
            } else {
                cell.dotImageView.hidden = true
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = NSBundle.mainBundle().loadNibNamed("FilterSectionHeaderView", owner: self, options: nil)[0] as! FilterSectionHeaderView
        headerView.frame = CGRectMake(0, 0, MainScreen.width, 50)
        
        if section == 0 {
            headerView.headerLabel.text = "Food Type"
        } else {
            headerView.headerLabel.text = "Price Range"
        }
        
        return headerView
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            selectedFoodType = indexPath.row
            tableView.reloadSections(CuretoGlobal.tableViewFirstSectionIndex, withRowAnimation: .Automatic)
        } else if indexPath.section == 1 {
            selectedPriceRange = indexPath.row
            tableView.reloadSections(CuretoGlobal.tableViewSecondSectionIndex, withRowAnimation: .Automatic)
        }
    }
    
}
