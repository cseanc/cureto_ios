//
//  ExploreAreaTableViewCell.swift
//  Cureto
//
//  Created by Sean Choo on 7/24/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class ExploreAreaTableViewCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var articlesCollectionView: UICollectionView!
    
}
