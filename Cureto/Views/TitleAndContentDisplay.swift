//
//  TitleAndContentDisplay.swift
//  Cureto
//
//  Created by Sean on 12/15/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class TitleAndContentDisplay: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: KILabel!
    
}
