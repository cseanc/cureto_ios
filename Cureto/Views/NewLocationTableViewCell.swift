//
//  NewLocationTableViewCell.swift
//  Cureto
//
//  Created by Sean Choo on 4/28/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class NewLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var addButton: UIButton!
}
