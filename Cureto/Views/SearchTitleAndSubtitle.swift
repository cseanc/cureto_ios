//
//  SearchTitleAndSubtitle.swift
//  Cureto
//
//  Created by Sean on 11/30/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class SearchTitleAndSubtitle: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var touchFace: UIButton!
    
}
