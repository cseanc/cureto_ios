//
//  SubPhotoAndSubtitle.swift
//  Cureto
//
//  Created by Sean on 11/26/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class SubPhotoAndSubtitle: UITableViewCell {

    @IBOutlet weak var subPhotoView: UIImageView!
    @IBOutlet weak var subPhotoViewHeight: NSLayoutConstraint!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var subcontentEditButton: UIButton!
    
}
