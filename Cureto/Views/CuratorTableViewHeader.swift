//
//  CuratorTableViewHeader.swift
//  Cureto
//
//  Created by Sean on 12/15/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class CuratorTableViewHeader: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var statsLabel: UILabel!
    @IBOutlet weak var headerBlurView: UIVisualEffectView!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var mailButton: UIButton!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var viewOptions: UISegmentedControl!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var editProfileImageButton: UIButton!
    @IBOutlet weak var dotView: UIView!
}
