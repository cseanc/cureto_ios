//
//  TitleAndContent.swift
//  Cureto
//
//  Created by Sean on 11/25/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class TitleAndContent: UITableViewCell {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var contentButton: UIButton!
    
}
