//
//  CoverF1.swift
//  Cureto
//
//  Created by Sean Choo on 5/12/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CoverF1: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var shadeView: UIView!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleTopSpace: NSLayoutConstraint!
    @IBOutlet weak var touchFace: UIButton!
    
}
