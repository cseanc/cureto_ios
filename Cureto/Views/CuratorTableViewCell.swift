//
//  CuratorTableViewCell.swift
//  Cureto
//
//  Created by Sean Choo on 7/10/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuratorTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
}
