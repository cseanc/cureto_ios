//
//  ChillTableViewCell.swift
//  Cureto
//
//  Created by Sean Choo on 5/2/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class ChillTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var chillCollectionView: UICollectionView!
    
}
