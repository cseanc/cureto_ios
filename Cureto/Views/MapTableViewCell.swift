//
//  MapTableViewCell.swift
//  Cureto
//
//  Created by Sean on 12/15/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import MapKit

class MapTableViewCell: UITableViewCell {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantAddressLabel: UILabel!
    @IBOutlet weak var displayNameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var mealTypeLabel: UILabel!
    @IBOutlet weak var mealPriceLabel: UILabel!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var userButton: UIButton!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
}
