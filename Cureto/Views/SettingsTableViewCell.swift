//
//  SettingsTableViewCell.swift
//  Cureto
//
//  Created by Sean Choo on 3/5/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var disclosureIndicator: UIImageView!
    
}
