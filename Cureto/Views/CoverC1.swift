//
//  CoverC1.swift
//  Cureto
//
//  Created by Sean on 12/23/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class CoverC1: UICollectionViewCell {

    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var displayNameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
}
