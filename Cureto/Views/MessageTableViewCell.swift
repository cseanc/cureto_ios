//
//  MessageTableViewCell.swift
//  Cureto
//
//  Created by Sean Choo on 3/30/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
}
