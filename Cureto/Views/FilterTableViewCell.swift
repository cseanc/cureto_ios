//
//  FilterTableViewCell.swift
//  Cureto
//
//  Created by Sean Choo on 3/31/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dotImageView: UIImageView!
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
}
