//
//  CuretoProgressHUDView.swift
//  Cureto
//
//  Created by Sean Choo on 1/21/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuretoProgressHUDView: UIView {

    @IBOutlet weak var progressBarView: UIView!
    @IBOutlet weak var progressWidth: NSLayoutConstraint!
    @IBOutlet weak var progressLabel: UILabel!
    
}
