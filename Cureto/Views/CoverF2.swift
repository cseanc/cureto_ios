//
//  CoverF2.swift
//  Cureto
//
//  Created by Sean Choo on 7/9/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CoverF2: UITableViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
}
