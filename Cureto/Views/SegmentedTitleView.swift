//
//  SegmentedTitleView.swift
//  Cureto
//
//  Created by Sean Choo on 6/28/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class SegmentedTitleView: UIView {

    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    func titles(firstTitle: String, secondTitle: String) {
        firstButton.setTitle(firstTitle, forState: .Normal)
        secondButton.setTitle(secondTitle, forState: .Normal)
    }
    
    func viewOptionsChanged(index: Int) {
        if index == 1 {
            firstButton.backgroundColor = MainColor.darkGray
            firstButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            
            secondButton.backgroundColor = MainColor.lightGray
            secondButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
            
        } else {
            secondButton.backgroundColor = MainColor.darkGray
            secondButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            
            firstButton.backgroundColor = MainColor.lightGray
            firstButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        }
    }
}
