//
//  SubPhotoEdit.swift
//  Cureto
//
//  Created by Sean on 12/28/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class SubPhotoEdit: UITableViewCell {

    @IBOutlet weak var subPhotoView: UIImageView!
    @IBOutlet weak var changeButton: UIButton!
    
}
