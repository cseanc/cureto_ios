//
//  ChillTableViewSectionHeader.swift
//  Cureto
//
//  Created by Sean Choo on 5/2/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class ChillTableViewSectionHeader: UIView {

    @IBOutlet weak var headerLabel: UILabel!
}
