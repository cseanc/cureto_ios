//
//  CoverG1.swift
//  Cureto
//
//  Created by Sean on 12/21/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class CoverG1: UICollectionViewCell {
    
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var authorLabelTopSpace: NSLayoutConstraint!
    @IBOutlet weak var titleLabelTopSpace: NSLayoutConstraint!
    @IBOutlet weak var titleLabelBottomSpace: NSLayoutConstraint!
    
}
