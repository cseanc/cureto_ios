//
//  CuratorShowTableViewHeader.swift
//  Cureto
//
//  Created by Sean Choo on 3/23/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuratorShowTableViewHeader: UITableViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var displayNameLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var statsLabel: UILabel!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var viewOptions: UISegmentedControl!
    
}
