//
//  CuretoProgressMessageSpinView.swift
//  Cureto
//
//  Created by Sean Choo on 3/11/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuretoProgressMessageSpinView: UIView {

    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var messasgeLabel: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
}
