//
//  ArticleMenu.swift
//  Cureto
//
//  Created by Sean on 11/23/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class ArticleMenu: UIView {

    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var pinButton: UIButton!
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var starImageView: UIImageView!
    @IBOutlet weak var pinImageView: UIImageView!
    @IBOutlet weak var starCountLabel: UILabel!
    
}
