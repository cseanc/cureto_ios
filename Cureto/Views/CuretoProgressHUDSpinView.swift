//
//  CuretoProgressHUDSpinView.swift
//  Cureto
//
//  Created by Sean Choo on 1/22/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuretoProgressHUDSpinView: UIView {

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
}
