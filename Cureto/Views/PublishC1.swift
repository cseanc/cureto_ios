//
//  PublishC1.swift
//  Cureto
//
//  Created by Sean on 11/26/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class PublishC1: UITableViewCell {

    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var addLocationButton: UIButton!
    
}
