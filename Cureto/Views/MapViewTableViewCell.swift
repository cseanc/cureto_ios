//
//  MapViewTableViewCell.swift
//  Cureto
//
//  Created by Sean Choo on 1/21/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import MapKit

class MapViewTableViewCell: UITableViewCell {

    @IBOutlet weak var mapView: MKMapView!
}
