//
//  FilterSectionHeaderView.swift
//  Cureto
//
//  Created by Sean Choo on 4/1/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class FilterSectionHeaderView: UIView {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var bottomSpace: NSLayoutConstraint!
}
