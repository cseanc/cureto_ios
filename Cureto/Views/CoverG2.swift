//
//  CoverG2.swift
//  Cureto
//
//  Created by Sean on 12/26/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class CoverG2: UICollectionViewCell {

    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
}
