//
//  CoverPhoto.swift
//  Cureto
//
//  Created by Sean on 11/25/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class CoverPhoto: UITableViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var addCoverPhotoLabel: UILabel!
    @IBOutlet weak var addCoverPhotoButton: UIButton!
    @IBOutlet weak var changeCoverPhotoButton: UIButton!
    
}
