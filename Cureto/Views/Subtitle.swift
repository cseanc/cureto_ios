//
//  Subtitle.swift
//  Cureto
//
//  Created by Sean on 11/26/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class Subtitle: UITableViewCell {

    @IBOutlet weak var subtitleTextView: UITextView!
    
}