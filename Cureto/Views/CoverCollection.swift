//
//  CoverCollection.swift
//  Cureto
//
//  Created by Sean on 12/24/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class CoverCollection: UITableViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
}
