//
//  PinCollectionViewHeader.swift
//  Cureto
//
//  Created by Sean Choo on 3/21/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class PinCollectionViewHeader: UICollectionReusableView {

    @IBOutlet weak var titleLabel: UILabel!
    
}
