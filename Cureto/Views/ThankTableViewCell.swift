//
//  ThankTableViewCell.swift
//  Cureto
//
//  Created by Sean Choo on 4/8/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class ThankTableViewCell: UITableViewCell {

    @IBOutlet weak var peopleLabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!
    
}
