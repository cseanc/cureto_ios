//
//  CoverA1.swift
//  Cureto
//
//  Created by Sean on 11/20/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class CoverA1: UITableViewCell {
    
    @IBOutlet weak var topSeparatorHeight: NSLayoutConstraint!
    
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var mealTypeLabel: UILabel!
    @IBOutlet weak var mealPriceLabel: UILabel!
    
}
