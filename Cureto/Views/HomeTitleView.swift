//
//  HomeTitleView.swift
//  Cureto
//
//  Created by Sean Choo on 3/14/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class HomeTitleView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var filterLabel: UILabel!
}
