//
//  CoverPhotoDisplay.swift
//  Cureto
//
//  Created by Sean on 12/15/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class CoverPhotoDisplay: UITableViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var coverPhotoHeight: NSLayoutConstraint!
    
}
