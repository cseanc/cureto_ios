
//
//  RestaurantInfoTableViewCell.swift
//  Cureto
//
//  Created by Sean Choo on 1/22/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import MapKit

class RestaurantInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var webButton: UIButton!
    @IBOutlet weak var foursquareButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    
}
