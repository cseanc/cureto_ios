//
//  CuretoLocationHelper.swift
//  Cureto
//
//  Created by Sean Choo on 1/21/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuretoLocationHelper {
    
    static func withinBoundary(boundary: CuretoBoundary, latitude: Double, longitude: Double) -> Bool {
        if latitude <= boundary.northest && latitude >= boundary.southest
            && longitude >= boundary.westest && longitude <= boundary.eastest {
                return true
        }
        return false
    }
}
