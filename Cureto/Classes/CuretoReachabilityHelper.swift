//
//  CuretoReachabilityHelper.swift
//  Cureto
//
//  Created by Sean Choo on 3/16/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuretoReachabilityHelper: NSObject {
    
    static let sharedInstance = CuretoReachabilityHelper()
    
    let kNOTREACHABLE = "NotReachable"
    let kREACHABLEWITHWIFI = "ReachableWithWIFI"
    let kREACHABLEWITHWWAN = "ReachableWithWWAN"
    
    var reachability: Reachability?
    
    func addReachabilityObserver() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(reachabilityChanged(_:)), name: kReachabilityChangedNotification, object: nil)
    }
    
    func startObserving() {
        reachability = Reachability.reachabilityForInternetConnection()
        reachability?.startNotifier()
        
        if let reach = reachability {
            statusChangedWithReachability(reach)
        }
    }
    
    func reachabilityChanged(notification: NSNotification) {
        if let reach = notification.object as? Reachability {
            statusChangedWithReachability(reach)
        }
    }
    
    func statusChangedWithReachability(reach: Reachability) {
        let networkStatus = reach.currentReachabilityStatus()
        switch networkStatus.rawValue {
        case ReachableViaWiFi.rawValue:
            NSNotificationCenter.defaultCenter().postNotificationName("NetworkStatusChanged", object: true)
            CuretoBannerAlert.showNetworkBanner(true)
//            print(kREACHABLEWITHWIFI)
        case ReachableViaWWAN.rawValue:
            NSNotificationCenter.defaultCenter().postNotificationName("NetworkStatusChanged", object: true)
            CuretoBannerAlert.showNetworkBanner(true)
//            print(kREACHABLEWITHWWAN)
        default:
            NSNotificationCenter.defaultCenter().postNotificationName("NetworkStatusChanged", object: false)
            CuretoBannerAlert.showNetworkBanner(false)
//            print(kNOTREACHABLE)
        }
    }
    
    func isConnectedToNetwork() -> Bool {
        guard let reach = reachability else {
            return false
        }
        let status = reach.currentReachabilityStatus()
        
        if status.rawValue == NotReachable.rawValue {
            return false
        } else {
            return true
        }
    }
    
}
