//
//  CuretoRequestsHelper.swift
//  Cureto
//
//  Created by Sean on 12/23/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CuretoRequestsHelper {
    
    internal static let dateFormatter = NSDateFormatter()
    
    static func parseUserWithJSON(json: JSON) -> User? {
        if let username = json["username"].string, displayName = json["display_name"].string {
            let user = User()
            user.username = username
            user.displayName = displayName
            
            if let profileImageUrl = json["cover_photo"]["cover_photo"]["retina"]["url"].string {
                user.profileImageUrl = profileImageUrl
            }
            
            if let bio = json["bio"].string {
                user.bio = bio
            }
            
            if let articlesCount = json["public_articles_count"].int {
                user.articlesCount = articlesCount
            }
            
            return user
        }
        return nil
    }
    
    static func parseUsersWithJSON(json: JSON) -> [User] {
        var users = [User]()
        
        for (_, user): (String, JSON) in json["users"] {
            if let username = user["username"].string, displayName = user["display_name"].string {
                let newUser = User()
                newUser.username = username
                newUser.displayName = displayName
                
                if let profileImageUrl = user["cover_photo"]["cover_photo"]["retina"]["url"].string {
                    newUser.profileImageUrl = profileImageUrl
                }
                
                if let bio = user["bio"].string {
                    newUser.bio = bio
                }
                
                if let articlesCount = user["public_articles_count"].int {
                    newUser.articlesCount = articlesCount
                }
                
                users.append(newUser)
            }
        }
        
        return users
    }
    
    static func parsePlaceWithJSON(json: JSON) -> Restaurant? {
        let venue = json["response"]["venue"]
        
        if let identifier = venue["id"].string, name = venue["name"].string, latitude = venue["location"]["lat"].double, longitude = venue["location"]["lng"].double {
            
            let restaurant = Restaurant(identifier: identifier, name: name, latitude: latitude, longitude: longitude)
            
            if let address = venue["location"]["address"].string {
                restaurant.address = address
            }
            
            if let city = venue["location"]["city"].string {
                restaurant.city = city
            }
            
            if let country = venue["location"]["country"].string {
                restaurant.country = country
            }
            
            if let categoryId = venue["categories"][0]["id"].string, categoryName = venue["categories"][0]["name"].string {
                restaurant.categoryId = categoryId
                restaurant.categoryName = categoryName
            }
            
            return restaurant
        }
        
//        if let identifier = venue["id"].string, name = venue["name"].string,
//            latitude = venue["location"]["lat"].double, longitude = venue["location"]["lng"].double,
//            address = venue["location"]["address"].string,
//            categoryId = venue["categories"][0]["id"].string,
//            categoryName = venue["categories"][0]["name"].string {
//                let restaurant = Restaurant(identifier: identifier, name: name, latitude: latitude, longitude: longitude, address: address, categoryId: categoryId, categoryName: categoryName)
//                
//                if let city = venue["location"]["city"].string {
//                    restaurant.city = city
//                }
//                
//                return restaurant
//        }
        
        return nil
    }
    
    static func parseMessagesWithJSON(json: JSON) -> [Message] {
        var messages = [Message]()
        
        for (_, message): (String, JSON) in json["messages"] {
            if let id = message["id"].int, senderID = message["sender_id"].int, recipientID = message["recipient_id"].int, content = message["content"].string, read = message["read"].bool, createdAt = message["created_at"].string {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sssZ"
                if let date = dateFormatter.dateFromString(createdAt) {
                    let newMessage = Message(id: id, senderId: senderID, recipientId: recipientID, content: content, read: read, createdAt: date)
                    messages.append(newMessage)
                }
            }
        }
        
        return messages
    }
    
    static func parseFeaturedGroupsWithJSON(json: JSON) -> [FeaturedGroup] {
        var groups = [FeaturedGroup]()
        
        for (_, group): (String, JSON) in json["featured_groups"] {
            guard let id = group["id"].int, type = group["featured_type"].int, title = group["title"].string, desc = group["description"].string, coverUrl = group["cover_photo_url"].string, opacity = group["opacity"].int else {
                continue
            }
            
            let newGroup = FeaturedGroup(id: id, featuredType: type, title: title, desc: desc, coverPhotoUrl: coverUrl, opacity: opacity)
            groups.append(newGroup)
        }
        
        return groups
    }
    
    static func parseFeaturedElementsWithJSON(json: JSON) -> [FeaturedElement] {
        var elements = [FeaturedElement]()
        
        for (_, element): (String, JSON) in json["featured_elements"] {
            guard let id = element["id"].int, targetId = element["target_id"].int else {
                continue
            }
            
            let newElement = FeaturedElement(id: id, targetId: targetId)
            elements.append(newElement)
        }
        
        return elements
    }
    
    static func parseRestaurantsWithJSON(json: JSON) -> [Restaurant] {
        var places = [Restaurant]()
        for (_, venue): (String, JSON) in json["response"]["venues"] {
            if let identifier = venue["id"].string, name = venue["name"].string, latitude = venue["location"]["lat"].double, longitude = venue["location"]["lng"].double {
                
                let restaurant = Restaurant(identifier: identifier, name: name, latitude: latitude, longitude: longitude)
                
                if let address = venue["location"]["address"].string {
                    restaurant.address = address
                }
                
                if let city = venue["location"]["city"].string {
                    restaurant.city = city
                }

                if let country = venue["location"]["country"].string {
                    restaurant.country = country
                }
                
                if let categoryId = venue["categories"][0]["id"].string, categoryName = venue["categories"][0]["name"].string {
                    restaurant.categoryId = categoryId
                    restaurant.categoryName = categoryName
                }
                
                places.append(restaurant)
                
            }
//            if let identifier = venue["id"].string, name = venue["name"].string,
//                latitude = venue["location"]["lat"].double, longitude = venue["location"]["lng"].double,
//                address = venue["location"]["address"].string,
//                categoryId = venue["categories"][0]["id"].string,
//                categoryName = venue["categories"][0]["name"].string {
//                    let restaurant = Restaurant(identifier: identifier, name: name, latitude: latitude, longitude: longitude, address: address, categoryId: categoryId, categoryName: categoryName)
//                    
//                    if let city = venue["location"]["city"].string {
//                        restaurant.city = city
//                    }
//                    
//                    if let country = venue["location"]["country"].string {
//                        restaurant.country = country
//                    }
//                    
//                    places.append(restaurant)
//            }
        }
        return places
    }
    
    static func parseServerRestaurantWithJSON(json: JSON) -> Restaurant? {
        let restaurantJson = json["restaurant"]
        
        guard let id = restaurantJson["id"].int, identifier = restaurantJson["identifier"].string, name = restaurantJson["name"].string, latitude = restaurantJson["latitude"].string, longitude = restaurantJson["longitude"].string else {
            return nil
        }
        
        let lat = Double(latitude)
        let lng = Double(longitude)

        let restaurant = Restaurant(identifier: identifier, name: name, latitude: lat!, longitude: lng!)
        restaurant.id = id
        
        if let address = restaurantJson["street"].string {
            restaurant.address = address
        }

        if let city = restaurantJson["city"].string {
            restaurant.city = city
        }

        if let country = restaurantJson["country"].string {
            restaurant.country = country
        }
        
        if let categoryId = restaurantJson["category_id"].string {
            restaurant.categoryId = categoryId
        }
        
        if let categoryName = restaurantJson["category_name"].string {
            restaurant.categoryName = categoryName
        }
        
        return restaurant
        
//        guard let id = restaurantJson["id"].int, identifier = restaurantJson["identifier"].string, name = restaurantJson["name"].string, address = restaurantJson["street"].string, latitude = restaurantJson["latitude"].string, longitude = restaurantJson["longitude"].string, categoryId = restaurantJson["category_id"].string, categoryName = restaurantJson["category_name"].string else {
//            return nil
//        }
//        
//        let lat = Double(latitude)
//        let lng = Double(longitude)
//        
//        let restaurant = Restaurant(identifier: identifier, name: name, latitude: lat!, longitude: lng!, address: address, categoryId: categoryId, categoryName: categoryName)
//        restaurant.id = id
//        
//        if let city = restaurantJson["city"].string {
//            restaurant.city = city
//        }
//        
//        if let country = restaurantJson["country"].string {
//            restaurant.country = country
//        }
//        
//        return restaurant
    }
    
    static func parseArticleWithJSON(json: JSON, root: Bool = false) -> Article? {
        var article = json["article"]
        
        if root {
            article = json
        }
        
        guard let id = article["id"].int, username = article["user"]["username"].string, displayName = article["user"]["display_name"].string else {
            return nil
        }
        
        let user = User()
        user.username = username
        user.displayName = displayName
        user.bio = article["user"]["bio"].string
        user.profileImageUrl = article["user"]["cover_photo"]["cover_photo"]["retina"]["url"].string
        user.articlesCount = article["user"]["public_articles_count"].int
        
        let draft = Article(id: id, author: user)
        draft.title = article["title"].string
        draft.content = article["content"].string
        draft.originalCoverPhotoUrl = article["cover_photo"]["cover_photo"]["url"].string
        draft.coverPhotoUrl = article["cover_photo"]["cover_photo"]["retina"]["url"].string
        draft.coverPhotoThumbUrl = article["cover_photo"]["cover_photo"]["thumb"]["url"].string
        if let coverPhotoWidth = article["cover_photo_width"].float {
            draft.coverPhotoWidth = CGFloat(coverPhotoWidth)
        }
        if let coverPhotoHeight = article["cover_photo_height"].float {
            draft.coverPhotoHeight = CGFloat(coverPhotoHeight)
        }
        if let foodType = article["food_type"].int {
            draft.foodType = foodType
        }
        if let mealType = article["meal_type"].string {
            if mealType != "" {
                draft.mealType = mealType
            }
        }
        if let isDraft = article["draft"].bool {
            draft.draft = isDraft
        }
        if let starsCount = article["stars_count"].int {
            draft.starsCount = starsCount
        }
        draft.mealPrice = article["meal_price"].float
        
        let res = article["restaurant"]
        if let resID = res["id"].int, resIdentifier = res["identifier"].string, resName = res["name"].string, latitude = res["latitude"].string, longitude = res["longitude"].string {
            
            let lat = Double(latitude)
            let lng = Double(longitude)
            
            let restaurant = Restaurant(identifier: resIdentifier, name: resName, latitude: lat!, longitude: lng!)
            restaurant.id = resID
            
            if let address = res["street"].string {
                restaurant.address = address
            }
            
            if let city = res["city"].string {
                restaurant.city = city
            }
            if let country = res["country"].string {
                restaurant.country = country
            }
            
            if let catID = res["category_id"].string {
                restaurant.categoryId = catID
            }
            
            if let catName = res["category_name"].string {
                restaurant.categoryName = catName
            }
            
            draft.restaurant = restaurant
        }
        
        for (_, subcontent): (String, JSON) in article["subcontents"] {
            guard let subcontentID = subcontent["id"].int, subtitle = subcontent["subtitle"].string, originalUrl = subcontent["subphoto"]["subphoto"]["url"].string, subphotoUrl = subcontent["subphoto"]["subphoto"]["retina"]["url"].string, subphotoThumbUrl = subcontent["subphoto"]["subphoto"]["thumb"]["url"].string, subphotoWidth = subcontent["subphoto_width"].float, subphotoHeight = subcontent["subphoto_height"].float, articleID = subcontent["article_id"].int else {
                continue
            }
            
            let newSubcontent = Subcontent(id: subcontentID, subphotoUrl: subphotoUrl, subtitle: subtitle)
            newSubcontent.originalSubphotoUrl = originalUrl
            newSubcontent.subphotoThumbUrl = subphotoThumbUrl
            newSubcontent.subphotoWidth = CGFloat(subphotoWidth)
            newSubcontent.subphotoHeight = CGFloat(subphotoHeight)
            newSubcontent.articleId = articleID
            
            draft.subcontent.append(newSubcontent)
        }
        
        return draft
    }
    
    static func parseDraftsWithJSON(json: JSON) -> [Article] {
        var drafts = [Article]()
        
        for (_, article): (String, JSON) in json["articles"] {
            guard let id = article["id"].int, username = article["user"]["username"].string, displayName = article["user"]["display_name"].string else {
                continue
            }
            
            let user = User()
            user.username = username
            user.displayName = displayName
            user.bio = article["user"]["bio"].string
            user.profileImageUrl = article["user"]["cover_photo"]["cover_photo"]["retina"]["url"].string
            
            let draft = Article(id: id, author: user)
            draft.title = article["title"].string
            draft.content = article["content"].string
            draft.originalCoverPhotoUrl = article["cover_photo"]["cover_photo"]["url"].string
            draft.coverPhotoUrl = article["cover_photo"]["cover_photo"]["retina"]["url"].string
            draft.coverPhotoThumbUrl = article["cover_photo"]["cover_photo"]["thumb"]["url"].string
            if let coverPhotoWidth = article["cover_photo_width"].float {
                draft.coverPhotoWidth = CGFloat(coverPhotoWidth)
            }
            if let coverPhotoHeight = article["cover_photo_height"].float {
                draft.coverPhotoHeight = CGFloat(coverPhotoHeight)
            }
            if let foodType = article["food_type"].int {
                draft.foodType = foodType
            }
            if let mealType = article["meal_type"].string {
                if mealType != "" {
                    draft.mealType = mealType
                }
            }
            if let isDraft = article["draft"].bool {
                draft.draft = isDraft
            }
            draft.mealPrice = article["meal_price"].float
            
            let res = article["restaurant"]
            if let resID = res["id"].int, resIdentifier = res["identifier"].string, resName = res["name"].string, latitude = res["latitude"].string, longitude = res["longitude"].string {
                
                let lat = Double(latitude)
                let lng = Double(longitude)
                
                let restaurant = Restaurant(identifier: resIdentifier, name: resName, latitude: lat!, longitude: lng!)
                restaurant.id = resID
                
                if let address = res["street"].string {
                    restaurant.address = address
                }
                if let city = res["city"].string {
                    restaurant.city = city
                }
                if let country = res["country"].string {
                    restaurant.country = country
                }
                if let catID = res["cateogory_id"].string {
                    restaurant.categoryId = catID
                }
                if let catName = res["category_name"].string {
                    restaurant.categoryName = catName
                }
                
                draft.restaurant = restaurant
            }
            
            for (_, subcontent): (String, JSON) in article["subcontents"] {
                guard let subcontentID = subcontent["id"].int, subtitle = subcontent["subtitle"].string, originalUrl = subcontent["subphoto"]["subphoto"]["url"].string, subphotoUrl = subcontent["subphoto"]["subphoto"]["retina"]["url"].string, subphotoThumbUrl = subcontent["subphoto"]["subphoto"]["thumb"]["url"].string, subphotoWidth = subcontent["subphoto_width"].float, subphotoHeight = subcontent["subphoto_height"].float, articleID = subcontent["article_id"].int else {
                    continue
                }
                
                let newSubcontent = Subcontent(id: subcontentID, subphotoUrl: subphotoUrl, subtitle: subtitle)
                newSubcontent.originalSubphotoUrl = originalUrl
                newSubcontent.subphotoThumbUrl = subphotoThumbUrl
                newSubcontent.subphotoWidth = CGFloat(subphotoWidth)
                newSubcontent.subphotoHeight = CGFloat(subphotoHeight)
                newSubcontent.articleId = articleID
                
                draft.subcontent.append(newSubcontent)
            }
            
            
            drafts.append(draft)
        }
        
        return drafts
    }
    
    static func parseArticleGroupsWithJSON(json: JSON, rootName: String = "articles") -> [ArticleGroupWithHeader] {
        var articleGroups = [ArticleGroupWithHeader]()
        
        let articlesJson = json[rootName]
        
        guard let fiftyCount = articlesJson[1].int, hundredCount = articlesJson[2].int, twoHundredCount = articlesJson[3].int, threeHundredCount = articlesJson[4].int, dessertCount = articlesJson[5].int, snackCount = articlesJson[6].int else {
            return articleGroups
        }
        
        var fiftyArticles = [Article]()
        var hundredArticles = [Article]()
        var twoHundredArticles = [Article]()
        var threeHundredArticles = [Article]()
        var dessertArticles = [Article]()
        var snackArticles = [Article]()
        
        var parsedArticles = [Article]()
        
        for (_, article): (String, JSON) in articlesJson[0] {
            if let parsedArticle = parseArticleWithJSON(article, root: true) {
                parsedArticles.append(parsedArticle)
            }
        }
        
        for _ in 0..<fiftyCount {
            let currentArticle = parsedArticles[0]
            fiftyArticles.append(currentArticle)
            parsedArticles.removeFirst()
        }
        
        for _ in 0..<hundredCount {
            let currentArticle = parsedArticles[0]
            hundredArticles.append(currentArticle)
            parsedArticles.removeFirst()
        }
        
        for _ in 0..<twoHundredCount {
            let currentArticle = parsedArticles[0]
            twoHundredArticles.append(currentArticle)
            parsedArticles.removeFirst()
        }
        
        for _ in 0..<threeHundredCount {
            let currentArticle = parsedArticles[0]
            threeHundredArticles.append(currentArticle)
            parsedArticles.removeFirst()
        }
        
        for _ in 0..<dessertCount {
            let currentArticle = parsedArticles[0]
            dessertArticles.append(currentArticle)
            parsedArticles.removeFirst()
        }
        
        for _ in 0..<snackCount {
            let currentArticle = parsedArticles[0]
            snackArticles.append(currentArticle)
            parsedArticles.removeFirst()
        }
        
        if fiftyArticles.count > 0 {
            let fiftyGroup = ArticleGroupWithHeader(header: "HK$0-50", articles: fiftyArticles)
            articleGroups.append(fiftyGroup)
        }
        if hundredArticles.count > 0 {
            let hundredGroup = ArticleGroupWithHeader(header: "HK$50-100", articles: hundredArticles)
            articleGroups.append(hundredGroup)
        }
        if twoHundredArticles.count > 0 {
            let twoHundredGroup = ArticleGroupWithHeader(header: "HK$100-200", articles: twoHundredArticles)
            articleGroups.append(twoHundredGroup)
        }
        if threeHundredArticles.count > 0 {
            let threeHundredGroup = ArticleGroupWithHeader(header: "HK$200+", articles: threeHundredArticles)
            articleGroups.append(threeHundredGroup)
        }
        if dessertArticles.count > 0 {
            let dessertGroup = ArticleGroupWithHeader(header: "Desserts", articles: dessertArticles)
            articleGroups.append(dessertGroup)
        }
        if snackArticles.count > 0 {
            let snackGroup = ArticleGroupWithHeader(header: "Snacks", articles: snackArticles)
            articleGroups.append(snackGroup)
        }
        
        return articleGroups
    }
    
    static func parseRestaurantAreasWithPicture(json: JSON, rootName: String = "restaurants") -> [GroupedAreaWithPicture] {
        var groupedAreas = [GroupedAreaWithPicture]()
        
        var areasWithPicture = [AreaWithPicture]()
        
        for (_, areaWithPicture): (String, JSON) in json[rootName] {
            if let area = areaWithPicture["area"].string, pictureUrl = areaWithPicture["photo"].string {
                let newAreaWithPicture = AreaWithPicture(name: area, pictureUrl: pictureUrl)
                areasWithPicture.append(newAreaWithPicture)
            }
        }
        
        let sortedAreasWithPicture = areasWithPicture.sort {
            $0.name < $1.name
        }
        
        if sortedAreasWithPicture.count == 1 {
            if let firstArea = sortedAreasWithPicture[0].name?.characters.first {
                var currentGroup = [AreaWithPicture]()
                currentGroup.append(sortedAreasWithPicture[0])
                let newGroupedArea = GroupedAreaWithPicture(title: "\(firstArea)")
                newGroupedArea.areas = currentGroup
                groupedAreas.append(newGroupedArea)
            }
            
        } else if sortedAreasWithPicture.count > 1 {
            if let firstArea = sortedAreasWithPicture[0].name {
                var currentGroup = [AreaWithPicture]()
                var previousArea: String = firstArea
                currentGroup.append(sortedAreasWithPicture[0])
                
                var i = 1
                while i < sortedAreasWithPicture.count {
                    if let name = sortedAreasWithPicture[i].name {
                        
                        if name != "" {
                            if let currentAreaFirst = name.characters.first, previousAreaFirst = previousArea.characters.first {
                                if currentAreaFirst == previousAreaFirst {
                                    currentGroup.append(sortedAreasWithPicture[i])
                                    previousArea = name
                                    i += 1
                                    
                                } else {
                                    if let previousAreaFirst = previousArea.characters.first {
                                        let newGroupedAreas = GroupedAreaWithPicture(title: "\(previousAreaFirst)")
                                        newGroupedAreas.areas = currentGroup
                                        groupedAreas.append(newGroupedAreas)
                                        currentGroup = []
                                        currentGroup.append(sortedAreasWithPicture[i])
                                        previousArea = name
                                        
                                        if i == sortedAreasWithPicture.count - 1 {
                                            if let lastAreaFirst = sortedAreasWithPicture[i].name?.characters.first {
                                                let lastGroupedAreas = GroupedAreaWithPicture(title: "\(lastAreaFirst)")
                                                lastGroupedAreas.areas = currentGroup
                                                groupedAreas.append(lastGroupedAreas)
                                            }
                                        }
                                        
                                        i += 1
                                    }
                                }
                            }
                            
                        } else {
                            i += 1
                        }
                        
                    } else {
                        i += 1
                    }
                }
            }
        }
        
        return groupedAreas
    }
    
    static func parsePinGroupsWithJSON(json: JSON) -> [PinGroup] {
        var pinGroups = [PinGroup]()
        
        for (area, articles): (String, JSON) in json {
            let pinGroup = PinGroup(area: area)
            pinGroup.articles = parseArticlesWithJSON(articles, root: false)
            pinGroups.append(pinGroup)
        }
        
        let sortedPinGroups = pinGroups.sort {
            $0.area < $1.area
        }
        
        return sortedPinGroups
    }
    
    static func parseArticlesWithJSON(json: JSON, key: String = "articles", root: Bool = true) -> [Article] {
        var articles = [Article]()
        
        var targetJson = json
        
        if root {
            targetJson = json[key]
        }
        
        for (_, article): (String, JSON) in targetJson {
            guard let id = article["id"].int, title = article["title"].string, content = article["content"].string, originalUrl = article["cover_photo"]["cover_photo"]["url"].string, coverPhotoUrl = article["cover_photo"]["cover_photo"]["retina"]["url"].string, coverPhotoThumbUrl = article["cover_photo"]["cover_photo"]["thumb"]["url"].string, coverPhotoWidth = article["cover_photo_width"].float, coverPhotoHeight = article["cover_photo_height"].float, foodType = article["food_type"].int, mealPrice = article["meal_price"].float, draft = article["draft"].bool, restaurantID = article["restaurant"]["id"].int, restaurantIdentifier = article["restaurant"]["identifier"].string, restaurantName = article["restaurant"]["name"].string, latitude = article["restaurant"]["latitude"].string, longitude = article["restaurant"]["longitude"].string, username = article["user"]["username"].string, displayName = article["user"]["display_name"].string else {
                return articles
            }
            
            let lat = Double(latitude)
            let lng = Double(longitude)

            let restaurant = Restaurant(identifier: restaurantIdentifier, name: restaurantName, latitude: lat!, longitude: lng!)
            restaurant.id = restaurantID
            
            if let address = article["restaurant"]["street"].string {
                restaurant.address = address
            }
            
            if let city = article["restaurant"]["city"].string {
                restaurant.city = city
            }
            
            if let country = article["restaurant"]["country"].string {
                restaurant.country = country
            }
            
            if let categoryId = article["restaurant"]["category_id"].string {
                restaurant.categoryId = categoryId
            }
            
            if let categoryName = article["restaurant"]["category_name"].string {
                restaurant.categoryName = categoryName
            }
            
            let user = User()
            user.username = username
            user.displayName = displayName
            
            if let bio = article["user"]["bio"].string {
                user.bio = bio
            }
            
            if let userCoverPhotoUrl = article["user"]["cover_photo"]["cover_photo"]["retina"]["url"].string {
                user.profileImageUrl = userCoverPhotoUrl
            }
            
            if let articlesCount = article["user"]["public_articles_count"].int {
                user.articlesCount = articlesCount
            }
            
            let newArticle = Article(id: id, title: title, content: content, coverPhotoUrl: coverPhotoUrl, foodType: foodType, mealPrice: mealPrice, restaurant: restaurant, author: user)
            newArticle.coverPhotoWidth = CGFloat(coverPhotoWidth)
            newArticle.coverPhotoHeight = CGFloat(coverPhotoHeight)
            newArticle.originalCoverPhotoUrl = originalUrl
            newArticle.coverPhotoThumbUrl = coverPhotoThumbUrl
            newArticle.draft = draft
            if let mealType = article["meal_type"].string {
                if mealType != "" {
                    newArticle.mealType = mealType
                }
            }
            
            if let starsCount = article["stars_count"].int {
                newArticle.starsCount = starsCount
            }
            
            for (_, subcontent): (String, JSON) in article["subcontents"] {
                guard let subcontentID = subcontent["id"].int, subtitle = subcontent["subtitle"].string, originalSubphotoUrl = subcontent["subphoto"]["subphoto"]["url"].string, subphotoUrl = subcontent["subphoto"]["subphoto"]["retina"]["url"].string, subphotoThumbUrl = subcontent["subphoto"]["subphoto"]["thumb"]["url"].string, subphotoWidth = subcontent["subphoto_width"].float, subphotoHeight = subcontent["subphoto_height"].float, articleID = subcontent["article_id"].int else {
                    continue
                }
                
                let newSubcontent = Subcontent(id: subcontentID, subphotoUrl: subphotoUrl, subtitle: subtitle)
                newSubcontent.originalSubphotoUrl = originalSubphotoUrl
                newSubcontent.subphotoThumbUrl = subphotoThumbUrl
                newSubcontent.subphotoWidth = CGFloat(subphotoWidth)
                newSubcontent.subphotoHeight = CGFloat(subphotoHeight)
                newSubcontent.articleId = articleID
                
                newArticle.subcontent.append(newSubcontent)
            }
            
            articles.append(newArticle)
        }
        
        return articles
    }
    
    static func parseRestaurantAreasWithJSON(json: JSON) -> [String] {
        var areas = [String]()
        
        for (_, area): (String, JSON) in json["restaurants"] {
            if let areaString = area.string {
                areas.append(areaString)
            }
        }
        
        return areas
    }
    
    static func getIdsFromPhotosWithSubtitle(subcontents: [PhotoWithSubtitle]) -> [Int] {
        var ids = [Int]()
        
        for sub in subcontents {
            if let id = sub.id {
                ids.append(id)
            } else {
                ids.append(0)
            }
        }
        return ids
    }
    
    static func getIdsFromSubcontents(subcontents: [Subcontent]) -> [Int] {
        var ids = [Int]()
        
        for sub in subcontents {
            if let id = sub.id {
                ids.append(id)
            } else {
                ids.append(0)
            }
        }
        return ids
    }
    
    static func photosWithSubtitleOfCorrectSequence(original: [Subcontent], target: [PhotoWithSubtitle]) -> [PhotoWithSubtitle] {
        var subcontents = [PhotoWithSubtitle]()
        
        let originalIds = getIdsFromSubcontents(original)
        
        for id in originalIds {
            if let sub = findPhotoWithSubtitleWithId(id, photosWithSubtitle: target) {
                subcontents.append(sub)
            }
        }
        
        return subcontents
    }
    
    static func findPhotoWithSubtitleWithId(id: Int, photosWithSubtitle: [PhotoWithSubtitle]) -> PhotoWithSubtitle? {
        for sub in photosWithSubtitle {
            guard let subId = sub.id else {
                continue
            }
            
            if subId == id {
                return sub
            }
        }
        return nil
    }
}
