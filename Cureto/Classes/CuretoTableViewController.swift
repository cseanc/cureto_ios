//
//  CuretoTableViewController.swift
//  Cureto
//
//  Created by Sean Choo on 3/16/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuretoTableViewController: UITableViewController, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    // For navigation transition
    var initialFrame: CGRect = CGRectZero
    var toImageView: UIImageView?
    var navigationBarHeight: CGFloat = 64
    
    var articles = [Article]()
    var nextArticles = [Article]()
    var page = 1
    var nextPage = 2
    var safeToLoadNextPage = true
    
    var selectedArticle: Article?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var statusBarHeight = UIApplication.sharedApplication().statusBarFrame.size.height
        if statusBarHeight == 0 {
            statusBarHeight = 20
        }
        
        if let navBarHeight = navigationController?.navigationBar.frame.size.height {
            navigationBarHeight = statusBarHeight + navBarHeight
        }
                        
    }
    
    // For navigation transition
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        guard let _ = toVC as? ArticleController else {
            return nil
        }
        
        if operation == UINavigationControllerOperation.Push {
            let pushAnimator = CuretoPushAnimator()
            pushAnimator.initialFrame = initialFrame
            pushAnimator.toImageView = toImageView
            return pushAnimator
        }
        
        return nil
        
    }
    
}
