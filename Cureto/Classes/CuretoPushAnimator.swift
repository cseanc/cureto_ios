//
//  CuretoPushAnimator.swift
//  Cureto
//
//  Created by Sean Choo on 5/24/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuretoPushAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let transitionDuration: NSTimeInterval = 0.3
    let tabBarHeight: CGFloat = 49
    
    var initialFrame = CGRectZero
    var finalFrame = CGRectZero
    
    var toImageView: UIImageView?
    
    var fromCollectionView: Bool = false
    var tabBarPresent: Bool = true
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return transitionDuration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        guard let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey), containerView = transitionContext.containerView(), toIV = toImageView else {
            return
        }
        
        finalFrame = toIV.frame
        
        let xScaleFactor = initialFrame.width / finalFrame.width
        let yScaleFactor = initialFrame.height / finalFrame.height
        
        let scaleTransform = CGAffineTransformMakeScale(xScaleFactor, yScaleFactor)
        
        toIV.transform = scaleTransform
        toIV.center = CGPoint(x: CGRectGetMidX(initialFrame), y: CGRectGetMidY(initialFrame))
        toIV.clipsToBounds = true
        
        let tmpIV = UIImageView(frame: toIV.frame)
        tmpIV.image = toIV.image
        tmpIV.clipsToBounds = true
        tmpIV.contentMode = .ScaleAspectFill
        
        containerView.addSubview(toVC.view)
        toVC.view.alpha = 0
        
        var tabBarView = UIView(frame: CGRectZero)
        if tabBarPresent {
            tabBarView = UIView(frame: CGRectMake(0, MainScreen.height - tabBarHeight, MainScreen.width, tabBarHeight))
            tabBarView.backgroundColor = MainColor.tabBarTint
            containerView.addSubview(tabBarView)
            tabBarView.alpha = 1
        }
        
        containerView.addSubview(tmpIV)
        
        UIView.animateWithDuration(transitionDuration, delay: 0, options: .CurveEaseInOut, animations: {
            if self.tabBarPresent {
                NSTimer.scheduledTimerWithTimeInterval(0.3, target: self, selector: #selector(CuretoPushAnimator.dissolveTabBar(_:)), userInfo: tabBarView, repeats: false)
                tabBarView.alpha = 0.89
            }
            toVC.view.alpha = 1
            toIV.transform = CGAffineTransformIdentity
            toIV.center = CGPoint(x: CGRectGetMidX(self.finalFrame), y: CGRectGetMidY(self.finalFrame))
            tmpIV.frame = toIV.frame
            tmpIV.center = toIV.center
            
            }, completion: { finished in
                NSNotificationCenter.defaultCenter().postNotificationName("PushAnimatorComplete", object: self.fromCollectionView)
                tmpIV.removeFromSuperview()
                transitionContext.completeTransition(true)
        })
        
    }
    
    func dissolveTabBar(timer: NSTimer) {
        if let tabBarView = timer.userInfo as? UIView {
            UIView.animateWithDuration(0.2, animations: {
                tabBarView.alpha = 0
                }, completion: { finished in
                    tabBarView.removeFromSuperview()
            })
        }
    }
}