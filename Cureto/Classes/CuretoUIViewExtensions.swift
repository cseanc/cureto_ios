//
//  CuretoUIViewExtensions.swift
//  Cureto
//
//  Created by Sean on 12/15/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

extension UIView {
    
    func slideInFromBottom(duration: NSTimeInterval = 0.1, completionDelegate: AnyObject? = nil) {
        let slideInFromBottomTransition = CATransition()
        
        if let delegate: AnyObject = completionDelegate {
            slideInFromBottomTransition.delegate = delegate
        }
        
        slideInFromBottomTransition.type = kCATransitionPush
        slideInFromBottomTransition.subtype = kCATransitionFromTop
        slideInFromBottomTransition.duration = duration
        slideInFromBottomTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromBottomTransition.fillMode = kCAFillModeRemoved
        
        self.layer.addAnimation(slideInFromBottomTransition, forKey: "slideInFromBottomTransition")
    }
    
    func slideInFromTop(duration: NSTimeInterval = 0.1, completionDelegate: AnyObject? = nil) {
        let slideInFromTopTransition = CATransition()
        
        if let delegate: AnyObject = completionDelegate {
            slideInFromTopTransition.delegate = delegate
        }
        
        slideInFromTopTransition.type = kCATransitionPush
        slideInFromTopTransition.subtype = kCATransitionFromBottom
        slideInFromTopTransition.duration = duration
        slideInFromTopTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromTopTransition.fillMode = kCAFillModeRemoved
        
        self.layer.addAnimation(slideInFromTopTransition, forKey: "slideInFromTopTransition")
    }
}
