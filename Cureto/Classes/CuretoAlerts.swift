//
//  CuretoAlerts.swift
//  Cureto
//
//  Created by Sean on 11/7/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class CuretoAlerts {
    
    enum MessageType {
        case LoginFailed
        case SignupFailed
        case InvalidEmail
        case EmailNoUser
        case PasswordReset
        case iCloudNotSignedIn
        case CacheCleared
        case AvailableInHongKongOnly
        case AddressCopied
        case ArticleReported
        case ArticleAskedToRevise
    }
    
    static func notifyUserWith(messageType: MessageType, viewController: UIViewController) {
        
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .Alert)
        
        var title = ""
        var message = ""
        
        switch messageType {
        case .LoginFailed:
            title = "Login Failed"
            message = "Please try again"
        case .SignupFailed:
            title = "Signup Failed"
            message = "Please try again"
        case .InvalidEmail:
            title = "Invalid Email"
            message = "Please enter a valid email"
        case .EmailNoUser:
            title = "No Account"
            message = "This email is not associated with any account in Cureto"
        case .PasswordReset:
            title = "Instruction Sent"
            message = "Password reset instruction has been sent to your inbox"
        case .iCloudNotSignedIn:
            title = "No iCloud Account"
            message = "Make sure you have signed into iCloud on your iPhone first"
        case .CacheCleared:
            title = "Cache Cleared"
            message = "All image cache cleared"
        case .AvailableInHongKongOnly:
            title = "Not Available"
            message = "Article publishing is currently available in Hong Kong only"
        case .AddressCopied:
            title = "Copied"
            message = "Address copied to clipboard"
        case .ArticleReported:
            title = "Article Reported"
            message = "You have reported this article"
        case .ArticleAskedToRevise:
            title = "Article Asked To Revise"
            message = "You have asked the author to revise this article"
        }
        
        alertController.title = title
        alertController.message = message
        let OKAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(OKAction)
        
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
}
