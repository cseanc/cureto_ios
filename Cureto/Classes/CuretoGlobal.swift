//
//  CuretoGlobal.swift
//  Cureto
//
//  Created by Sean on 11/11/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

struct CuretoGlobal {
    
    static let tableViewFirstSectionIndex = NSIndexSet(index: 0)
    static let tableViewSecondSectionIndex = NSIndexSet(index: 1)
    static let tableViewThirdSectionIndex = NSIndexSet(index: 2)
        
    static let adminUsername = "cureto"
    
    static let locationPermissonSet = "location_permission_set"
    static let featuredLastUpdated = "featured_last_updated"
    static let trueString = "TRUE"
    static let falseString = "FALSE"
    static let toBeDetermined = "AATobeDetermined"
    
    static let smile = "😉"
    static let frown = "😧"
    static let searching = "🔍"
        
    static let placeholderImageName = "Test_Picture"
    
    static let everywhere = "Everywhere"
    
    static let sourcePhotoLibrary = "SOURCE_PHOTO_LIBRARY"
    static let sourceCamera = "SOURCE_CAMERA"
    static let typeImage = "TYPE_IMAGE"
    static let typeSubImage = "TYPE_SUBIMAGE"
    
    static let locationFadedIconName = "Publish_Location_Faded"
    static let locationIconName = "Publish_Location"
    
//    static let contentFont = UIFont(name: "Iowan Old Style", size: 17)
    static let lineSpacing: CGFloat = 8.0
    static let contentFont = UIFont.systemFontOfSize(17)
    static let featuredTitleFont = UIFont.systemFontOfSize(24, weight: UIFontWeightSemibold)
    static let featuredDescFont = UIFont.systemFontOfSize(12, weight: UIFontWeightLight)
    
    static let paginationDelay: NSTimeInterval = 0.25
    
}
