//
//  CuretoArticleHelper.swift
//  Cureto
//
//  Created by Sean Choo on 3/11/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import Haneke

class CuretoArticleHelper {
    
    static func parseArticleForEdit(article: Article, controller: ArticleController, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if let title = article.title {
            controller.titleText = title
        }
        if let content = article.content {
            controller.contentText = content
        }
        if let mealType = article.mealType {
            controller.mealType = CuretoMealType.typeFromString(mealType)
        }
        if let mealPrice = article.mealPrice {
            controller.mealPrice = String(mealPrice)
        }
        controller.articleIdForEdit = article.id
        controller.foodType = article.foodType
        controller.restaurant = article.restaurant
        
        guard let coverUrl = article.originalCoverPhotoUrl else {
            onCompletion(succeeded: false, message: "FAILED")
            return
        }
        
        if let url = NSURL(string: coverUrl) {
            Shared.imageCache.fetch(URL: url, formatName: HanekeGlobals.Cache.OriginalFormatName, failure: { error in
                onCompletion(succeeded: false, message: "FAILED")
                }, success: { image in
                    controller.coverPhoto = image
                    if article.subcontent.count > 0 {
                        var subcontents = [PhotoWithSubtitle]()
                        var subcontentRetrieved = 0
                        for sub in article.subcontent {
                            guard let id = sub.id, subtitle = sub.subtitle, subphotoUrl = sub.originalSubphotoUrl else {
                                onCompletion(succeeded: false, message: "FAILED")
                                return
                            }
                            
                            if let subUrl = NSURL(string: subphotoUrl) {
                                Shared.imageCache.fetch(URL: subUrl, formatName: HanekeGlobals.Cache.OriginalFormatName, failure: { error in
                                    onCompletion(succeeded: false, message: "FAILED")
                                    
                                    }, success: { image in
                                        let subcontent = PhotoWithSubtitle(photo: image, subtitle: subtitle)
                                        subcontent.id = id
                                        subcontents.append(subcontent)
                                        subcontentRetrieved += 1
                                        if subcontentRetrieved == article.subcontent.count {
                                            let subs = CuretoRequestsHelper.photosWithSubtitleOfCorrectSequence(article.subcontent, target: subcontents)
                                            controller.subcontent = subs
                                            controller.originalSubcontent = subs
                                            onCompletion(succeeded: true, message: "SUCCESS")
                                        }
                                })
                            } else {
                                onCompletion(succeeded: false, message: "FAILED")
                            }
                        }
                        
                    } else {
                        onCompletion(succeeded: true, message: "SUCCESS")
                    }
            })
        } else {
            onCompletion(succeeded: false, message: "FAILED")
        }
    }
    
}
