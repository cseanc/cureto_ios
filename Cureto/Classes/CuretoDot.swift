//
//  CuretoDot.swift
//  Cureto
//
//  Created by Sean Choo on 3/30/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuretoDot: CAShapeLayer {
    
    let animationDuration = 0.3
    
    override init() {
        super.init()
        fillColor = UIColor.redColor().CGColor
        path = ovalPathSmall.CGPath
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var ovalPathSmall: UIBezierPath {
        return UIBezierPath(ovalInRect: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    var ovalPathLarge: UIBezierPath {
        return UIBezierPath(ovalInRect: CGRect(x: 0, y: 0, width: 9, height: 9))
    }
    
    func expand() {
        let expandAnimation = CABasicAnimation(keyPath: "path")
        expandAnimation.fromValue = ovalPathSmall.CGPath
        expandAnimation.toValue = ovalPathLarge.CGPath
        expandAnimation.duration = animationDuration
        expandAnimation.fillMode = kCAFillModeForwards
        expandAnimation.removedOnCompletion = false
        addAnimation(expandAnimation, forKey: nil)
    }
    
    func contract() {
        let contractAnimation = CABasicAnimation(keyPath: "path")
        contractAnimation.fromValue = ovalPathLarge.CGPath
        contractAnimation.toValue = ovalPathSmall.CGPath
        contractAnimation.duration = animationDuration
        contractAnimation.fillMode = kCAFillModeForwards
        contractAnimation.removedOnCompletion = false
        addAnimation(contractAnimation, forKey: nil)
    }
    
}


