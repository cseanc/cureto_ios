//
//  CuretoMealType.swift
//  Cureto
//
//  Created by Sean Choo on 3/10/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

enum CuretoMealType {
    case Breakfast
    case Lunch
    case Teatime
    case Dinner
    case Supper
    case AllDay
    
    static func stringForFoodType(type: Int) -> String {
        switch type {
        case 1:
            return "Dessert"
        case 2:
            return "Snack"
        case 3:
            return "Drinks"
        default:
            return "Breakfast"
        }
    }
    
    static func typeFromString(string: String) -> CuretoMealType {
        switch string {
        case "Breakfast":
            return .Breakfast
        case "Lunch":
            return .Lunch
        case "Teatime":
            return .Teatime
        case "Dinner":
            return .Dinner
        case "Supper":
            return .Supper
        default:
            return .AllDay
        }
    }
    
    func string() -> String {
        switch self {
        case .Breakfast:
            return "Breakfast"
        case .Lunch:
            return "Lunch"
        case .Teatime:
            return "Teatime"
        case .Dinner:
            return "Dinner"
        case .Supper:
            return "Supper"
        case .AllDay:
            return "All Day"
        }
    }
    
    func row() -> Int {
        switch self {
        case .Breakfast:
            return 0
        case .Lunch:
            return 1
        case .Teatime:
            return 2
        case .Dinner:
            return 3
        case .Supper:
            return 4
        case .AllDay:
            return 5
        }
    }
    
}
