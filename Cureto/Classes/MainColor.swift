//
//  MainColor.swift
//  Cureto
//
//  Created by Sean Choo on 6/27/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

struct MainColor {
    
    // Based on element
    static let globalTint = UIColor(red: 47.0/255.0, green: 47.0/255.0, blue: 47.0/255.0, alpha: 1.0) //#2f2f2f
    static let defaultBlueTint = UIColor(red: 13.0/255.0, green: 148.0/255.0, blue: 252.0/255.0, alpha: 1.0) //#0d94fc
    static let navBarTint = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0) //#f7f7f7
    static let tabBarTint = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0) //#f7f7f7
    static let shadeTint = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.7) //#000000 70% opacity
    static let placeholderTint = UIColor(red: 199.0/255.0, green: 199.0/255.0, blue: 204.0/255.0, alpha: 1.0) //#c7c7cc
    static let contentTint = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0) //#333333
    static let disabledButtonTint = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0) //#cccccc
    static let publishFadeTint = UIColor(red: 136.0/255.0, green: 136.0/255.0, blue: 136.0/255.0, alpha: 1.0) //#878787
    static let publishTint = UIColor(red: 47.0/255.0, green: 47.0/255.0, blue: 47.0/255.0, alpha: 1.0) //#2f2f2f
    static let separatorTint = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0) //#cccccc
    static let unreadTint = UIColor(red: 226.0/255.0, green: 244.0/255.0, blue: 255.0/255.0, alpha: 1.0) //#e2f4ff
    static let blueTint = UIColor(red: 28.0/255.0, green: 166.0/255.0, blue: 243.0/255.0, alpha: 1.0) //#1ca6f3
    static let featuredTitleTint = UIColor(red: 68.0/255.0, green: 68.0/255.0, blue: 68.0/255.0, alpha: 1.0) //#444444
    static let sectionIndexTint = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1.0) //#ededed
    
    // Raw colors
    static let darkGray = UIColor(red: 68.0/255.0, green: 68.0/255.0, blue: 68.0/255.0, alpha: 1.0) //444444
    static let lightGray = UIColor(red: 234.0/255.0, green: 234.0/255.0, blue: 234.0/255.0, alpha: 1.0) //eaeaea
    static let lighterGray = UIColor(red: 239.0/255.0, green: 239.0/255.0, blue: 239.0/255.0, alpha: 1.0) //efefef
}
