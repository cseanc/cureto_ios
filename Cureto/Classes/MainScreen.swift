//
//  MainScreen.swift
//  Cureto
//
//  Created by Sean Choo on 6/27/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

struct MainScreen {
    
    static let width = UIScreen.mainScreen().bounds.width
    static let height = UIScreen.mainScreen().bounds.height
}
