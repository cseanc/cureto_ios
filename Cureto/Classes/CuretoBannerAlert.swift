//
//  CuretoBannerAlert.swift
//  Cureto
//
//  Created by Sean Choo on 1/20/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuretoBannerAlert: NSObject {
    
    internal static var alertWindow: UIWindow?
    internal static var bannerAlert: BannerAlert?
    
    internal static var networkBanner: CuretoNetworkBanner?
    internal static var networkBannerShown = false
    
    static func showMessage(message: String) {
        if alertWindow == nil && bannerAlert == nil {
            alertWindow = UIWindow(frame: CGRect(x: 0, y: 0, width: MainScreen.width, height: 80))
            alertWindow?.backgroundColor = UIColor.clearColor()
            alertWindow?.windowLevel = UIWindowLevelAlert
            alertWindow?.makeKeyAndVisible()
            
            bannerAlert = NSBundle.mainBundle().loadNibNamed("BannerAlert", owner: self, options: nil)[0] as? BannerAlert
            bannerAlert?.frame = CGRectMake(0, 0, MainScreen.width, 80)
            bannerAlert?.alertLabel.text = message
            
            alertWindow?.addSubview(bannerAlert!)
            bannerAlert?.slideInFromTop()
            
            hideAlert(bannerAlert!)
        }
    }
    
    internal static func hideAlert(bannerAlert: BannerAlert) {
        UIView.animateWithDuration(0.3, delay: 1.8, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            bannerAlert.frame = CGRectMake(0, -80, MainScreen.width, 80)
            }, completion: { finished in
                bannerAlert.removeFromSuperview()
                self.bannerAlert = nil
                self.alertWindow = nil
        })
    }
    
    static func showNetworkBanner(connected: Bool) {
        if connected && !networkBannerShown {
            return
        }
        
        if networkBanner == nil {
            let banner = NSBundle.mainBundle().loadNibNamed("CuretoNetworkBanner", owner: self, options: nil)[0] as! CuretoNetworkBanner
            banner.frame = CGRectMake(0, 0, MainScreen.width, 42)
            
            if !connected {
                banner.alertLabel.text = "No Internet Connection 😧"
                networkBannerShown = true
                
            } else if connected && networkBannerShown {
                banner.alertLabel.text = "Connection Resumed 😉"
                networkBannerShown = false
            }

            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window?.addSubview(banner)
            banner.slideInFromTop()
            networkBanner = banner
            
            hideNetworkBanner()
        }
    }
    
    internal static func hideNetworkBanner() {
        let hiddenFrame = CGRectMake(0, -42, MainScreen.width, 42)
        UIView.animateWithDuration(0.3, delay: 1.8, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.networkBanner?.frame = hiddenFrame
            }, completion: { finished in
                self.networkBanner?.removeFromSuperview()
                self.networkBanner = nil
        })
    }
}
