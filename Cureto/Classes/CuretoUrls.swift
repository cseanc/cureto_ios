//
//  CuretoUrls.swift
//  Cureto
//
//  Created by Sean on 11/7/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

struct CuretoUrls {
    // Development
//    static let endPoint = "http://localhost:3000"
    // Production
    static let endPoint = "https://www.fridayaki.com"
    
    static let foursquareApi = "https://api.foursquare.com/v2"
    
    static let policiesAndTermsUrl = "https://s3-ap-southeast-1.amazonaws.com/cureto/policies_and_terms/policies_and_terms.html"
    
    static let designPhilosophyUrl = "https://medium.com/@cseanc/every-meal-has-a-story-16023cca9eed#.trv1sbvob"
}

// MARK: - User API

extension CuretoUrls {
    
    static let validUsernameUrl = CuretoUrls.endPoint + "/users/valid_username"
    static let validEmailUrl = CuretoUrls.endPoint + "/users/valid_email"
    static let findUsernameUrl = CuretoUrls.endPoint + "/users/username"
    
    static let loginUrl = CuretoUrls.endPoint + "/users/login"
    static let signupUrl = CuretoUrls.endPoint + "/users/signup"
    static let logoutUrl = CuretoUrls.endPoint + "/tokens"
    
    static let validateTokenUrl = CuretoUrls.endPoint + "/tokens/validate"
    
    static let userInfoUrl = CuretoUrls.endPoint + "/users/info"
    static let userShowUrl = CuretoUrls.endPoint + "/users/show"
    static let userUpdateUrl = CuretoUrls.endPoint + "/users/update"
    static let userUpdatePasswordUrl = CuretoUrls.endPoint + "/users/update_password"
    static let userResetPasswordUrl = CuretoUrls.endPoint + "/users/send_password_reset"
    static let changeCoverPhotoUrl = CuretoUrls.endPoint + "/users/change_cover_photo"
    static let removeCoverPhotoUrl = CuretoUrls.endPoint + "/users/remove_cover_photo"
    
    static let curatorSearchUrl = CuretoUrls.endPoint + "/users/search"
    static let curatorsDiscoverUrl = CuretoUrls.endPoint + "/users/discover"
}

// MARK: - Foursquare API

extension CuretoUrls {

    static let foursquarePublicAuthUrl = CuretoUrls.endPoint + "/foursquare/public_authenticate"
    static let foursquareAuthUrl = CuretoUrls.endPoint + "/foursquare/authenticate"
    static let foursquareSearchUrl = CuretoUrls.foursquareApi + "/venues/search"
    static let foursquareInfoUrl = CuretoUrls.foursquareApi + "/venues"
}

// MARK: - Article API

extension CuretoUrls {
    
    static let homeUrl = CuretoUrls.endPoint + "/articles"
    static let homeFirstUrl = CuretoUrls.endPoint + "/articles/first"
    
    static let curatedUrl = CuretoUrls.endPoint + "/articles/curated"
    
    static let chillUrl = CuretoUrls.endPoint + "/articles/chill"
    static let exploreUrl = CuretoUrls.endPoint + "/articles/explore"
    
    static let featuredArticlesUrl = CuretoUrls.endPoint + "/articles/for_featured_group"
    
    static let userArticlesUrl = CuretoUrls.endPoint + "/articles/for_user"
    static let userDraftsUrl = CuretoUrls.endPoint + "/articles/drafts_for_user"
    static let userArticleCountUrl = CuretoUrls.endPoint + "/articles/count_for_user"
    
    static let restaurantArticlesUrl = CuretoUrls.endPoint + "/articles/for_restaurant"
    
    static let createArticleUrl = CuretoUrls.endPoint + "/articles"
    static let createDraftUrl = CuretoUrls.endPoint + "/articles/draft"
    static let attachSubcontentUrl = CuretoUrls.endPoint + "/subcontents"
    
    static let retrieveArticleUrl = CuretoUrls.endPoint + "/articles/retrieve_for_user"
    
    static let updateArticleUrl = CuretoUrls.endPoint + "/articles/update"
    static let changeArticleCoverPhotoUrl = CuretoUrls.endPoint + "/articles/change_cover_photo"
    static let updateSubcontentUrl = CuretoUrls.endPoint + "/subcontents/update"
    static let changeSubphotoUrl = CuretoUrls.endPoint + "/subcontents/change_subphoto"

    static let destroyArticleUrl = CuretoUrls.endPoint + "/articles/destroy_for_user"
    static let destroySubcontentUrl = CuretoUrls.endPoint + "/subcontents/destroy_for_user"
    
    static let articleUserRelationshipUrl = CuretoUrls.endPoint + "/articles/relationship_with_user"
    
}

// MARK: - Pin API

extension CuretoUrls {
    
    static let userPinsAreasWithPictureUrl = CuretoUrls.endPoint + "/pins/areas_with_picture"
    static let userPinsWithinAreaUrl = CuretoUrls.endPoint + "/pins/for_user_within_area"
    
    static let userPinUrl = CuretoUrls.endPoint + "/pins"
    
    static let userPinsUrl = CuretoUrls.endPoint + "/pins/for_user"
    static let userAreaPinsUrl = CuretoUrls.endPoint + "/pins/for_user_by_area"
    
    static let createPinUrl = CuretoUrls.endPoint + "/pins"
    static let destroyPinUrl = CuretoUrls.endPoint + "/pins/destroy_for_user"
}

// MARK: - Star API

extension CuretoUrls {
    
    static let starCountUrl = CuretoUrls.endPoint + "/stars/count_for_article"
    
    static let createStarUrl = CuretoUrls.endPoint + "/stars"
    static let destroyStarUrl = CuretoUrls.endPoint + "/stars/destroy_for_user"
    
}

// MARK: - Message API
extension CuretoUrls {
    
    static let userUnreadCountUrl = CuretoUrls.endPoint + "/messages/unread_count_for_user"
    static let userMessagesUrl = CuretoUrls.endPoint + "/messages/for_user"
    static let markAllMessagesReadUrl = CuretoUrls.endPoint + "/messages/mark_all_read"
    
    static let createMessageUrl = CuretoUrls.endPoint + "/messages"
    
}

// MARK: - Restaurant API

extension CuretoUrls {
    
    static let restaurantAreasUrl = CuretoUrls.endPoint + "/restaurants/areas"
    static let retrieveElseCreateRestaurantUrl = CuretoUrls.endPoint + "/restaurants"
    
    static let restaurantAreasWithPictureUrl = CuretoUrls.endPoint + "/restaurants/areas_with_picture"
}

// MARK: - Featured Groups and Elements API

extension CuretoUrls {
    
    static let featuredLastUpdatedUrl = CuretoUrls.endPoint + "/featured_groups/last_updated"
    static let featuredGroupsUrl = CuretoUrls.endPoint + "/featured_groups"
    static let featuredElementsUrl = CuretoUrls.endPoint + "/featured_elements"
    
}