//
//  CuretoStringExtension.swift
//  Cureto
//
//  Created by Sean Choo on 1/21/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

extension String {
    
    func urlSafe() -> String {
        return self.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
    }
    
    func searchOptimized() -> String {
        return self.stringByReplacingOccurrencesOfString(" ", withString: "+")
    }
    
}
