//
//  CuretoTableViewHelper.swift
//  Cureto
//
//  Created by Sean Choo on 1/28/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit
import Haneke

class CuretoTableViewHelper: NSObject {
    
    static func setupTableViewSelectionForPushAnimator(controller: CuretoTableViewController, tableView: UITableView, indexPath: NSIndexPath, segueIdentifier: String) {
        guard let cell = tableView.cellForRowAtIndexPath(indexPath) as? CoverA1 else {
            return
        }
        
        controller.initialFrame = cell.convertRect(cell.coverImageView.frame, toView: nil)
        
        let article = controller.articles[indexPath.row]
        controller.selectedArticle = article
        
        if let width = article.coverPhotoWidth, height = article.coverPhotoHeight {
            let heightRatio = height / width
            let toImageView = UIImageView(frame: CGRectMake(0, controller.navigationBarHeight, MainScreen.width, MainScreen.width * heightRatio))
            toImageView.image = cell.coverImageView.image
            controller.toImageView = toImageView
        }
        
        controller.performSegueWithIdentifier(segueIdentifier, sender: controller)
    }
    
    static func setupLoadMoreForTableView(tableView: UITableView) {
        let loadMoreView = NSBundle.mainBundle().loadNibNamed("CuretoLoadMoreView", owner: self, options: nil)[0] as! CuretoLoadMoreView
        loadMoreView.frame = CGRectMake(0, 0, MainScreen.width, 60)
        loadMoreView.spinner.startAnimating()
        tableView.tableFooterView = loadMoreView
    }

    static func setupCoverCellForArticle(article: Article, row: Int, cell: CoverA1, tableViewController: UITableViewController) -> UITableViewCell {
        cell.topSeparatorHeight.constant = 0.5
        cell.selectionStyle = .None
        
        cell.coverImageView.image = UIImage(named: "Test_Picture")
        
        if let title = article.title, author = article.author {
            
            cell.titleLabel.text = title
            
            if let name = author.displayName {
                cell.authorLabel.text = name
            }
            
            if article.foodType == 0 {
                cell.mealTypeLabel.text = article.mealType
            } else {
                cell.mealTypeLabel.text = CuretoMealType.stringForFoodType(article.foodType)
            }
            
            if let mealPriceString = article.mealPrice?.roundedString() {
                cell.mealPriceLabel.text = "HK$ \(mealPriceString)"
            } else {
                cell.mealPriceLabel.text = "--"
            }
            
        }
        
        if let restaurant = article.restaurant {
            var restaurantString = ""
            if let restaurantName = restaurant.name {
                restaurantString += restaurantName
            }
            if let city = restaurant.city {
                if city != "Hong Kong" {
                    restaurantString += " (\(city))"
                }
            }
            cell.locationLabel.text = restaurantString
        } else {
            cell.locationLabel.text = "--"
        }
        
        if let coverPhotoUrl = article.coverPhotoUrl {
            if let url = NSURL(string: coverPhotoUrl) {
                cell.coverImageView.hnk_setImageFromURL(url)
            }
        }
        
        return cell
    }
}
