//
//  CuretoRequests.swift
//  Cureto
//
//  Created by Sean on 11/7/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import CloudKit
import Alamofire
import SwiftyJSON

class CuretoRequests {
    
    internal static let basicAuthHeader = "Basic dGhvc2Vfd2hvX2FyZV9jcmF6eV9lbm91Z2g6QXBpNzEzMzE0QA=="
    internal static let AESPassword = "Aes131072@"
    internal static let dateFormatter = NSDateFormatter()
    
    
    static func validateUsername(query: String, onCompletion: (valid: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(valid: false, message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.validUsernameUrl + "?q=\(query)"
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        Alamofire.request(.GET, url, headers: header, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode else {
                onCompletion(valid: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(valid: true, message: "VALID")
            } else if status == 302 {
                onCompletion(valid: false, message: "INVALID")
            } else {
                onCompletion(valid: false, message: "FAILED")
            }
        }
    }
    
    static func validateEmail(query: String, onCompletion: (valid: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(valid: false, message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.validEmailUrl + "?q=\(query)"
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        Alamofire.request(.GET, url, headers: header, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode else {
                onCompletion(valid: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(valid: true, message: "VALID")
            } else if status == 302 {
                onCompletion(valid: false, message: "INVALID")
            } else {
                onCompletion(valid: false, message: "FAILED")
            }
        }
    }
    
    static func validateToken(token: String, onCompletion: (valid: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(valid: true, message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.validateTokenUrl + "/\(token)"
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        Alamofire.request(.GET, url, headers: header, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode else {
                onCompletion(valid: true, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(valid: true, message: "VALID")
            } else {
                onCompletion(valid: false, message: "FAILED")
            }
        }
    }
    
    static func findUsernameByiCloudID(iCloudID: String, onCompletion: (username: String?, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(username: nil, message: "No Internet Connection")
            return
        }
        
        let encryptediCloudID = AESCrypt.encrypt(iCloudID, password: AESPassword)
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        let param: [String: AnyObject] = [
            "q": encryptediCloudID
        ]
        
        Alamofire.request(.POST, CuretoUrls.findUsernameUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                if let username = json["username"].string {
                    onCompletion(username: username, message: "SUCCESS")
                } else {
                    onCompletion(username: nil, message: "Can't find you. Please try again later")
                }
            } else if status == 404 {
                onCompletion(username: nil, message: "Please sign up first")
            } else {
                onCompletion(username: nil, message: "Can't find you. Please try again later")
            }
        }
    }
    
    static func getUseriCloudID(onCompletion: (instance: CKRecordID?, error: NSError?) -> Void) {
        let container = CKContainer.defaultContainer()
        container.fetchUserRecordIDWithCompletionHandler({ recordID, error in
            dispatch_async(dispatch_get_main_queue(), {
                if let e = error {
                    onCompletion(instance: nil, error: e)
                } else {
                    onCompletion(instance: recordID, error: nil)
                }
            })
        })
    }
    
    static func signup(username: String, email: String, password: String, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let encryptedPassword = AESCrypt.encrypt(password, password: AESPassword)
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        let param: [String: AnyObject] = [
            "user": [
                "username": username,
                "email": email,
                "password": encryptedPassword
            ]
        ]
        
        Alamofire.request(.POST, CuretoUrls.signupUrl, headers: header, parameters: param, encoding: .JSON).responseJSON {
            response in
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "Sign up failed. Please try again later")
                return
            }
            
            if status == 201 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "Sign up failed. Please try again later")
            }
            
        }
    }
    
    static func instantSignUp(iCloudID: String, username: String, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let encryptediCloudID = AESCrypt.encrypt(iCloudID, password: AESPassword)
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        let param: [String: AnyObject] = [
            "user": [
                "icloud_id": encryptediCloudID,
                "username": username
            ]
        ]
        
        Alamofire.request(.POST, CuretoUrls.signupUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 201 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else if status == 302 {
                onCompletion(succeeded: false, message: "You already have an account. Please log in")
            } else {
                onCompletion(succeeded: false, message: "Sign up failed. Please try again later")
            }
        }
    }
    
    static func login(username: String, password: String, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let encryptedPassword = AESCrypt.encrypt(password, password: AESPassword)
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        let param: [String: AnyObject] = [
            "user": [
                "username": username,
                "password": encryptedPassword
            ]
        ]
        
        Alamofire.request(.POST, CuretoUrls.loginUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                if let token = json["token_string"].string, username = json["username"].string {
                    CredentialStore.sharedInstance.accessToken = token
                    CredentialStore.sharedInstance.username = username
                    onCompletion(succeeded: true, message: "SUCCESS")
                } else {
                    onCompletion(succeeded: false, message: "Login failed. Please try again later")
                }
            } else if status == 403 {
                let json = JSON(data)
                if let message = json["error"].string {
                    onCompletion(succeeded: false, message: message)
                } else {
                    onCompletion(succeeded: false, message: "Login failed. Please try again later")
                }
                
            } else {
                onCompletion(succeeded: false, message: "Login failed. Please try again later")
            }
        }
    }
    
    static func instantLogIn(iCloudID: String, username: String, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let encryptediCloudID = AESCrypt.encrypt(iCloudID, password: AESPassword)
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        let param: [String: AnyObject] = [
            "user": [
                "icloud_id": encryptediCloudID,
                "username": username
            ]
        ]
        
        Alamofire.request(.POST, CuretoUrls.loginUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                if let token = json["token_string"].string, username = json["username"].string {
                    CredentialStore.sharedInstance.accessToken = token
                    CredentialStore.sharedInstance.username = username
                    onCompletion(succeeded: true, message: "SUCCESS")
                } else {
                    onCompletion(succeeded: false, message: "Login failed. Please try again later")
                }
            } else {
                onCompletion(succeeded: false, message: "Login failed. Please try again later")
            }
        }
    }
    
    static func logout(accessToken: String, all: Bool = false, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        var url = CuretoUrls.logoutUrl + "/\(accessToken)"
        
        if all {
            url = CuretoUrls.logoutUrl + "/\(accessToken)/all"
        }
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        Alamofire.request(.DELETE, url, headers: header, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode else {
                onCompletion(succeeded: false, message: "Log out failed. Please try again later")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "Log out failed. Please try again later")
            }
        }
    }
    
    static func updatePassword(token: String, original: String, new: String, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let encryptedOriginal = AESCrypt.encrypt(original, password: AESPassword)
        let encryptedNew = AESCrypt.encrypt(new, password: AESPassword)
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let param: [String: AnyObject] = [
            "original": encryptedOriginal,
            "new": encryptedNew
        ]
        
        Alamofire.request(.PATCH, CuretoUrls.userUpdatePasswordUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(succeeded: false, message: "Password change failed. Please try again later")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
                
            } else {
                let json = JSON(data)
                if let message = json["error"].string {
                    onCompletion(succeeded: false, message: message)
                } else {
                    onCompletion(succeeded: false, message: "Password change failed. Please try again later")
                }
            }
        }
    }
    
    static func resetPassword(email: String, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        let param: [String: AnyObject] = [
            "email": email
        ]
        
        Alamofire.request(.PATCH, CuretoUrls.userResetPasswordUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
    static func editProfile(token: String, displayName: String, bio: String, onCompletion: (result: User?, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: nil, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let param: [String: AnyObject] = [
            "user": [
                "display_name": displayName,
                "bio": bio
            ]
        ]
        
        Alamofire.request(.PATCH, CuretoUrls.userUpdateUrl, headers: header, parameters: param, encoding: .JSON).responseJSON {
            response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: nil, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                if let user = CuretoRequestsHelper.parseUserWithJSON(json) {
                    onCompletion(result: user, message: "SUCCESS")
                } else {
                    onCompletion(result: nil, message: "FAILED")
                }
            } else {
                onCompletion(result: nil, message: "FAILED")
            }
        }
    }
    
}

// MARK: - User

extension CuretoRequests {
    
    static func discoverCurators(seed: Int, page: Int, onCompletion: (result: [User], message: String) -> Void) {
        var users = [User]()
        
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: users, message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.curatorsDiscoverUrl + "?seed=\(seed)&page=\(page)"
        
        Alamofire.request(.GET, url).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: users, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                users = CuretoRequestsHelper.parseUsersWithJSON(json)
                onCompletion(result: users, message: "SUCCESS")
                
            } else {
                onCompletion(result: users, message: "FAILED")
            }
        }
    }
    
    static func searchCurators(query: String, onCompletion: (result: [User], message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [User](), message: "No Internet Connection")
            return
        }
        
        let cleanQuery = query.lowercaseString.urlSafe()
        let url = CuretoUrls.curatorSearchUrl + "?q=\(cleanQuery)"
        
        Alamofire.request(.GET, url, encoding: .JSON).responseJSON { response in
            var curators = [User]()
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: curators, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                curators = CuretoRequestsHelper.parseUsersWithJSON(json)
                onCompletion(result: curators, message: "SUCCESS")
                
            } else {
                onCompletion(result: curators, message: "FAILED")
            }
        }
    }
    
    static func getUserWithId(id: Int, onCompletion: (result: User?, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: nil, message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.userShowUrl + "/\(id)"
        
        Alamofire.request(.GET, url).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: nil, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                let user = User()
                if let username = json["user"]["username"].string {
                    user.username = username
                }
                if let displayName = json["user"]["display_name"].string {
                    user.displayName = displayName
                }
                if let bio = json["user"]["bio"].string {
                    user.bio = bio
                }
                if let coverUrl = json["user"]["cover_photo"]["cover_photo"]["retina"]["url"].string {
                    user.profileImageUrl = coverUrl
                }
                onCompletion(result: user, message: "SUCCESS")
                
            } else {
                onCompletion(result: nil, message: "FAILED")
            }
        }
    }
    
    static func getUserInfo(username: String, onCompletion: (result: User?, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: nil, message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.userInfoUrl + "?username=\(username)"
        
        Alamofire.request(.GET, url, encoding: .JSON).responseJSON {
            response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: nil, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                let user = CuretoRequestsHelper.parseUserWithJSON(json["user"])
                onCompletion(result: user, message: "SUCCESS")
            } else {
                onCompletion(result: nil, message: "FAILED")
            }
        }
    }
    
    static func getUserArticleCount(username: String, onCompletion: (result: Int, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: 0, message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.userArticleCountUrl + "?username=\(username)"
        
        Alamofire.request(.GET, url, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: 0, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                if let count = json["count"].int {
                    onCompletion(result: count, message: "SUCCESS")
                } else {
                    onCompletion(result: 0, message: "FAILED")
                }
            } else {
                onCompletion(result: 0, message: "FAILED")
            }
        }
    }
    
    static func getUserArticles(username: String, page: Int = 1, onCompletion: (result: [Article], message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [Article](), message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.userArticlesUrl + "?username=\(username)&page=\(page)"
        
        Alamofire.request(.GET, url, encoding: .JSON).responseJSON { response in
            
            var articles = [Article]()
            
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: articles, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                articles = CuretoRequestsHelper.parseArticlesWithJSON(json)
                onCompletion(result: articles, message: "SUCCESS")
            } else {
                onCompletion(result: articles, message: "FAILED")
            }
        }
    }
    
}

// MARK: - Image upload

extension CuretoRequests {
    
    static func changeCoverPhoto(token: String, image: NSData, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let param: [String: AnyObject] = [
            "cover_photo": image.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        ]
        
        Alamofire.request(.POST, CuretoUrls.changeCoverPhotoUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            
            guard let status = response.response?.statusCode else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
    static func removeCoverPhoto(token: String, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.removeCoverPhotoUrl + "/\(token)"
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        Alamofire.request(.DELETE, url, headers: header, encoding: .JSON).responseJSON { response in
            
            guard let status = response.response?.statusCode else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
}

// MARK: - Foursquare API

extension CuretoRequests {
    
    static func publicAuthWithFoursquare(onCompletion: (result: FoursquareAuth?, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: nil, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": basicAuthHeader
        ]
        
        Alamofire.request(.GET, CuretoUrls.foursquarePublicAuthUrl, headers: header, encoding: .JSON).responseJSON {
            response in
            if let status = response.response?.statusCode {
                if status == 200 {
                    if let data = response.result.value {
                        let json = JSON(data)
                        if let clientId = json["client_id"].string,
                            clientSecret = json["client_secret"].string {
                            let id = AESCrypt.decrypt(clientId, password: AESPassword)
                            let secret = AESCrypt.decrypt(clientSecret, password: AESPassword)
                            let foursquareAuth = FoursquareAuth(clientId: id, clientSecret: secret)
                            onCompletion(result: foursquareAuth, message: "SUCCESS")
                        } else {
                            onCompletion(result: nil, message: "FAILED")
                        }
                    } else {
                        onCompletion(result: nil, message: "FAILED")
                    }
                } else {
                    onCompletion(result: nil, message: "FAILED")
                }
            }
        }
    }
    
    static func authWithFoursquare(token: String, onCompletion: (result: FoursquareAuth?, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: nil, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        Alamofire.request(.GET, CuretoUrls.foursquareAuthUrl, headers: header, encoding: .JSON).responseJSON {
            response in
            if let status = response.response?.statusCode {
                if status == 200 {
                    if let data = response.result.value {
                        let json = JSON(data)
                        if let clientId = json["client_id"].string,
                            clientSecret = json["client_secret"].string {
                                let id = AESCrypt.decrypt(clientId, password: AESPassword)
                                let secret = AESCrypt.decrypt(clientSecret, password: AESPassword)
                                let foursquareAuth = FoursquareAuth(clientId: id, clientSecret: secret)
                                onCompletion(result: foursquareAuth, message: "SUCCESS")
                        } else {
                            onCompletion(result: nil, message: "FAILED")
                        }
                    } else {
                        onCompletion(result: nil, message: "FAILED")
                    }
                } else {
                    onCompletion(result: nil, message: "FAILED")
                }
            }
        }
    }
    
    static func getNearbyPlaces(auth: FoursquareAuth, latitude: Double, longitude: Double, query: String, onCompletion: (places: [Restaurant], message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(places: [Restaurant](), message: "No Internet Connection")
            return
        }
        
        let ll = "\(latitude),\(longitude)".urlSafe()
        let radius = "200"
        let categoryId = "4d4b7105d754a06374d81259"
        let locale = "en"
        let location = "Hong Kong".urlSafe()
        
        let today = NSDate()
        dateFormatter.dateFormat = "YYYYMMdd"
        let v = dateFormatter.stringFromDate(today)
        
        var url = ""
        
        if query == "" {
            url = CuretoUrls.foursquareSearchUrl + "?client_id=\(auth.clientId!)&client_secret=\(auth.clientSecret!)&ll=\(ll)&radius=\(radius)&location=\(location)&v=\(v)&categoryId=\(categoryId)&query=\(query)&locale=\(locale)"
        } else {
            url = CuretoUrls.foursquareSearchUrl + "?client_id=\(auth.clientId!)&client_secret=\(auth.clientSecret!)&ll=\(ll)&location=\(location)&v=\(v)&categoryId=\(categoryId)&query=\(query)&locale=\(locale)"
        }
                
        Alamofire.request(.GET, url, encoding: .JSON).responseJSON { response in
            if let status = response.response?.statusCode {
                if status == 200 {
                    if let data = response.result.value {
                        let json = JSON(data)
                        let places = CuretoRequestsHelper.parseRestaurantsWithJSON(json)
                        onCompletion(places: places, message: "SUCCESS")
                    }
                } else {
                    onCompletion(places: [Restaurant](), message: "FAILED")
                }
            }
        }
    }
    
    static func getPlaceInfo(auth: FoursquareAuth, id: String, onCompletion: (place: Restaurant?, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(place: nil, message: "No Internet Connection")
            return
        }
        
        let locale = "en"
        
        let today = NSDate()
        dateFormatter.dateFormat = "YYYYMMdd"
        let v = dateFormatter.stringFromDate(today)
        
        let url = CuretoUrls.foursquareInfoUrl + "/\(id)?client_id=\(auth.clientId!)&client_secret=\(auth.clientSecret!)&v=\(v)&locale=\(locale)"
        
        Alamofire.request(.GET, url, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(place: nil, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                let restaurant = CuretoRequestsHelper.parsePlaceWithJSON(json)
                onCompletion(place: restaurant, message: "SUCCESS")

            } else {
                onCompletion(place: nil, message: "FAILED")
            }
        }
    }
    
}

// MARK: - Handling articles

extension CuretoRequests {
    
    static func getHomeFirstArticleId(style: Int = 0, foodType: Int = 0, priceRange: Int = 4, onCompletion: (result: Int?, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: nil, message: "No Internet Connection")
            return
        }
        
        var url = CuretoUrls.homeFirstUrl + "?page=1"
        
        if foodType == 1 {
            url += "&type=0"
        } else if foodType == 2 {
            url += "&type=1"
        }
        
        if priceRange == 0 {
            url += "&price=50"
        } else if priceRange == 1 {
            url += "&price=100"
        } else if priceRange == 2 {
            url += "&price=200"
        } else if priceRange == 3 {
            url += "&price=300"
        }
        
        Alamofire.request(.GET, url).responseJSON { response in
            
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: nil, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                if let id = json["article"]["id"].int {
                    onCompletion(result: id, message: "SUCCESS")
                    
                } else {
                    onCompletion(result: nil, message: "FAILED")
                }
                
            } else {
                onCompletion(result: nil, message: "FAILED")
            }
        }
    }
    
    static func getCuratedArticles(seed: Int, page: Int = 1, onCompletion: (result: [Article], message: String) -> Void) {
        var articles = [Article]()
        
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: articles, message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.curatedUrl + "?seed=\(seed)&page=\(page)"
        
        Alamofire.request(.GET, url).responseJSON { response in
            
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: articles, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                articles = CuretoRequestsHelper.parseArticlesWithJSON(json)
                onCompletion(result: articles, message: "SUCCESS")
                
            } else {
                onCompletion(result: articles, message: "FAILED")
            }
        }
    }
    
    static func getHomeArticles(page: Int = 1, style: Int = 0, foodType: Int = 0, priceRange: Int = 4, onCompletion: (result: [Article], message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [Article](), message: "No Internet Connection")
            return
        }
        
        var url = CuretoUrls.homeUrl + "?page=\(page)"
        
        if foodType == 1 {
            url += "&type=0"
        } else if foodType == 2 {
            url += "&type=1"
        } else if foodType == 3 {
            url += "&type=2"
        } else if foodType == 4 {
            url += "&type=3"
        }
        
        if priceRange == 0 {
            url += "&price=50"
        } else if priceRange == 1 {
            url += "&price=100"
        } else if priceRange == 2 {
            url += "&price=200"
        } else if priceRange == 3 {
            url += "&price=300"
        }
        
        Alamofire.request(.GET, url).responseJSON { response in
                        
            var articles = [Article]()
            
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: articles, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                articles = CuretoRequestsHelper.parseArticlesWithJSON(json)
                onCompletion(result: articles, message: "SUCCESS")
            } else {
                onCompletion(result: articles, message: "FAILED")
            }
        }
    }
    
    static func getRestaurantArticles(page: Int = 1, id: Int, onCompletion: (result: [Article], message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [Article](), message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.restaurantArticlesUrl + "?id=\(id)&page=\(page)"
        
        Alamofire.request(.GET, url).responseJSON { response in
            
            var articles = [Article]()
            
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: articles, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                articles = CuretoRequestsHelper.parseArticlesWithJSON(json)
                onCompletion(result: articles, message: "SUCCESS")
            } else {
                onCompletion(result: articles, message: "FAILED")
            }
        }
    }
    
    static func saveAsDraft(token: String, articleHead: ArticleHead, subContent: [PhotoWithSubtitle], restaurant: Restaurant?, foodType: Int, mealType: CuretoMealType?, mealPrice: Float?, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        CuretoProgressHUD.sharedInstance.showProgress(1)
        
        guard let title = articleHead.title, content = articleHead.content, coverPhoto = articleHead.coverPhoto else {
            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
            onCompletion(succeeded: false, message: "FAILED")
            return
        }
        
        if let cover = UIImageJPEGRepresentation(coverPhoto, 1.0) {
            
            let header: [String: String] = [
                "Authorization": "Token \(token)"
            ]
            
            var param: [String: [String: AnyObject]] = [
                "article": [
                    "title": title,
                    "content": content,
                    "cover_photo": cover.base64EncodedStringWithOptions(.Encoding64CharacterLineLength),
                    "cover_photo_width": coverPhoto.size.width,
                    "cover_photo_height": coverPhoto.size.height,
                    "food_type": foodType
                ]
            ]
            
            if let restaurantID = restaurant?.id {
                param["article"]!["restaurant_id"] = restaurantID
            }
            
            if let type = mealType {
                param["article"]!["meal_type"] = type.string()
            } else {
                param["article"]!["meal_type"] = CuretoMealType.Breakfast.string()
            }
            
            if let price = mealPrice {
                param["article"]!["meal_price"] = price
            }
            
            Alamofire.request(.POST, CuretoUrls.createDraftUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
                guard let status = response.response?.statusCode, data = response.result.value else {
                    NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                    onCompletion(succeeded: false, message: "FAILED")
                    return
                }
                
                if status == 201 {
                    let json = JSON(data)
                    if let serverArticleID = json["article"]["id"].int {
                        NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 50)
                        
                        if subContent.count == 0 {
                            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 100)
                            onCompletion(succeeded: true, message: "SUCCESS")
                        } else {
                            var subContentUploaded = 0
                            for i in 0...subContent.count-1 {
                                CuretoRequests.attachSubcontent(token, subcontent: subContent[i], sequence: i, articleID: serverArticleID, onCompletion: { succeeded, message in
                                    if succeeded {
                                        subContentUploaded += 1
                                        if subContentUploaded == subContent.count {
                                            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 100)
                                            onCompletion(succeeded: true, message: "FINISHED PUBLISHING")
                                        } else {
                                            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 75)
                                        }
                                    } else {
                                        NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                                        onCompletion(succeeded: false, message: "FAILED")
                                    }
                                })
                            }
                        }
                        
                    } else {
                        NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                        onCompletion(succeeded: false, message: "FAILED")
                    }
                } else {
                    NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                    onCompletion(succeeded: false, message: "FAILED")
                }
            }
        } else {
            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
            onCompletion(succeeded: false, message: "FAILED")
        }
    }

    // Main method - Publish Article
    static func publishArticle(token: String, articleHead: ArticleHead, subContent: [PhotoWithSubtitle], restaurant: Restaurant, foodType: Int, mealType: CuretoMealType?, mealPrice: Float, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        CuretoProgressHUD.sharedInstance.showProgress()
        
        CuretoRequests.createArticle(token, articleHead: articleHead, restaurant: restaurant, foodType: foodType, mealType: mealType, mealPrice: mealPrice, onCompletion: { serverArticleID, message in
            
            guard let articleID = serverArticleID else {
                NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if subContent.count == 0 {
                NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 100)
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                var subContentUploaded = 0
                for i in 0...subContent.count-1 {
                    CuretoRequests.attachSubcontent(token, subcontent: subContent[i], sequence: i, articleID: articleID, onCompletion: {
                        succeeded, message in
                        if succeeded {
                            subContentUploaded += 1
                            if subContentUploaded == subContent.count {
                                NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 100)
                                onCompletion(succeeded: true, message: "FINISHED PUBLISHING")
                            } else {
                                NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 75)
                            }
                        } else {
                            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                            onCompletion(succeeded: false, message: "FAILED")
                        }
                    })
                }
            }
        })
    }
    
    static func retrieveElseCreateRestaurant(token: String, restaurant: Restaurant, onCompletion: (result: Restaurant?, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: nil, message: "No Internet Connection")
            return
        }
        
        guard let identifier = restaurant.identifier, name = restaurant.name, latitude = restaurant.latitude, longitude = restaurant.longitude else {
            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
            onCompletion(result: nil, message: "FAILED")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        var param: [String: [String: AnyObject]] = [
            "restaurant": [
                "identifier": identifier,
                "name": name,
                "latitude": latitude,
                "longitude": longitude
            ]
        ]
        
        if let address = restaurant.address {
            param["restaurant"]!["street"] = address
        }
        
        if let categoryId = restaurant.categoryId {
            param["restaurant"]!["category_id"] = categoryId
        }
        
        if let categoryName = restaurant.categoryName {
            param["restaurant"]!["category_name"] = categoryName
        }
        
        if let city = restaurant.city {
            param["restaurant"]!["city"] = city
        }
        
        if let country = restaurant.country {
            param["restaurant"]!["country"] = country
        }
        
        Alamofire.request(.POST, CuretoUrls.retrieveElseCreateRestaurantUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: nil, message: "FAILED")
                return
            }
            
            if status == 200 || status == 201 {
                NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 25)
                
                let json = JSON(data)
                if let restaurant = CuretoRequestsHelper.parseServerRestaurantWithJSON(json) {
                    onCompletion(result: restaurant, message: "SUCCESS")
                } else {
                    onCompletion(result: nil, message: "FAILED")
                }
                
            } else {
                NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                onCompletion(result: nil, message: "FAILED")
            }
        }
    }
    
    static func createArticle(token: String, articleHead: ArticleHead, restaurant: Restaurant, foodType: Int, mealType: CuretoMealType?, mealPrice: Float, onCompletion: (result: Int?, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: nil, message: "No Internet Connection")
            return
        }
        
        guard let title = articleHead.title, content = articleHead.content, coverPhoto = articleHead.coverPhoto, restaurantId = restaurant.id else {
            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
            onCompletion(result: nil, message: "FAILED")
            return
        }
        
        if let cover = UIImageJPEGRepresentation(coverPhoto, 1.0) {
            
            let header: [String: String] = [
                "Authorization": "Token \(token)"
            ]
            
            var param: [String: [String: AnyObject]] = [
                "article": [
                    "title": title,
                    "content": content,
                    "cover_photo": cover.base64EncodedStringWithOptions(.Encoding64CharacterLineLength),
                    "cover_photo_width": coverPhoto.size.width,
                    "cover_photo_height": coverPhoto.size.height,
                    "food_type": foodType,
                    "meal_price": mealPrice,
                    "restaurant_id": restaurantId
                ]
            ]
            
            if let type = mealType {
                param["article"]!["meal_type"] = type.string()
            } else {
                param["article"]!["meal_type"] = CuretoMealType.Breakfast.string()
            }
            
            Alamofire.request(.POST, CuretoUrls.createArticleUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
                guard let status = response.response?.statusCode, data = response.result.value else {
                    NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                    onCompletion(result: nil, message: "FAILED")
                    return
                }
                
                if status == 201 {
                    let json = JSON(data)
                    if let serverArticleID = json["article"]["id"].int {
                        NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 50)
                        onCompletion(result: serverArticleID, message: "SUCCESS")
                    } else {
                        NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                        onCompletion(result: nil, message: "FAILED")
                    }
                    
                } else {
                    NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                    onCompletion(result: nil, message: "FAILED")
                }
            }
            
        } else {
            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
            onCompletion(result: nil, message: "FAILED")
        }
        
    }
    
    static func attachSubcontent(token: String, subcontent: PhotoWithSubtitle, sequence: Int, articleID: Int, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        guard let subphoto = subcontent.photo, subtitle = subcontent.subtitle else {
            onCompletion(succeeded: false, message: "FAILED")
            return
        }
        
        if let photo = UIImageJPEGRepresentation(subphoto, 1.0) {
            
            let header: [String: String] = [
                "Authorization": "Token \(token)"
            ]
            
            let param: [String: AnyObject] = [
                "subcontent": [
                    "subphoto": photo.base64EncodedStringWithOptions(.Encoding64CharacterLineLength),
                    "subphoto_width": subphoto.size.width,
                    "subphoto_height": subphoto.size.height,
                    "subtitle": subtitle,
                    "sequence": sequence,
                    "article_id": articleID
                ]
            ]
            
            Alamofire.request(.POST, CuretoUrls.attachSubcontentUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
                
                guard let status = response.response?.statusCode else {
                    onCompletion(succeeded: false, message: "FAILED")
                    return
                }
                
                if status == 201 {
                    onCompletion(succeeded: true, message: "SUCCESS")
                } else {
                    onCompletion(succeeded: false, message: "FAILED")
                }
            }
        }
        
    }
    
    static func destroyArticle(token: String, articleId: Int, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let url = CuretoUrls.destroyArticleUrl + "/\(articleId)"
        
        Alamofire.request(.POST, url, headers: header, encoding: .JSON).responseJSON {
            response in
            guard let status = response.response?.statusCode else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
    static func destroySubcontent(token: String, id: Int, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let url = CuretoUrls.destroySubcontentUrl + "/\(id)"
        
        Alamofire.request(.POST, url, headers: header, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
}

// MARK: - Handling drafts

extension CuretoRequests {
    
    static func getDrafts(token: String, page: Int = 1, onCompletion: (result: [Article], message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [Article](), message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let url = CuretoUrls.userDraftsUrl + "?page=\(page)"
        
        Alamofire.request(.GET, url, headers: header, encoding: .JSON).responseJSON { response in
            var drafts = [Article]()
            
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: drafts, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                drafts = CuretoRequestsHelper.parseDraftsWithJSON(json)
                onCompletion(result: drafts, message: "SUCCESS")
            } else {
                onCompletion(result: drafts, message: "FAILED")
            }
        }
    }
    
}

// MARK: - Handling edit articles

extension CuretoRequests {
    
    static func retrieveArticle(token: String, id: Int, onCompletion: (result: Article?, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: nil, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let url = CuretoUrls.retrieveArticleUrl + "/\(id)"
        
        Alamofire.request(.GET, url, headers: header, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: nil, message: "FAILED")
                return
            }
            
            if status == 200 {
               let json = JSON(data)
                let article = CuretoRequestsHelper.parseArticleWithJSON(json)
                onCompletion(result: article, message: "SUCCESS")
            } else {
                onCompletion(result: nil, message: "FAILED")
            }
        }
        
    }
    
    // Main method - Update Article
    static func updateArticle(token: String, coverDidChange: Bool, articleHead: ArticleHead, originalSubContent: [PhotoWithSubtitle], subContent: [PhotoWithSubtitle], restaurant: Restaurant?, draft: Bool, foodType: Int, mealType: CuretoMealType?, mealPrice: Float?, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        CuretoProgressHUD.sharedInstance.showProgress(2)
        
        updateArticleInfo(token, articleHead: articleHead, restaurant: restaurant, draft: draft, foodType: foodType, mealType: mealType, mealPrice: mealPrice, onCompletion: { succeeded, message in
            
            if succeeded {
                if coverDidChange {
                    
                    changeArticleCoverPhoto(token, articleHead: articleHead, onCompletion: { succeeded, message in
                        
                        if succeeded {
                            
                            updateSubcontent(token, articleHead: articleHead, originalSubContent: originalSubContent, subContent: subContent, onCompletion: { succeeded, message in
                                
                                if succeeded {
                                    NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 100)
                                    onCompletion(succeeded: true, message: "SUCCESS")
                                } else {
                                    NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                                    onCompletion(succeeded: false, message: "FAILED")
                                }
                            })
                            
                        } else {
                            // Failed to change cover photo
                            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                            onCompletion(succeeded: false, message: "FAILED")
                        }
                    })
                    
                } else {
                    
                    updateSubcontent(token, articleHead: articleHead, originalSubContent: originalSubContent, subContent: subContent, onCompletion: {
                        succeeded, message in
                        
                        if succeeded {
                            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 100)
                            onCompletion(succeeded: true, message: "SUCCESS")
                        } else {
                            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                            onCompletion(succeeded: false, message: "FAILED")
                        }
                    })
                    
                }
                
            } else {
                // Failed to update article info
                NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: -1)
                onCompletion(succeeded: false, message: "FAILED")
            }
        })
    }
    
    static func updateArticleInfo(token: String, articleHead: ArticleHead, restaurant: Restaurant?, draft: Bool, foodType: Int, mealType: CuretoMealType?, mealPrice: Float?, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        guard let id = articleHead.id, title = articleHead.title, content = articleHead.content else {
            onCompletion(succeeded: false, message: "FAILED")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        var param: [String: [String: AnyObject]] = [
            "article": [
                "title": title,
                "content": content,
                "draft": draft,
                "food_type": foodType
            ]
        ]
        
        if let restaurantID = restaurant?.id {
            param["article"]!["restaurant_id"] = restaurantID
        }
        
        if let type = mealType {
            param["article"]!["meal_type"] = type.string()
        } else {
            param["article"]!["meal_type"] = CuretoMealType.Breakfast.string()
        }
        
        if let price = mealPrice {
            param["article"]!["meal_price"] = price
        }
        
        let url = CuretoUrls.updateArticleUrl + "/\(id)"
        
        Alamofire.request(.PATCH, url, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 25)
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
    static func changeArticleCoverPhoto(token: String, articleHead: ArticleHead, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        guard let id = articleHead.id, coverPhoto = articleHead.coverPhoto else {
            onCompletion(succeeded: false, message: "FAILED")
            return
        }
        
        if let cover = UIImageJPEGRepresentation(coverPhoto, 1.0) {
            
            let header: [String: String] = [
                "Authorization": "Token \(token)"
            ]
            
            let param: [String: AnyObject] = [
                "cover_photo": cover.base64EncodedStringWithOptions(.Encoding64CharacterLineLength),
                "cover_photo_width": coverPhoto.size.width,
                "cover_photo_height": coverPhoto.size.height
            ]
            
            let url = CuretoUrls.changeArticleCoverPhotoUrl + "/\(id)"
            
            Alamofire.request(.PATCH, url, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
                guard let status = response.response?.statusCode, _ = response.result.value else {
                    onCompletion(succeeded: false, message: "FAILED")
                    return
                }
                
                if status == 200 {
                    NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 50)
                    onCompletion(succeeded: true, message: "SUCCESS")
                } else {
                    onCompletion(succeeded: false, message: "FAILED")
                }
            }
            
        } else {
            onCompletion(succeeded: false, message: "FAILED")
        }
    }
    
    // Sub method - Update Subcontent
    static func updateSubcontent(token: String, articleHead: ArticleHead, originalSubContent: [PhotoWithSubtitle], subContent: [PhotoWithSubtitle], onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        if originalSubContent.count == 0 && subContent.count == 0 {
            onCompletion(succeeded: true, message: "SUCCESS")
        } else {
            let originalIds = CuretoRequestsHelper.getIdsFromPhotosWithSubtitle(originalSubContent)
            let ids = CuretoRequestsHelper.getIdsFromPhotosWithSubtitle(subContent)
            
            // Step 1: Destroy subcontent that exists no more
            var toBeDeleted = [Int]()
            
            if originalIds.count > 0 {
                for i in 0...originalIds.count-1 {
                    if !ids.contains(originalIds[i]) {
                        toBeDeleted.append(originalIds[i])
                    }
                }
            }

            destroyDeletedSubcontents(token, ids: toBeDeleted, onCompletion: { succeeded, message in
                if succeeded {
                    NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 60)
                    // Step 2: Update existing subcontents
                    var toBeUpdated = [PhotoWithSubtitle]()
                    var toBeUploaded = [PhotoWithSubtitle]()
                    var sequenceForUpdate = [Int]()
                    var sequenceForUpload = [Int]()
                    
                    if ids.count > 0 {
                        for j in 0...ids.count-1 {
                            if ids[j] != 0 {
                                toBeUpdated.append(subContent[j])
                                sequenceForUpdate.append(j)
                            } else {
                                toBeUploaded.append(subContent[j])
                                sequenceForUpload.append(j)
                            }
                        }
                    }

                    updateExistingSubcontents(token, sequences: sequenceForUpdate, subcontents: toBeUpdated, onCompletion: {
                        succeeded, message in
                        if succeeded {
                            NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 70)
                            
                            // Step 3: Upload new subcontents
                            uploadNewSubcontents(token, articleHead: articleHead, sequences: sequenceForUpload, subcontents: toBeUploaded, onCompletion: { succeeded, message in
                                if succeeded {
                                    NSNotificationCenter.defaultCenter().postNotificationName("UploadProgressChanged", object: 80)
                                    onCompletion(succeeded: true, message: "SUCCESS")
                                } else {
                                    onCompletion(succeeded: false, message: "FAILED")
                                }
                            })
                            
                        } else {
                            onCompletion(succeeded: false, message: "FAILED")
                        }
                    })
                    
                } else {
                    onCompletion(succeeded: false, message: "FAILED")
                }
            })
        }
    }
    
    // Method for Step 1 of updating subcontents
    static func destroyDeletedSubcontents(token: String, ids: [Int], onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        if ids.count == 0 {
            onCompletion(succeeded: true, message: "SUCCESS")
        } else {
            var totalDeleted = 0
            for i in 0...ids.count-1 {
                destroySubcontent(token, id: ids[i], onCompletion: { succeeded, message in
                    if succeeded {
                        totalDeleted += 1
                        if totalDeleted == ids.count {
                            onCompletion(succeeded: true, message: "SUCCESS")
                        }
                    } else {
                        onCompletion(succeeded: false, message: "FAILED")
                    }
                })
            }
        }
    }
    
    // Method for Step 2 of updating subcontents
    static func updateExistingSubcontents(token: String, sequences: [Int], subcontents: [PhotoWithSubtitle], onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        if subcontents.count == 0 {
            onCompletion(succeeded: true, message: "SUCCESS")
        } else {
            var totalUpdated = 0
            for i in 0...subcontents.count-1 {
                let sub = subcontents[i]
                updateSubcontentInfo(token, sequence: sequences[i], subContent: sub, onCompletion: { succeeded, message in
                    if succeeded {
                        if sub.subphotoDidChange {
                            changeSubphoto(token, subContent: sub, onCompletion: { succeeded, message in
                                if succeeded {
                                    totalUpdated += 1
                                    if totalUpdated == subcontents.count {
                                        onCompletion(succeeded: true, message: "SUCCESS")
                                    }
                                } else {
                                    onCompletion(succeeded: false, message: "FAILED")
                                }
                            })
                            
                        } else {
                            totalUpdated += 1
                            if totalUpdated == subcontents.count {
                                onCompletion(succeeded: true, message: "SUCCESS")
                            }
                        }
                        
                    } else {
                        onCompletion(succeeded: false, message: "FAILED")
                    }
                })
            }
        }
    }
    
    // Method for Step 3 of updating subcontents
    static func uploadNewSubcontents(token: String, articleHead: ArticleHead, sequences: [Int], subcontents: [PhotoWithSubtitle], onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        if subcontents.count == 0 {
            onCompletion(succeeded: true, message: "SUCCESS")
        } else {
            guard let id = articleHead.id else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            var totalUploaded = 0
            for i in 0...subcontents.count-1 {
                let sub = subcontents[i]
                attachSubcontent(token, subcontent: sub, sequence: sequences[i], articleID: id, onCompletion: { succeeded, message in
                    if succeeded {
                        totalUploaded += 1
                        if totalUploaded == subcontents.count {
                            onCompletion(succeeded: true, message: "SUCCESS")
                        }
                    } else {
                        onCompletion(succeeded: false, message: "FAILED")
                    }
                })
            }
        }
    }
    
    static func updateSubcontentInfo(token: String, sequence: Int, subContent: PhotoWithSubtitle, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        guard let id = subContent.id, subtitle = subContent.subtitle else {
            onCompletion(succeeded: false, message: "FAILED")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let param: [String: AnyObject] = [
            "subcontent": [
                "subtitle": subtitle,
                "sequence": sequence
            ]
        ]
        
        let url = CuretoUrls.updateSubcontentUrl + "/\(id)"
        
        Alamofire.request(.PATCH, url, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
    static func changeSubphoto(token: String, subContent: PhotoWithSubtitle, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        guard let id = subContent.id, subphoto = subContent.photo else {
            onCompletion(succeeded: false, message: "FAILED")
            return
        }
        
        if let photo = UIImageJPEGRepresentation(subphoto, 1.0) {
            
            let header: [String: String] = [
                "Authorization": "Token \(token)"
            ]
            
            let param: [String: AnyObject] = [
                "subphoto": photo.base64EncodedStringWithOptions(.Encoding64CharacterLineLength),
                "subphoto_width": subphoto.size.width,
                "subphoto_height": subphoto.size.height
            ]
            
            let url = CuretoUrls.changeSubphotoUrl + "/\(id)"
            
            Alamofire.request(.PATCH, url, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
                guard let status = response.response?.statusCode, _ = response.result.value else {
                    onCompletion(succeeded: false, message: "FAILED")
                    return
                }
                
                if status == 200 {
                    onCompletion(succeeded: true, message: "SUCCESS")
                } else {
                    onCompletion(succeeded: false, message: "FAILED")
                }
            }
            
        } else {
            onCompletion(succeeded: false, message: "FAILED")
        }
    }
    
    // Relationship with user (star and pin)
    static func getArticleRelationshipWithUser(token: String, articleID: Int, onCompletion: (starred: Bool, pinned: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(starred: false, pinned: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let url = CuretoUrls.articleUserRelationshipUrl + "/\(articleID)"
        
        Alamofire.request(.GET, url, headers: header, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(starred: false, pinned: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                
                var star = false
                var pin = false
                
                if let starON = json["starred"].bool {
                    if starON {
                        star = true
                    }
                }
                
                if let pinON = json["pinned"].bool {
                    if pinON {
                        pin = true
                    }
                }
                
                onCompletion(starred: star, pinned: pin, message: "SUCCESS")
                
            } else {
                onCompletion(starred: false, pinned: false, message: "FAILED")
            }
        }
        
    }
}

// MARK: - Handling explore

extension CuretoRequests {
    
    static func getRestaurantAreasWithPicture(onCompletion: (areasWithPicture: [GroupedAreaWithPicture], message: String) -> Void) {
        var areas = [GroupedAreaWithPicture]()
        
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(areasWithPicture: areas, message: "No Internet Connection")
            return
        }
        
        Alamofire.request(.GET, CuretoUrls.restaurantAreasWithPictureUrl).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(areasWithPicture: areas, message: "FAILED")
                return
            }
                        
            if status == 200 {
                let json = JSON(data)
                areas = CuretoRequestsHelper.parseRestaurantAreasWithPicture(json)
                onCompletion(areasWithPicture: areas, message: "SUCCESS")
                
            } else {
                onCompletion(areasWithPicture: areas, message: "FAILED")
            }
        }
    }
    
}

// MARK: - Handling pins

extension CuretoRequests {
    
    static func getUserPinnedArticles(token: String, area: String, onCompletion: (result: [ArticleGroupWithHeader], message: String) -> Void) {
        var articleGroups = [ArticleGroupWithHeader]()
        
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: articleGroups, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let url = CuretoUrls.userPinsWithinAreaUrl + "?area=\(area.urlSafe())"
        
        Alamofire.request(.GET, url, headers: header, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: articleGroups, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                articleGroups = CuretoRequestsHelper.parseArticleGroupsWithJSON(json, rootName: "pins")
                onCompletion(result: articleGroups, message: "SUCCESS")
                
            } else {
                onCompletion(result: articleGroups, message: "FAILED")
            }
        }
        
    }
    
    static func getUserPinsAreasWithPicture(token: String, onCompletion: (areasWithPicture: [GroupedAreaWithPicture], message: String) -> Void) {
        var areas = [GroupedAreaWithPicture]()
        
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(areasWithPicture: areas, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        Alamofire.request(.GET, CuretoUrls.userPinsAreasWithPictureUrl, headers: header, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(areasWithPicture: areas, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                areas = CuretoRequestsHelper.parseRestaurantAreasWithPicture(json, rootName: "pins")
                onCompletion(areasWithPicture: areas, message: "SUCCESS")
                
            } else {
                onCompletion(areasWithPicture: areas, message: "FAILED")
            }
        }
        
    }
    
    static func getUserPin(token: String, articleID: Int, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let url = CuretoUrls.userPinUrl + "/\(articleID)"
        
        Alamofire.request(.GET, url, headers: header, encoding: .JSON).responseJSON {
            response in
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
    static func getUserChronologicalPins(token: String, page: Int = 1, onCompletion: (result: [Article], message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [Article](), message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let url = CuretoUrls.userPinsUrl + "?page=\(page)"
        
        Alamofire.request(.GET, url, headers: header, encoding: .JSON).responseJSON { response in
            var articles = [Article]()
            
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: articles, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                articles = CuretoRequestsHelper.parseArticlesWithJSON(json, key: "pins", root: true)
                onCompletion(result: articles, message: "SUCCESS")
            } else {
                onCompletion(result: articles, message: "FAILED")
            }
        }
    }
    
    static func getUserAreaPins(token: String, onCompletion: (result: [PinGroup], message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [PinGroup](), message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        Alamofire.request(.GET, CuretoUrls.userAreaPinsUrl, headers: header, encoding: .JSON).responseJSON { response in
            
            var pinGroups = [PinGroup]()
            
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: pinGroups, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                pinGroups = CuretoRequestsHelper.parsePinGroupsWithJSON(json)
                onCompletion(result: pinGroups, message: "SUCCESS")
            } else {
                onCompletion(result: pinGroups, message: "FAILED")
            }
        }
    }
    
    static func createPin(token: String, articleID: Int, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let param: [String: AnyObject] = [
            "pin": [
                "article_id": articleID,
            ]
        ]
        
        Alamofire.request(.POST, CuretoUrls.createPinUrl, headers: header, parameters: param, encoding: .JSON).responseJSON {
            response in
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 201 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
    static func destroyPin(token: String, articleID: Int, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let url = CuretoUrls.destroyPinUrl + "/\(articleID)"
        
        Alamofire.request(.DELETE, url, headers: header, encoding: .JSON).responseJSON {
            response in
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
}

// MARK: - Handling stars

extension CuretoRequests {
    
    static func getStarCountForArticle(id: Int, onCompletion: (count: Int, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(count: 0, message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.starCountUrl + "/\(id)"
        
        Alamofire.request(.GET, url, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(count: 0, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                if let number = json["message"].int {
                    onCompletion(count: number, message: "SUCCESS")
                } else {
                    onCompletion(count: 0, message: "FAILED")
                }
                
            } else {
                onCompletion(count: 0, message: "FAILED")
            }
        }
    }
    
    static func createStar(token: String, articleID: Int, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let param: [String: AnyObject] = [
            "article_id": articleID
        ]
        
        Alamofire.request(.POST, CuretoUrls.createStarUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 201 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
    static func destroyStar(token: String, articleID: Int, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let url = CuretoUrls.destroyStarUrl + "/\(articleID)"
        
        Alamofire.request(.DELETE, url, headers: header, encoding: .JSON).responseJSON {
            response in
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
}

// MARK: - Handling messages

extension CuretoRequests {
    
    static func getUserUnreadCount(token: String, onCompletion: (result: Int, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: 0, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        Alamofire.request(.GET, CuretoUrls.userUnreadCountUrl, headers: header, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: 0, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                if let count = json["unread_count"].int {
                    onCompletion(result: count, message: "SUCCESS")
                } else {
                    onCompletion(result: 0, message: "FAILED")
                }
                
            } else {
                onCompletion(result: 0, message: "FAILED")
            }
        }
    }
    
    static func getUserMessages(token: String, page: Int, onCompletion: (result: [Message], message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [Message](), message: "No Internet Connection")
            return
        }
        
        var messages = [Message]()
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let url = CuretoUrls.userMessagesUrl + "?page=\(page)"
        
        Alamofire.request(.GET, url, headers: header, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: messages, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                messages = CuretoRequestsHelper.parseMessagesWithJSON(json)
                onCompletion(result: messages, message: "SUCCESS")
                
            } else {
                onCompletion(result: messages, message: "FAILED")
            }
        }
        
    }
    
    static func markAllUserMessagesRead(token: String, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        Alamofire.request(.PATCH, CuretoUrls.markAllMessagesReadUrl, headers: header, encoding: .JSON).responseJSON {
            response in
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 200 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
    static func sendMessage(token: String, recipientUsername: String, content: String, onCompletion: (succeeded: Bool, message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(succeeded: false, message: "No Internet Connection")
            return
        }
        
        let header: [String: String] = [
            "Authorization": "Token \(token)"
        ]
        
        let param: [String: AnyObject] = [
            "username": recipientUsername,
            "content": content
        ]
        
        Alamofire.request(.POST, CuretoUrls.createMessageUrl, headers: header, parameters: param, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, _ = response.result.value else {
                onCompletion(succeeded: false, message: "FAILED")
                return
            }
            
            if status == 201 {
                onCompletion(succeeded: true, message: "SUCCESS")
            } else {
                onCompletion(succeeded: false, message: "FAILED")
            }
        }
    }
    
}

// MARK: - Handling chills

extension CuretoRequests {
    
    static func getRestaurantAreas(onCompletion: (areas: [String]) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(areas: [String]())
            return
        }
        
        var restaurantAreas = [String]()
        
        Alamofire.request(.GET, CuretoUrls.restaurantAreasUrl, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(areas: restaurantAreas)
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                restaurantAreas = CuretoRequestsHelper.parseRestaurantAreasWithJSON(json)
                onCompletion(areas: restaurantAreas)
                
            } else {
                onCompletion(areas: restaurantAreas)
            }
        }
    }
    
    static func getExploreArticles(area: String, onCompletion: (result: [ArticleGroupWithHeader], message: String) -> Void) {
        var articleGroups = [ArticleGroupWithHeader]()
        
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: articleGroups, message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.exploreUrl + "?area=\(area.urlSafe())"
                
        Alamofire.request(.GET, url).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: articleGroups, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                articleGroups = CuretoRequestsHelper.parseArticleGroupsWithJSON(json)
                onCompletion(result: articleGroups, message: "SUCCESS")
                
            } else {
                onCompletion(result: articleGroups, message: "FAILED")
            }
        }
        
    }
    
    static func getChillArticles(area: String, type: Int, lowerPrice: Int, upperPrice: Int, onCompletion: (result: [Article]) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [Article]())
            return
        }
        
        let url = CuretoUrls.chillUrl + "?area=\(area.urlSafe())&type=\(type)&lower_price=\(lowerPrice)&upper_price=\(upperPrice)"
        
        var articles = [Article]()
        
        Alamofire.request(.GET, url, encoding: .JSON).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: articles)
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                articles = CuretoRequestsHelper.parseArticlesWithJSON(json)
                onCompletion(result: articles)
                
            } else {
                onCompletion(result: articles)
            }
        }
    }
}

// MARK: - Handling Featured Groups and Elements

extension CuretoRequests {
    
    static func getFeaturedLastUpdated(onCompletion: (result: String?) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: nil)
            return
        }
        
        Alamofire.request(.GET, CuretoUrls.featuredLastUpdatedUrl).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: nil)
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                if let date = json["updated"].string {
                    onCompletion(result: date)
                } else {
                    onCompletion(result: nil)
                }
                
            } else {
                onCompletion(result: nil)
            }
        }
    }
    
    static func getFeaturedGroups(onCompletion: (result: [FeaturedGroup]) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [FeaturedGroup]())
            return
        }
        
        var groups = [FeaturedGroup]()
        
        Alamofire.request(.GET, CuretoUrls.featuredGroupsUrl).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: groups)
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                groups = CuretoRequestsHelper.parseFeaturedGroupsWithJSON(json)
                onCompletion(result: groups)
                
            } else {
                onCompletion(result: groups)
            }
        }
    }
    
    static func getFeaturedElements(id: Int, onCompletion: (result: [FeaturedElement]) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [FeaturedElement]())
            return
        }
        
        let url = CuretoUrls.featuredElementsUrl + "/\(id)"
        
        var elements = [FeaturedElement]()
        
        Alamofire.request(.GET, url).responseJSON { response in
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: elements)
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                elements = CuretoRequestsHelper.parseFeaturedElementsWithJSON(json)
                onCompletion(result: elements)
                
            } else {
                onCompletion(result: elements)
            }
        }
    }
    
    static func getFeaturedGroupArticles(ids: String, onCompletion: (result: [Article], message: String) -> Void) {
        if !CuretoReachabilityHelper.sharedInstance.isConnectedToNetwork() {
            onCompletion(result: [Article](), message: "No Internet Connection")
            return
        }
        
        let url = CuretoUrls.featuredArticlesUrl + "?ids=\(ids)"
        
        Alamofire.request(.GET, url).responseJSON { response in
            
            var articles = [Article]()
            
            guard let status = response.response?.statusCode, data = response.result.value else {
                onCompletion(result: articles, message: "FAILED")
                return
            }
            
            if status == 200 {
                let json = JSON(data)
                articles = CuretoRequestsHelper.parseArticlesWithJSON(json)
                onCompletion(result: articles, message: "SUCCESS")
                
            } else {
                onCompletion(result: articles, message: "FAILED")
            }
        }
    }
    
}
