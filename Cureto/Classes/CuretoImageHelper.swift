//
//  CuretoImageHelper.swift
//  Cureto
//
//  Created by Sean on 12/29/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import CoreLocation
import AssetsLibrary

class CuretoImageHelper {
    
    internal static let assetLibrary = ALAssetsLibrary()
    
    static func coordinateForImageWithUrl(url: NSURL, onCompletion: (location: CLLocation?) -> Void) {
        assetLibrary.assetForURL(url, resultBlock: { asset in
            if let location = asset.valueForProperty(ALAssetPropertyLocation) as? CLLocation {
                onCompletion(location: location)
            } else {
                onCompletion(location: nil)
            }
            }, failureBlock: { error in
                onCompletion(location: nil)
        })
    }

    static func resizeImageWithMaxWidth(image: UIImage, maxWidth: CGFloat, quality: CGFloat) -> UIImage? {
        var actualWidth = image.size.width
        var actualHeight = image.size.height
        let heightRatio = actualHeight / actualWidth
        
//        print("actual \(actualWidth)x\(actualHeight) ratio \(heightRatio)")
        
        if actualWidth > maxWidth {
            actualWidth = maxWidth
            actualHeight = maxWidth * heightRatio
        }
        
//        print("actual \(actualWidth)x\(actualHeight)")
        
        let rect = CGRectMake(0, 0, actualWidth, actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.drawInRect(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        
        if let imageData = UIImageJPEGRepresentation(img, quality) {
            return UIImage(data: imageData)
        }
        return nil
    }
    
}
