//
//  CuretoCustomView.swift
//  Cureto
//
//  Created by Sean Choo on 1/31/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuretoCustomView: NSObject {
    
    internal static let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    internal static var blurView: UIVisualEffectView?
    internal static var pinView: CuretoPinView?

    static func presentPinView() {
        addBlurToWindow()
        
        let pinFromNib = NSBundle.mainBundle().loadNibNamed("CuretoPinView", owner: self, options: nil)[0] as! CuretoPinView
        pinFromNib.frame = CGRect(x: 16, y: MainScreen.height, width: MainScreen.width - 32, height: MainScreen.height * 0.75)
        pinFromNib.layer.cornerRadius = 3
        pinFromNib.cancelButton.addTarget(self, action: #selector(dismissView(_:)), forControlEvents: .TouchUpInside)
        pinView = pinFromNib
        appDelegate.window?.addSubview(pinFromNib)
        
        UIView.animateWithDuration(0.2, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            pinFromNib.frame = CGRect(x: 16, y: MainScreen.height * 0.1, width: MainScreen.width - 32, height: MainScreen.height * 0.75)
            }, completion: { finished in
        })
    }
    
    static func addBlurToWindow() {
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark))
        blurEffectView.frame = CGRect(x: 0, y: 0, width: MainScreen.width, height: MainScreen.height)
        blurView = blurEffectView
        appDelegate.window?.addSubview(blurEffectView)
        blurEffectView.alpha = 0
        
        UIView.animateWithDuration(0.1, delay: 0.0, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            blurEffectView.alpha = 1.0
            }, completion: { finished in
                
        })
    }
    
    static func dismissView(sender: UIButton) {
        guard let pin = pinView, blur = blurView else {
            return
        }
        
        UIView.animateWithDuration(0.2, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            pin.frame = CGRect(x: 16, y: MainScreen.height, width: MainScreen.width - 32, height: MainScreen.height * 0.75)
            }, completion: { finished in
                self.pinView = nil
        })
        
        UIView.animateWithDuration(0.1, delay: 0.0, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            blur.alpha = 0
            }, completion: { finished in
                self.blurView = nil
        })
    }
    
}
