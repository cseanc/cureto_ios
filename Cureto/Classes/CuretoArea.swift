//
//  CuretoArea.swift
//  Cureto
//
//  Created by Sean Choo on 1/21/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

enum CuretoArea {
    case HK
    
    func boundary() -> CuretoBoundary {
        switch self {
        case .HK:
            return CuretoBoundary(northest: 22.510451, southest: 22.162448, westest: 113.837585, eastest: 114.397888)
        }
    }
}
