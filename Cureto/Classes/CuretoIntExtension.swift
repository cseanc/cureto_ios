//
//  CuretoIntExtension.swift
//  Cureto
//
//  Created by Sean Choo on 3/24/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

extension Int {
    
    func humanizedString() -> String? {
        
        if self < 1000 {
            return "\(self)"
            
        } else if self >= 1000 && self < 999950 {
            let shortened = Float(self) / 1000.0
            if Int(shortened * 10) % 10 == 0 {
                return String(format: "%.fk", shortened)
            } else {
                return String(format: "%.1fk", shortened)
            }
        } else if self >= 999950 && self < 1000000 {
            return "1m"
            
        } else if self >= 1000000 {
            let shortened = Float(self) / 1000000.0
            if Int(shortened * 10) % 10 == 0 {
                return String(format: "%.fm", shortened)
            } else {
                return String(format: "%.1fm", shortened)
            }
        }
        
        return nil
    }
    
}