//
//  CuretoProgressHUD.swift
//  Cureto
//
//  Created by Sean Choo on 1/21/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class CuretoProgressHUD: NSObject {
    
    static let sharedInstance = CuretoProgressHUD()

    var progressWindow: UIWindow?
    var shownProgressView: CuretoProgressHUDView?
    
    var shownSpinView: CuretoProgressHUDSpinView?
    var shownMessageSpinView: CuretoProgressMessageSpinView?
    
    func addProgressObserver() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(uploadProgressChanged(_:)), name: "UploadProgressChanged", object: nil)
    }
    
    func showProgress(mode: Int = 0) {
        if progressWindow == nil && shownProgressView == nil {
            progressWindow = UIWindow(frame: CGRect(x: 0, y: 0, width: MainScreen.width, height: 20))
            progressWindow?.backgroundColor = UIColor.clearColor()
            progressWindow?.windowLevel = UIWindowLevelAlert
            progressWindow?.makeKeyAndVisible()
            
            let progressView = NSBundle.mainBundle().loadNibNamed("CuretoProgressHUDView", owner: self, options: nil)[0] as! CuretoProgressHUDView
            progressView.frame = CGRectMake(0, 0, MainScreen.width, 20)
            
            if mode == 0 {
                progressView.progressLabel.text = "Publishing"
            } else if mode == 1 {
                progressView.progressLabel.text = "Saving as Draft"
            } else if mode == 2 {
                progressView.progressLabel.text = "Updating"
            }
            
            progressWindow?.addSubview(progressView)
            shownProgressView = progressView
            
            progressView.slideInFromTop()
            
            NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(CuretoProgressHUD.sharedInstance.animateProgressLabel), userInfo: progressView.progressLabel, repeats: false)
        }
    }
    
    func animateProgressLabel(sender: NSTimer) {
        if let label = sender.userInfo as? UILabel {
            UIView.animateWithDuration(1, delay: 0, options: [.CurveEaseInOut, .Repeat, .Autoreverse], animations: {
                label.alpha = 0.5
                }, completion: { finished in
                    
            })
        }
    }
    
    func endProcess() {
        UIView.animateWithDuration(0.3, delay: 1.8, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.shownProgressView?.frame = CGRectMake(0, -20, MainScreen.width, 20)
            }, completion: { finished in
                self.shownProgressView?.removeFromSuperview()
                self.shownProgressView = nil
                self.progressWindow = nil
        })
    }
    
    func uploadProgressChanged(notification: NSNotification) {
        if let progress = notification.object as? CGFloat {
//            print("Progress: \(progress)")
            if progress == -1 {
                endProcess()
            } else {
                let progressConstant: CGFloat = (progress/100.0) * MainScreen.width
                shownProgressView?.progressWidth.constant = progressConstant
                UIView.animateWithDuration(0.1, delay: 0.0, options: .CurveEaseInOut, animations: {
                    self.shownProgressView?.layoutIfNeeded()
                    }, completion: { finished in
                        
                })
                
                if progress == 100 {
                    endProcess()
                }
            }
        }
    }
    
}

// MARK: - Spin View

extension CuretoProgressHUD {
    
    func startSpinning(view: UIView? = nil) {
        if shownSpinView == nil {
            let spinView = NSBundle.mainBundle().loadNibNamed("CuretoProgressHUDSpinView", owner: self, options: nil)[0] as! CuretoProgressHUDSpinView
            spinView.frame = CGRectMake(0, 0, 50, 50)
            spinView.blurView.layer.masksToBounds = true
            spinView.blurView.layer.cornerRadius = 25
            
            if let v = view {
                spinView.center = v.center
                v.addSubview(spinView)
            } else {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                if let center = appDelegate.window?.center {
                    spinView.center = center
                }
                appDelegate.window?.addSubview(spinView)
            }
            
            shownSpinView = spinView
            
            spinView.spinner.startAnimating()
            
            spinView.alpha = 0
            
            UIView.animateWithDuration(0.25, delay: 0.0, options: .CurveEaseInOut, animations: {
                spinView.alpha = 1
                }, completion: { finished in
                    
            })
        }
    }
    
    func stopSpinning() {
        guard let spinView = shownSpinView else {
            return
        }
        spinView.spinner.stopAnimating()
        UIView.animateWithDuration(0.25, delay: 0.0, options: .CurveEaseInOut, animations: {
            spinView.alpha = 0
            }, completion: { finished in
                spinView.removeFromSuperview()
                self.shownSpinView = nil
        })
    }
    
}

// MARK: - Message Spin View

extension CuretoProgressHUD {
    
    func startMessageSpinning(view: UIView? = nil, message: String = "Loading") {
        if shownMessageSpinView == nil {
            let spinView = NSBundle.mainBundle().loadNibNamed("CuretoProgressMessageSpinView", owner: self, options: nil)[0] as! CuretoProgressMessageSpinView
            spinView.frame = CGRectMake(0, 0, 135, 125)
            spinView.layer.masksToBounds = true
            spinView.layer.cornerRadius = 7
            
            spinView.messasgeLabel.text = message
            
            if let v = view {
                spinView.center = v.center
                v.addSubview(spinView)
            } else {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                if let center = appDelegate.window?.center {
                    spinView.center = center
                }
                appDelegate.window?.addSubview(spinView)
            }
            
            shownMessageSpinView = spinView
            
            spinView.spinner.startAnimating()
            
            spinView.alpha = 0
            
            UIView.animateWithDuration(0.25, delay: 0.0, options: .CurveEaseInOut, animations: {
                spinView.alpha = 1
                }, completion: { finished in
                    
            })
        }
    }
    
    func stopMessageSpinning() {
        guard let spinView = shownMessageSpinView else {
            return
        }
        spinView.spinner.stopAnimating()
        UIView.animateWithDuration(0.25, delay: 0.0, options: .CurveEaseInOut, animations: {
            spinView.alpha = 0
            }, completion: { finished in
                spinView.removeFromSuperview()
                self.shownMessageSpinView = nil
        })
    }
}