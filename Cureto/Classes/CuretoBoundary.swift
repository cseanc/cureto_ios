//
//  CuretoBoundary.swift
//  Cureto
//
//  Created by Sean Choo on 1/21/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

struct CuretoBoundary {
    let northest: Double?
    let southest: Double?
    let westest: Double?
    let eastest: Double?
    
    init(northest: Double, southest: Double, westest: Double, eastest: Double) {
        self.northest = northest
        self.southest = southest
        self.westest = westest
        self.eastest = eastest
    }
}
