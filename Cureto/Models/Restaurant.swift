//
//  Restaurant.swift
//  Cureto
//
//  Created by Sean on 11/30/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class Restaurant {
    
    var id: Int?
    var identifier: String?
    var name: String?
    var latitude: Double?
    var longitude: Double?
    var address: String?
    var categoryId: String?
    var categoryName: String?
    var city: String?
    var country: String?
    
    init(identifier: String, name: String, latitude: Double, longitude: Double) {
        self.identifier = identifier
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
    }
    
    init(identifier: String, name: String, latitude: Double, longitude: Double, address: String, categoryId: String, categoryName: String) {
        self.identifier = identifier
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.address = address
        self.categoryId = categoryId
        self.categoryName = categoryName
    }
    
}
