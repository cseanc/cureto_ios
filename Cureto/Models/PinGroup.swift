//
//  PinGroup.swift
//  Cureto
//
//  Created by Sean Choo on 3/21/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class PinGroup {
    
    var area: String = ""
    var articles = [Article]()
    
    init(area: String) {
        self.area = area
    }
    
}