//
//  MessagesTracker.swift
//  Cureto
//
//  Created by Sean Choo on 7/10/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class MessagesTracker {
    
    static let sharedInstance = MessagesTracker()
    
    var count: Int = 0
}
