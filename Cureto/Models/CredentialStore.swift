//
//  CredentialStore.swift
//  Cureto
//
//  Created by Sean on 11/10/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import KeychainSwift

class CredentialStore: NSObject {
    
    static let sharedInstance = CredentialStore()
    let keychain = KeychainSwift()
    
    var accessToken: String? {
        get {
            if let token = keychain.get("access_token") {
                return token
            }
            return nil
        }
        set {
            keychain.set(newValue!, forKey: "access_token")
        }
    }
    
    var username: String? {
        get {
            if let username = keychain.get("username") {
                return username
            }
            return nil
        }
        set {
            keychain.set(newValue!, forKey: "username")
        }
    }
    
    func clearEverything() {
        keychain.clear()
    }
    
}
