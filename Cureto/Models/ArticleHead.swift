//
//  ArticleHead.swift
//  Cureto
//
//  Created by Sean on 12/1/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class ArticleHead {
    
    // For edit
    var id: Int?
    
    var title: String?
    var content: String?
    var coverPhoto: UIImage?
    
    init(title: String, content: String, coverPhoto: UIImage) {
        self.title = title
        self.content = content
        self.coverPhoto = coverPhoto
    }
    
    init(title: String, content: String) {
        self.title = title
        self.content = content
    }
    
}
