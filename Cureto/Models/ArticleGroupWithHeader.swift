//
//  ArticleGroupWithHeader.swift
//  Cureto
//
//  Created by Sean Choo on 7/24/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class ArticleGroupWithHeader {
    
    var header: String = ""
    var articles = [Article]()
    
    init(header: String, articles: [Article]) {
        self.header = header
        self.articles = articles
    }
}
