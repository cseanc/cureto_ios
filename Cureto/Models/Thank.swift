//
//  Thank.swift
//  Cureto
//
//  Created by Sean Choo on 4/9/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class Thank {
    
    var people: String?
    var reason: String?
    var link: String?
    
    init(people: String, reason: String, link: String) {
        self.people = people
        self.reason = reason
        self.link = link
    }
    
}
