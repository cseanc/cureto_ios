//
//  PhotoWithSubtitle.swift
//  Cureto
//
//  Created by Sean on 11/26/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class PhotoWithSubtitle {
    
    // For edit
    var id: Int?
    var subphotoDidChange = false
    
    var photo: UIImage?
    var subtitle: String?
    
    init(photo: UIImage, subtitle: String) {
        self.photo = photo
        self.subtitle = subtitle
    }
    
    init(subtitle: String) {
        self.subtitle = subtitle
    }
    
}
