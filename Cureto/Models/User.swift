//
//  User.swift
//  Cureto
//
//  Created by Sean on 11/17/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class User {
    
    var username: String?
    var displayName: String?
    var profileImageUrl: String?
    var bio: String?
    var articlesCount: Int?
    
}
