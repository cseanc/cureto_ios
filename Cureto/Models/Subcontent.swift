//
//  Subcontent.swift
//  Cureto
//
//  Created by Sean on 12/3/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class Subcontent {
    
    var id: Int?
    var articleId: Int?
    var originalSubphotoUrl: String?
    var subphotoUrl: String?
    var subphotoThumbUrl: String?
    var subtitle: String?
    
    var subphotoWidth: CGFloat?
    var subphotoHeight: CGFloat?
    
    init(id: Int?, subphotoUrl: String, subtitle: String) {
        self.id = id
        self.subphotoUrl = subphotoUrl
        self.subtitle = subtitle
    }
    
}
