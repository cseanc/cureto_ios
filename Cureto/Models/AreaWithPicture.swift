//
//  AreaWithPicture.swift
//  Cureto
//
//  Created by Sean Choo on 7/9/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class AreaWithPicture {
    
    var name: String?
    var pictureUrl: String?
    
    init(name: String, pictureUrl: String) {
        self.name = name
        self.pictureUrl = pictureUrl
    }
}
