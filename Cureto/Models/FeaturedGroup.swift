//
//  FeaturedGroup.swift
//  Cureto
//
//  Created by Sean Choo on 5/15/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class FeaturedGroup {
    
    var id: Int?
    var featuredType: Int?
    var title: String?
    var desc: String?
    var coverPhotoUrl: String?
    var opacity: Int?
    
    init(id: Int, featuredType: Int, title: String, desc: String, coverPhotoUrl: String, opacity: Int = 15) {
        self.id = id
        self.featuredType = featuredType
        self.title = title
        self.desc = desc
        self.coverPhotoUrl = coverPhotoUrl
        self.opacity = opacity
    }
    
}
