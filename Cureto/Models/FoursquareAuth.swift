//
//  FoursquareAuth.swift
//  Cureto
//
//  Created by Sean on 11/29/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class FoursquareAuth {
    
    let clientId: String?
    let clientSecret: String?
    
    init(clientId: String, clientSecret: String) {
        self.clientId = clientId
        self.clientSecret = clientSecret
    }
    
}
