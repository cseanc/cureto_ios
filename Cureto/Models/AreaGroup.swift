//
//  AreaGroup.swift
//  Cureto
//
//  Created by Sean Choo on 5/19/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class AreaGroup {
    
    var title: String = ""
    var areas = [String]()
    
    init(title: String) {
        self.title = title
    }
    
}
