//
//  Message.swift
//  Cureto
//
//  Created by Sean Choo on 3/29/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class Message {
    
    var id: Int?
    var senderId: Int?
    var recipientId: Int?
    var content: String?
    var read: Bool = false
    var createdAt: NSDate?
    
    init(id: Int, senderId: Int, recipientId: Int, content: String, read: Bool, createdAt: NSDate) {
        self.id = id
        self.senderId = senderId
        self.recipientId = recipientId
        self.content = content
        self.read = read
        self.createdAt = createdAt
    }
    
}
