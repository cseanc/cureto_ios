//
//  Article.swift
//  Cureto
//
//  Created by Sean on 11/26/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit

class Article {
    
    var id: Int?
    var title: String?
    var content: String?
    var originalCoverPhotoUrl: String?
    var coverPhotoUrl: String?
    var coverPhotoThumbUrl: String?
    
    var coverPhotoWidth: CGFloat?
    var coverPhotoHeight: CGFloat?
    
    var foodType: Int = 0
    var mealType: String?
    var mealPrice: Float?
    
    var subcontent = [Subcontent]()
    
    var restaurant: Restaurant?
    
    var author: User?
    
    var draft = false
    
    var starsCount: Int = 0
    
    init(id: Int, author: User) {
        self.id = id
        self.author = author
    }
    
    init(id: Int, title: String, content: String, coverPhotoUrl: String, foodType: Int, mealPrice: Float, restaurant: Restaurant, author: User) {
        self.id = id
        self.title = title
        self.content = content
        self.coverPhotoUrl = coverPhotoUrl
        self.foodType = foodType
        self.mealPrice = mealPrice
        self.restaurant = restaurant
        self.author = author
    }
    
    init(id: Int, title: String, content: String, coverPhotoUrl: String, mealType: String, mealPrice: Float, restaurant: Restaurant, author: User) {
        self.id = id
        self.title = title
        self.content = content
        self.coverPhotoUrl = coverPhotoUrl
        self.mealType = mealType
        self.mealPrice = mealPrice
        self.restaurant = restaurant
        self.author = author
    }
    
}
