//
//  FeaturedElement.swift
//  Cureto
//
//  Created by Sean Choo on 5/15/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class FeaturedElement {
    
    var id: Int?
    var targetId: Int?
    
    init(id: Int, targetId: Int) {
        self.id = id
        self.targetId = targetId
    }
    
}
