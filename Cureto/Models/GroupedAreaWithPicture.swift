//
//  GroupedAreaWithPicture.swift
//  Cureto
//
//  Created by Sean Choo on 7/9/16.
//  Copyright © 2016 Sean. All rights reserved.
//

import UIKit

class GroupedAreaWithPicture {
    
    var title: String = ""
    var areas = [AreaWithPicture]()
    
    init(title: String) {
        self.title = title
    }
    
}
