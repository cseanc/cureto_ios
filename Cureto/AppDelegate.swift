//
//  AppDelegate.swift
//  Cureto
//
//  Created by Sean on 10/29/15.
//  Copyright © 2015 Sean. All rights reserved.
//

import UIKit
import CoreData
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UITabBarControllerDelegate {
    
    var window: UIWindow?
    var statusBar: UIView?
    var middleButton: UIButton?
    var articleMenu: UIView?
    var articleController: ArticleController?
    var mainTabBarController: UITabBarController?
    var homeNavController: UINavigationController?
    var exploreNavController: UINavigationController?
//    var chillNavController: UINavigationController?
//    var featuredNavController: UINavigationController?
    var pinNavController: UINavigationController?
//    var messageNavController: UINavigationController?
    var curatorNavController: UINavigationController?
    
    var previousIndex = 0
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if userDefaults.valueForKey(CuretoGlobal.locationPermissonSet) == nil {
            userDefaults.setValue(CuretoGlobal.falseString, forKey: CuretoGlobal.locationPermissonSet)
        }
                        
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.tintColor = MainColor.globalTint
        initializeTabBarController()
        window?.makeKeyAndVisible()
        
//        addStatusBar()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(middleButtonNeeded(_:)), name: "MiddleButtonNeeded", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(middleButtonNotNeeded(_:)), name: "MiddleButtonNotNeeded", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(loginStatusChanged(_:)), name: "LoginStatusChanged", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(statusBarRequirementChanged(_:)), name: "StatusBarRequirementChanged", object: nil)
        
        CuretoReachabilityHelper.sharedInstance.addReachabilityObserver()
        CuretoProgressHUD.sharedInstance.addProgressObserver()
        
        CuretoReachabilityHelper.sharedInstance.startObserving()
        
        return true
    }
    
    func loginStatusChanged(notification: NSNotification) {
        initializeTabBarController()
        
        if let token = CredentialStore.sharedInstance.accessToken {
            CuretoRequests.getUserUnreadCount(token, onCompletion: { count, message in
                if count > 0 {
                    if let tabBarController = self.mainTabBarController {
                        if let items = tabBarController.tabBar.items {
                            MessagesTracker.sharedInstance.count = count
                            items[4].badgeValue = "\(count)"
                        }
                    }
                }
            })
        }
    }
    
    func statusBarRequirementChanged(notification: NSNotification) {
        if let needed = notification.object as? Bool {
            if needed {
                addStatusBar()
            } else {
                removeStatusBar()
            }
        }
    }
    
    func addStatusBar() {
        if statusBar == nil {
            let bar = UIView(frame: CGRect(x: 0, y: 0, width: MainScreen.width, height: 20))
            bar.backgroundColor = MainColor.navBarTint
            statusBar = bar
            window?.addSubview(bar)
        }
    }
    
    func removeStatusBar() {
        statusBar?.removeFromSuperview()
        statusBar = nil
    }
    
    func middleButtonNeeded(notification: NSNotification) {
        if let tabBarController = mainTabBarController {
            addMiddleButton(tabBarController)
        }
    }
    
    func middleButtonNotNeeded(notification: NSNotification) {
        if let button = middleButton {
            button.removeFromSuperview()
        }
        middleButton = nil
    }
    
    func replaceMiddleButton() {
        if let button = middleButton {
            button.removeFromSuperview()
        }
        middleButton = nil
        if let tabBarController = mainTabBarController {
            addMiddleButton(tabBarController)
        }
    }
    
    func initializeTabBarController() {
        mainTabBarController = UITabBarController()
        mainTabBarController?.delegate = self
        
        if let tabBarController = mainTabBarController {
            tabBarController.tabBar.translucent = false
            tabBarController.tabBar.barTintColor = MainColor.tabBarTint
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let homeController = storyboard.instantiateViewControllerWithIdentifier("HomeViewController")
            let homeNavigationController = UINavigationController(rootViewController: homeController)
            homeNavigationController.navigationBarHidden = false
            homeNavigationController.navigationBar.translucent = false
            homeNavigationController.navigationBar.barTintColor = MainColor.navBarTint
//            homeNavigationController.hidesBarsOnSwipe = true
            homeNavController = homeNavigationController
            
            let exploreController = storyboard.instantiateViewControllerWithIdentifier("ExploreViewController")
            let exploreNavigationController = UINavigationController(rootViewController: exploreController)
            exploreNavigationController.navigationBarHidden = false
            exploreNavigationController.navigationBar.translucent = false
            exploreNavigationController.navigationBar.barTintColor = MainColor.navBarTint
            exploreNavController = exploreNavigationController
            
//            let chillController = storyboard.instantiateViewControllerWithIdentifier("ChillViewController")
//            let chillNavigationController = UINavigationController(rootViewController: chillController)
//            chillNavigationController.navigationBarHidden = false
//            chillNavigationController.navigationBar.translucent = false
//            chillNavigationController.navigationBar.barTintColor = MainColor.navBarTint
//            chillNavigationController.hidesBarsOnSwipe = true
//            chillNavController = chillNavigationController
            
//            let featuredController = storyboard.instantiateViewControllerWithIdentifier("FeaturedViewController")
//            let featuredNavigationController = UINavigationController(rootViewController: featuredController)
//            featuredNavigationController.navigationBarHidden = false
//            featuredNavController = featuredNavigationController
            
            let dummyController = storyboard.instantiateViewControllerWithIdentifier("DummyViewController")
            
//            var messageController = UIViewController()
//            if let _ = CredentialStore.sharedInstance.accessToken {
//                messageController = storyboard.instantiateViewControllerWithIdentifier("MessageViewController")
//            } else {
//                messageController = storyboard.instantiateViewControllerWithIdentifier("BlankMessagesViewController")
//            }
//            let messageNavigationController = UINavigationController(rootViewController: messageController)
//            messageNavigationController.navigationBarHidden = false
//            messageNavigationController.navigationBar.translucent = false
//            messageNavigationController.navigationBar.barTintColor = MainColor.navBarTint
//            messageNavigationController.hidesBarsOnSwipe = true
//            messageNavController = messageNavigationController
            
            var pinController = UIViewController()
            if let _ = CredentialStore.sharedInstance.accessToken {
                pinController = storyboard.instantiateViewControllerWithIdentifier("PinViewController")
            } else {
                pinController = storyboard.instantiateViewControllerWithIdentifier("NotLoginViewController")
            }
            let pinNavigationController = UINavigationController(rootViewController: pinController)
            pinNavigationController.navigationBarHidden = false
            pinNavigationController.navigationBar.translucent = false
            pinNavigationController.navigationBar.barTintColor = MainColor.navBarTint
            pinNavController = pinNavigationController
            
            var curatorController = UIViewController()
            if let _ = CredentialStore.sharedInstance.accessToken {
                curatorController = storyboard.instantiateViewControllerWithIdentifier("CuratorViewController")
            } else {
                curatorController = storyboard.instantiateViewControllerWithIdentifier("BeCuratorController")
            }
            let curatorNavigationController = UINavigationController(rootViewController: curatorController)
            curatorNavigationController.navigationBarHidden = true
            curatorNavigationController.navigationBar.translucent = false
            curatorNavigationController.navigationBar.barTintColor = MainColor.navBarTint
//            curatorNavigationController.hidesBarsOnSwipe = true
            curatorNavController = curatorNavigationController
            
            
            tabBarController.viewControllers = [homeNavigationController, exploreNavigationController, dummyController, pinNavigationController, curatorNavigationController]

            
            if let items = tabBarController.tabBar.items {
                items[0].selectedImage = UIImage(named: "Showroom")?.imageWithRenderingMode(.AlwaysOriginal)
                items[0].image = UIImage(named: "Showroom_Faded")?.imageWithRenderingMode(.AlwaysOriginal)
                items[0].imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
                
                items[1].selectedImage = UIImage(named: "Explore")?.imageWithRenderingMode(.AlwaysOriginal)
                items[1].image = UIImage(named: "Explore_Faded")?.imageWithRenderingMode(.AlwaysOriginal)
                items[1].imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
                
                items[2].selectedImage = UIImage(named: "Curate")?.imageWithRenderingMode(.AlwaysOriginal)
                items[2].image = UIImage(named: "Curate")?.imageWithRenderingMode(.AlwaysOriginal)
                items[2].imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
                items[2].enabled = false
                
                items[3].selectedImage = UIImage(named: "Folder")?.imageWithRenderingMode(.AlwaysOriginal)
                items[3].image = UIImage(named: "Folder_Faded")?.imageWithRenderingMode(.AlwaysOriginal)
                items[3].imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
                
                items[4].selectedImage = UIImage(named: "Profile")?.imageWithRenderingMode(.AlwaysOriginal)
                items[4].image = UIImage(named: "Profile_Faded")?.imageWithRenderingMode(.AlwaysOriginal)
                items[4].imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
            }
            
            middleButton = nil
            addMiddleButton(tabBarController)
            
            window?.rootViewController = tabBarController
        }
    }
    
    func addMiddleButton(tabBarController: UITabBarController) {
        
        if middleButton == nil {
            let buttonImage = UIImage(named: "Curate_Faded")
            let highlightImage = UIImage(named: "Curate_Inverted")
            
            let frame = CGRectMake(0, 0, buttonImage!.size.width, buttonImage!.size.height)
            let button = UIButton(frame: frame)
            
            button.setBackgroundImage(buttonImage, forState: .Normal)
            button.setBackgroundImage(highlightImage, forState: .Highlighted)
            
            button.center = tabBarController.tabBar.center
            
            button.addTarget(self, action: #selector(middleButtonTapped(_:)), forControlEvents: .TouchUpInside)
            tabBarController.view.addSubview(button)
            
            middleButton = button
        }

    }
    
    func middleButtonTapped(sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let _ = CredentialStore.sharedInstance.accessToken {
            
            let publishNavController = storyboard.instantiateViewControllerWithIdentifier("PublishNavController") as! UINavigationController
            publishNavController.navigationBarHidden = true
            
            if let tabBarController = mainTabBarController {
                tabBarController.presentViewController(publishNavController, animated: true, completion: nil)
            }
            
        } else {
            CuretoBannerAlert.showMessage("Log in to publish article")
        }
        
    }
    
    func addArticleMenu() {
        let menuView = NSBundle.mainBundle().loadNibNamed("ArticleMenu", owner: self, options: nil)[0] as! ArticleMenu
        menuView.frame = CGRectMake(0, MainScreen.height - 28, MainScreen.width, 28)
        window?.addSubview(menuView)
        articleMenu = menuView
    }
    
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        if tabBarController.selectedIndex == 2 {
            previousIndex = 2
            return
        }
        
        if tabBarController.selectedIndex == previousIndex {
            if let navController = viewController as? UINavigationController {
                if let controller = navController.viewControllers.first {
                    if let tableViewController = controller as? UITableViewController {
                        if tableViewController.tableView.numberOfSections > 0 {
                            if tableViewController.tableView.numberOfRowsInSection(0) > 0 {
                                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                                tableViewController.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
                            }
                        }
                    }
                }
            }
            
        } else {
            previousIndex = tabBarController.selectedIndex
        }
    }
    
    func application(application: UIApplication, didChangeStatusBarFrame oldStatusBarFrame: CGRect) {
        if let _ = middleButton {
            replaceMiddleButton()
        }
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.        
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        
        if let token = CredentialStore.sharedInstance.accessToken {
            CuretoRequests.validateToken(token, onCompletion: { valid, message in
                if valid {
                    CuretoRequests.getUserUnreadCount(token, onCompletion: { count, message in
                        if count > 0 {
                            if let tabBarController = self.mainTabBarController {
                                if let items = tabBarController.tabBar.items {
                                    MessagesTracker.sharedInstance.count = count
                                    items[4].badgeValue = "\(count)"
                                }
                            }
                        }
                    })
                    
                } else {
                    CredentialStore.sharedInstance.clearEverything()
                    NSNotificationCenter.defaultCenter().postNotificationName("LoginStatusChanged", object: self)
                }
            })
        }
        
        if let homeController = homeNavController?.visibleViewController as? HomeController {
            if homeController.isViewLoaded() {
                if let _ = homeController.view.window {
                    // HomeController visible
                    if homeController.articles.count > 0 {
                        if let firstArticleId = homeController.articles[0].id {
                            CuretoRequests.getHomeFirstArticleId(homeController.style, foodType: homeController.selectedFoodType, priceRange: homeController.selectedPriceRange, onCompletion: { result, message in
                                if let id = result {
                                    if id != firstArticleId {
                                        homeController.reloadArticlesForStyle(homeController.style)
                                    }
                                }
                            })
                        }
                    }
                }
            }
            
        }
        
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.cseanc.Cureto" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Cureto", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

